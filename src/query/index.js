import gql from 'graphql-tag';

export const GET_CONVERSATION_PROF = gql`
  query getConversationProf($ProfId: ID!) {
    getConversationProf(ProfId: $ProfId) {
      message
      success
      conversation {
        _id
        user
        prof
        profesional {
          id
          email
          nombre
          apellidos
          ciudad
          telefono
          avatar
          UserID
          profesion
          experiencia
          descricion
          Precio
          anadidoFavorito
          notificacion
          isProfesional
          verifyPhone
          isVerified
          isAvailable
          lastTime
          connected
          professionalRatinglist {
            id
          }
        }
        usuario {
          id
          email
          UserID
          verifyPhone
          nombre
          apellidos
          ciudad
          telefono
          avatar
          notificacion
          StripeID
          isPlus
          lastTime
          connected
        }
        messagechat {
          _id
          text
          image
          read
          createdAt
          user {
            _id
            name
            avatar
          }
        }
      }
    }
  }
`;

export const GET_STATISTICS = gql`
  query getStatistics($userId: ID!) {
    getStatistics(userId: $userId) {
      success
      message
      data {
        name
        Ene
        Feb
        Mar
        Abr
        May
        Jun
        Jul
        Aug
        Sep
        Oct
        Nov
        Dic
      }
    }
  }
`;

export const GET_PROFESIONAL_VISITAS = gql`
  query getProfVisitas($profesionalId: ID!) {
    getProfVisitas(profesionalId: $profesionalId) {
      success
      message
      list {
        id
        profesional_id
        mes_id
        ano
        visitas
      }
    }
  }
`;

export const GET_MESSAGE_NO_READ = gql`
  query getMessageNoReadProf($conversationID: ID!, $clientId: ID!) {
    getMessageNoReadProf(conversationID: $conversationID, clientId: $clientId) {
      message
      success
      conversation {
        _id
        count
      }
    }
  }
`;

export const PROFESIONAL_DETAIL = gql`
  query getProfesional($id: ID!) {
    getProfesional(id: $id) {
      id
      email
      UserID
      nombre
      verifyPhone
      apellidos
      ciudad
      telefono
      avatar
      notificacion
      category_id
      isVerified
      isAvailable
      profesion
      experiencia
      descricion
      Precio
      isPlusVet
      lastTime
      connected
    }
  }
`;

export const GET_MASCOTAS = gql`
  query getMascota($usuarios: ID!) {
    getMascota(usuarios: $usuarios) {
      message
      success
      list {
        id
        name
        age
        usuario
        avatar
        especie
        raza
        genero
        peso
        microchip
        vacunas
        alergias
        castrado
      }
    }
  }
`;

export const GET_MY_PACIENTES = gql`
  query getmyPacientes($id: ID!, $search: String) {
    getmyPacientes(id: $id, search: $search) {
      message
      success
      list {
        id
        email
        UserID
        nombre
        apellidos
        ciudad
        telefono
        avatar
        notificacion
        StripeID
        isPlus
        verifyPhone
        setVideoConsultas
        lastTime
        connected
        suscription {
          id
          current_period_end
          current_period_start
          status
          livemode
          trial_end
          trial_start
        }
      }
    }
  }
`;

export const GET_CATEGORIES = gql`
  query {
    getCategories {
      id
      title
      image
      font
      description
    }
  }
`;

export const FAVORITO_PROFESIONAL = gql`
  query getUsuarioFavoritoall($id: ID!) {
    getUsuarioFavoritoall(id: $id) {
      message
      success
      list {
        id
      }
    }
  }
`;

export const CONSULTA_BY_PROFESIONAL = gql`
  query getConsultaByProfessional(
    $profesional: ID!
    $dateRange: DateRangeInput
  ) {
    getConsultaByProfessional(
      profesional: $profesional
      dateRange: $dateRange
    ) {
      success
      message
      list {
        id
        time
        estado
        endDate
        nota
        created_at
        mascota {
          id
          name
          age
          usuario
          avatar
          especie
          raza
          genero
          peso
          microchip
          vacunas
          alergias
          castrado
        }
        usuario {
          id
          email
          verifyPhone
          UserID
          nombre
          apellidos
          ciudad
          telefono
          avatar
          notificacion
          lastTime
          connected
        }
        profesional {
          id
          email
          nombre
          apellidos
          UserID
          ciudad
          telefono
          avatar
          verifyPhone
          profesion
          experiencia
          descricion
          Precio
          notificacion
          isProfesional
          isVerified
          isAvailable
          lastTime
          connected
        }
      }
    }
  }
`;

export const GET_DIAGNOSTICO = gql`
  query getDiagnostico($id: ID!) {
    getDiagnostico(id: $id) {
      message
      success
      list {
        _id
        profesional {
          nombre
          apellidos
          profesion
          avatar
        }
        diagnostico
        mascota
        created_at
      }
    }
  }
`;

export const GET_NOTIFICATIONS = gql`
  query getNotifications($Id: ID!) {
    getNotifications(Id: $Id) {
      success
      message
      notifications {
        _id
        type
        read
        createdAt
        user
        profesional {
          id
          nombre
          apellidos
          avatar
        }
        usuario {
          id
          nombre
          apellidos
          avatar
        }
      }
    }
  }
`;

export const GET_DEPOSITOS = gql`
  query getDeposito($id: ID!) {
    getDeposito(id: $id) {
      success
      message
      list {
        id
        fecha
        estado
        total
        created_at
        profesionalID
      }
    }
  }
`;

export const PAGO_QUERY = gql`
  query getPago($id: ID!) {
    getPago(id: $id) {
      success
      message
      data {
        id
        iban
        nombre
        profesionalID
      }
    }
  }
`;

export const PROFESSIONAL_RATING_QUERY = gql`
  query getProfessionalRating($id: ID!) {
    getProfessionalRating(id: $id) {
      message
      success
      list {
        id
        coment
        rate
        updated_at
        customer {
          id
          ciudad
          nombre
          apellidos
          avatar
        }
      }
    }
  }
`;
