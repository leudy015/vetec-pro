import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';

/////////Screens//////////////

import Home from './screens/Home';
import Landing from './screens/Landing';
import Login from './screens/Login';
import Register from './screens/Register';
import Recovery from './screens/RecoveryPassword';
import Cobros from './screens/Cobros';
import Maps from './screens/Maps';
import Chats from './screens/Chats';
import Profile from './screens/Profile';
import DetailsUsuario from './screens/DetailsUsuario';
import UpdateProfile from './screens/UpdateProfile';
import DetailsMascota from './screens/DetailsMascora';
import DetailsConsulta from './screens/Detailsconsulta';
import Notifications from './screens/Notifications';
import Configuraciones from './screens/Configuraciones';
import ChatsScreens from './screens/messagechat';
import Deposito from './screens/Depositos';
import Opiniones from './screens/Opiniones';
import Pacientes from './screens/Mispacientes';
import CallScreen from './screens/CallScreen';
import CheckConnection from './screens/CheckConnection';
import VerifyPhone from './screens/VerifyPhone';
import Entrecode from './screens/EnterCode';

///////////end//////////////

const AuthAppNavigator = createStackNavigator(
  {
    Home,
    Cobros,
    Maps,
    CallScreen,
    CheckConnection,
    VerifyPhone,
    Entrecode,
    Chats,
    Profile,
    Pacientes,
    DetailsUsuario,
    UpdateProfile,
    DetailsMascota,
    Landing,
    Login,
    Register,
    Recovery,
    DetailsConsulta,
    Notifications,
    Configuraciones,
    ChatsScreens,
    Deposito,
    Opiniones,
  },
  {
    headerMode: 'none',
    defaultNavigationOptions: {
      ...TransitionPresets.ModalTransition,
    },
  },

  {
    initialRouteName: Home,
  },
);

const UnAuthAppNavigator = createStackNavigator(
  {
    Landing,
    Home,
    Login,
    VerifyPhone,
    Entrecode,
    CallScreen,
    CheckConnection,
    Register,
    Recovery,
    Cobros,
    Maps,
    Chats,
    Profile,
    DetailsUsuario,
    UpdateProfile,
    DetailsMascota,
    Pacientes,
    DetailsConsulta,
    Notifications,
    Configuraciones,
    ChatsScreens,
    Deposito,
    Opiniones,
  },
  {
    headerMode: 'none',
    defaultNavigationOptions: {
      ...TransitionPresets.ModalTransition,
    },
  },

  {
    initialRouteName: Landing,
  },
);

export const AuthSwitch = createSwitchNavigator({
  Auth: AuthAppNavigator,
});

export const AuthContainer = createAppContainer(AuthSwitch);

export const UnAuthSwitch = createSwitchNavigator({
  UnAuth: UnAuthAppNavigator,
});

export const UnAuthContainer = createAppContainer(UnAuthSwitch);
