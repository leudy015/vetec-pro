import axios from 'axios';

export const axiosPost = async (url, data) => {
  const header = {Authorization: `${global.token}`};
  return axios.post(url, data, {headers: header});
};

export const axiosGet = async url => {
  const header = {Authorization: `${global.token}`};
  return axios.get(url, {headers: header});
};

export const axiosPut = async (url, data) => {
  const header = {Authorization: `${global.token}`};
  return axios.put(url, data, {headers: header});
};

export const axiosPatch = async (url, data) => {
  const header = {Authorization: `${global.token}`};
  return axios.patch(url, data, {headers: header});
};
