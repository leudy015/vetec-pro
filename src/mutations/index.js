import gql from 'graphql-tag';

export const AUTENTICAR_PROFESIONAL = gql`
  mutation autenticarProfesional($email: String!, $password: String!) {
    autenticarProfesional(email: $email, password: $password) {
      success
      message
      data {
        token
        id
        verifyPhone
      }
    }
  }
`;

export const NUEVO_PROFESIONAL = gql`
  mutation crearProfesional($input: ProfesionalInput) {
    crearProfesional(input: $input) {
      success
      message
      data {
        id
        nombre
        apellidos
        email
      }
    }
  }
`;

export const ACTUALIZAR_PROFESIONAL = gql`
  mutation actualizarProfesional($input: ActualizarProfesionalInput) {
    actualizarProfesional(input: $input) {
      id
      email
      category_id
      nombre
      apellidos
      ciudad
      telefono
      notificacion
      isAvailable
      avatar
      profesion
      experiencia
      descricion
      Precio
    }
  }
`;

export const UPLOAD_FILE = gql`
  mutation singleUpload($imgBlob: Upload) {
    singleUpload(file: $imgBlob) {
      filename
    }
  }
`;

export const ELIMINAR_PROFESIONAL = gql`
  mutation eliminarProfesional($id: ID!) {
    eliminarProfesional(id: $id) {
      success
      message
    }
  }
`;

export const READ_MESSAGE = gql`
  mutation readMessage($conversationID: ID!) {
    readMessage(conversationID: $conversationID) {
      success
      message
    }
  }
`;

export const DELETE_CONVERSATION = gql`
  mutation deleteConversation($id: ID!) {
    deleteConversation(id: $id) {
      success
      message
    }
  }
`;

export const CREAR_MODIFICAR_CONSULTA = gql`
  mutation crearModificarConsulta($input: ConsultaCreationInput) {
    crearModificarConsulta(input: $input) {
      id
      cupon
      nota
      aceptaTerminos
      endDate
      time
      cantidad
      descuento {
        clave
        descuento
        tipo
      }
      usuario {
        id
      }
      profesional {
        id
        email
        nombre
        apellidos
        ciudad
        telefono
        avatar
        profesion
        experiencia
        descricion
        Precio
        notificacion
        isProfesional
        isVerified
        isAvailable
        lastTime
        connected
      }
      estado
      status
      progreso
      created_at
      mascota {
        id
        name
        age
        usuario
        avatar
      }
    }
  }
`;

export const PROFESSIONAL_CONSULTA_PROCEED = gql`
  mutation consultaProceed(
    $consultaID: ID!
    $estado: String!
    $progreso: String!
    $status: String!
    $nota: String
    $coment: String
    $rate: Int
    $descripcionproblem: String
  ) {
    consultaProceed(
      consultaID: $consultaID
      estado: $estado
      progreso: $progreso
      status: $status
      nota: $nota
      coment: $coment
      rate: $rate
      descripcionproblem: $descripcionproblem
    ) {
      success
      message
    }
  }
`;

export const READ_NOTIFICATION = gql`
  mutation readNotification($notificationId: ID!) {
    readNotification(notificationId: $notificationId) {
      success
      message
    }
  }
`;

export const CREATE_NOTIFICATION = gql`
  mutation createNotification($input: NotificationInput) {
    createNotification(input: $input) {
      success
      message
    }
  }
`;

export const NUEVO_DIAGNOSTICO = gql`
  mutation crearDiagnostico($input: Diagnosticoinput) {
    crearDiagnostico(input: $input) {
      success
      message
      data {
        profesional {
          nombre
          apellidos
          profesion
          avatar
        }
        diagnostico
        mascota
      }
    }
  }
`;

export const ELIMINAR_DEPOSITO = gql`
  mutation eliminarDeposito($id: ID!) {
    eliminarDeposito(id: $id) {
      success
      message
    }
  }
`;

export const CREAR_DEPOSTIO = gql`
  mutation crearDeposito($input: DepositoInput) {
    crearDeposito(input: $input) {
      success
      message
      data {
        id
        fecha
        estado
        total
        profesionalID
        created_at
      }
    }
  }
`;

export const NUEVO_PAGO = gql`
  mutation crearPago($input: PagoInput) {
    crearPago(input: $input) {
      success
      message
      data {
        id
        nombre
        iban
        profesionalID
      }
    }
  }
`;

export const ELIMINAR_PAGO = gql`
  mutation eliminarPago($id: ID!) {
    eliminarPago(id: $id) {
      success
      message
    }
  }
`;
