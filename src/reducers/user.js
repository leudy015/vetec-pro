import { SET_USER, RESET_USER} from './../redux/type';

const userInitialState = {
    user:{

    }
  };
  

  export default (state = userInitialState, action) => {
    switch (action.type) {
      case SET_USER: return { ...state, user: action.payload }
      case RESET_USER: return { ...state, user: {} }
      
      default: return state
    }
  }