const user = {
  USER_TYPE: {
    BUYER: 'BUYER',
    SELLER: 'SELLER',
  },
};

export {user};
