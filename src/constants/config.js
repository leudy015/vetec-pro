export const NETWORK_INTERFACE = 'https://server.vetec.es/graphql';
export const NETWORK_INTERFACE_LINK = 'https://server.vetec.es';
export const NETWORK_INTERFACE_LINK_AVATAR =
  'https://server.vetec.es/assets/images/';
