
import LandingImg from './../assets/image/landing.png'

import CALL from '../assets/image/call.png'
import CALL_ACCEPT from '../assets/image/accept.png'
import CALL_END from '../assets/image/decline.png'
import CAM_OFF from '../assets/image/cam-off.png'
import CHECKMARK from '../assets/image/check.png'
import CLOSE from '../assets/image/close.png'
import EXIT from '../assets/image/exit.png'
import INFO from '../assets/image/info.png'
import LOGO from '../assets/image/logo.png'
import MIC_OFF from '../assets/image/mic.png'
import SEARCH from '../assets/image/search.png'
import SPEAKER from '../assets/image/speaker.png'
import SWITCH_CAMERA from '../assets/image/switch-camera.png'
import VIDEO_ACCEPT from '../assets/image/video-accept.png'
import VIDEO_CALL from '../assets/image/videocall.png'


export const image ={
  LandingImg,
  CALL,
  CALL_ACCEPT,
  CALL_END,
  CAM_OFF,
  CHECKMARK,
  CLOSE,
  EXIT,
  INFO,
  LOGO,
  MIC_OFF,
  SEARCH,
  SPEAKER,
  SWITCH_CAMERA,
  VIDEO_ACCEPT,
  VIDEO_CALL
  }