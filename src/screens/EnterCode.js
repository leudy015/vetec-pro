import React, {useState} from 'react';
import {View, TouchableOpacity, Alert} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {colors, dimensions} from './../themes';
import {CustomText} from './../components/CustomText';
import {withApollo} from 'react-apollo';
import Navigation from './../services/NavigationService';
import Headers from '../components/Header';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {LineDotsLoader} from 'react-native-indicator';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import {useNavigationParam} from 'react-navigation-hooks';
import {NETWORK_INTERFACE_LINK} from './../constants/config';
import CountDown from 'react-native-countdown-component';

function Entreyourcode() {
  const [Loading, setLoading] = useState(false);
  const [value, setValue] = useState('');

  const data = useNavigationParam('data');

  const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  console.log(data.data);

  const sendCode = async () => {
    setLoading(true);
    fetch(
      `${NETWORK_INTERFACE_LINK}/verify-code-profesional?phone=${
        data.phone
      }&code=${value}&id=${data.id}`,
    )
      .then(res => {
        if (res.ok) {
          console.log('>>>>>>>>>>>', res);
          setLoading(false);
          if (data.data.fromSocial) {
            Navigation.navigate('Home');
          } else if (data.data.fromProfile) {
            Navigation.navigate('Profile', {data: data.data.id});
          } else {
            Navigation.navigate('Login');
          }
        } else {
          Alert.alert(
            'Hubo un error con tu código',
            'Hubo un error con tu código por favor vuelve a intentarlo.',
            [
              {
                text: 'Volver a enviar',
                onPress: () => Navigation.goBack(null),
              },
            ],

            {cancelable: false},
          );
        }
      })
      .catch(err => console.log(err));
  };

  const finisehd = () => {
    Alert.alert(
      'Se ha vencido el código',
      'El código enviado se ha vencido para verificar tu teléfono vuelve a intentarlo.',
      [
        {
          text: 'Volver a enviar',
          onPress: () => Navigation.goBack(null),
        },
      ],

      {cancelable: false},
    );
  };

  const dynamicStyles = new DynamicStyleSheet({
    container: {
      flex: 1,
    },
    formView: {
      marginHorizontal: dimensions.Width(8),
      marginTop: dimensions.Height(0),
      marginBottom: dimensions.Height(15),
    },
    rememberMeView: {
      marginTop: dimensions.Height(2),
      flexDirection: 'row',
    },
    rememberText: {
      fontSize: dimensions.FontSize(18),
    },
    signupButtonContainer: {
      marginTop: dimensions.Height(5),
      alignSelf: 'center',
    },
    buttonView: {
      backgroundColor:
        value.length === 4
          ? new DynamicValue(colors.main, colors.main)
          : colors.rgb_153,
      width: dimensions.Width(84),
      borderRadius: dimensions.Width(8),
    },
    buttonTitle: {
      alignSelf: 'center',
      paddingVertical: dimensions.Height(2),
      paddingHorizontal: dimensions.Width(5),
      color: colors.white,
      fontWeight: '400',
      fontSize: dimensions.FontSize(17),
    },
    contenedor: {
      width: dimensions.ScreenWidth,
      height: dimensions.ScreenHeight,
      backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    },
    h1: {
      fontSize: dimensions.FontSize(30),
      fontWeight: 'bold',
      textAlign: 'left',
    },

    root: {flex: 1, padding: 20},
    title: {textAlign: 'center', fontSize: 30},
    codeFiledRoot: {marginTop: 20},
    cell: {
      width: 50,
      height: 50,
      lineHeight: 48,
      fontSize: 24,
      borderRadius: 5,
      borderWidth: 1,
      borderColor: new DynamicValue(colors.back_dark, colors.rgb_153),
      textAlign: 'center',
    },
    focusCell: {
      borderColor: colors.main,
    },
  });

  const styles = useDynamicStyleSheet(dynamicStyles);

  const CELL_COUNT = 4;
  return (
    <View style={styles.container}>
      <View>
        <Headers
          navigation={Navigation}
          title="Verifica tu teléfono"
          back={true}
          rigth={true}
        />
      </View>
      <View style={styles.contenedor}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="always"
          showsVerticalScrollIndicator={false}
          style={{marginBottom: dimensions.Height(10)}}>
          <View
            style={{
              marginTop: dimensions.Height(5),
              marginLeft: dimensions.Width(8),
              marginBottom: dimensions.Height(5),
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={styles.h1}>
              Verifica tu
            </CustomText>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={styles.h1}>
              teléfono
            </CustomText>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={[styles.secondaryText, {marginTop: 30}]}>
              Introduce el código de verificación enviado al teléfono {''}
              {data.phone}.
            </CustomText>
          </View>
          <View style={styles.formView}>
            <CodeField
              ref={ref}
              {...props}
              value={value}
              onChangeText={setValue}
              cellCount={CELL_COUNT}
              rootStyle={styles.codeFiledRoot}
              keyboardType="number-pad"
              textContentType="oneTimeCode"
              renderCell={({index, symbol, isFocused}) => (
                <CustomText
                  light={colors.back_dark}
                  dark={colors.rgb_153}
                  key={index}
                  style={[styles.cell, isFocused && styles.focusCell]}
                  onLayout={getCellOnLayoutHandler(index)}>
                  {symbol || (isFocused ? <Cursor /> : null)}
                </CustomText>
              )}
            />
            <View style={{flexDirection: 'row'}}>
              <CustomText
                style={{
                  color: colors.rgb_153,
                  fontWeight: '200',
                  fontSize: 14,
                  paddingTop: 30,
                }}>
                El código vence en:{' '}
              </CustomText>

              <CountDown
                until={60 * 5 + 0}
                size={8}
                onFinish={() => finisehd()}
                digitStyle={{
                  backgroundColor: 'transparent',
                  marginTop: 29,
                }}
                digitTxtStyle={{
                  color: colors.rgb_153,
                  fontWeight: '200',
                  fontSize: 14,
                }}
                timeToShow={['M', 'S']}
                timeLabels={false}
              />
            </View>
            <View style={styles.signupButtonContainer}>
              {Loading ? (
                <LineDotsLoader color={colors.main} />
              ) : (
                <TouchableOpacity
                  style={styles.buttonView}
                  onPress={() => (value.length === 4 ? sendCode() : '')}>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.rgb_153}
                    style={styles.buttonTitle}>
                    Verificar
                  </CustomText>
                </TouchableOpacity>
              )}
            </View>
            <View />
          </View>
        </KeyboardAwareScrollView>
      </View>
    </View>
  );
}

export default withApollo(Entreyourcode);
