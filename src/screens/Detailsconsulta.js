import React, {useState} from 'react';
import {
  View,
  Image,
  Alert,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Modal,
  SafeAreaView,
} from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import {useNavigationParam} from 'react-navigation-hooks';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import {CustomText} from '../components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {Avatar} from 'react-native-elements';
import moment from 'moment';
import 'moment/locale/es';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import {NUEVO_DIAGNOSTICO} from './../mutations';
import {TextareaItem} from '@ant-design/react-native';
import {Mutation} from 'react-apollo';
import {Button} from './../components/Button';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Toast from 'react-native-simple-toast';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const window = Dimensions.get('window');

function DetailsConsulta() {
  const [val, setVal] = useState('');
  const [modalVisible, setModalVisible] = useState(false);

  const onChange = val => {
    console.log(val);
    setVal(val);
  };

  const openModal = () => {
    setModalVisible(true);
  };

  const styles = useDynamicStyleSheet(dynamicStyles);
  const data = useNavigationParam('data');

  const item = data.mascota;

  const items = {
    item,
  };

  const handlePublish = async crearDiagnostico => {
    if (!val) {
      Alert.alert(
        'Error debes añadir un diagnóstico',
        'Error debes añadir un diagnóstico',
        [
          {
            text: 'Ok',
            onPress: () => console.log('calcel'),
          },
        ],

        {cancelable: false},
      );
      return null;
    } else {
      await crearDiagnostico({
        variables: {
          input: {
            profesional: {
              nombre: data.profesional.nombre,
              apellidos: data.profesional.apellidos,
              profesion: data.profesional.profesion,
              avatar: data.profesional.avatar,
            },
            mascota: data.mascota.id,
            diagnostico: val,
          },
        },
      })
        .then(async () => {
          Toast.showWithGravity(
            'Consulta diagnosticada éxitosamente',
            Toast.LONG,
            Toast.TOP,
          );
          setTimeout(() => {
            setTimeout(() => {
              setModalVisible(!modalVisible);
            }, 200);
          });
          console.log('todo listo');
        })
        .catch(err => {
          console.log(
            'Algo salió mal. Por favor intente nuevamente en un momento.',
            err,
          );
        });
    }
  };

  return (
    <View style={styles.container}>
      <ParallaxScrollView
        showsVerticalScrollIndicator={false}
        headerBackgroundColor={new DynamicValue(colors.white, colors.back_dark)}
        stickyHeaderHeight={STICKY_HEADER_HEIGHT}
        parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
        backgroundSpeed={10}
        renderBackground={() => (
          <View key="background">
            <Image
              blurRadius={10}
              style={styles.background}
              source={{
                uri: NETWORK_INTERFACE_LINK_AVATAR + data.usuario.avatar,
              }}
            />
          </View>
        )}
        renderForeground={() => (
          <View key="parallax-header" style={styles.parallaxHeader}>
            <Image
              style={styles.avatar}
              source={{
                uri: NETWORK_INTERFACE_LINK_AVATAR + data.usuario.avatar,
              }}
            />
          </View>
        )}
        renderFixedHeader={() => (
          <View key="fixed-header" style={styles.fixedSection}>
            <View style={styles.fixedSectionText}>
              <TouchableOpacity
                onPress={() => Navigation.goBack(null)}
                style={styles.back}>
                <CustomText>
                  <Icon
                    name="close"
                    type="AntDesign"
                    size={20}
                    style={styles.icon}
                  />
                </CustomText>
              </TouchableOpacity>
            </View>
          </View>
        )}
        renderStickyHeader={() => (
          <View key="sticky-header" style={styles.stickySection}>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={styles.stickySectionText}>
              {data.usuario.nombre} {data.usuario.apellidos}
            </CustomText>
          </View>
        )}>
        <View style={styles.contenedor}>
          <View
            style={{
              marginTop: dimensions.Height(5),
              paddingHorizontal: dimensions.Width(4),
            }}>
            <View style={{flexDirection: 'row'}}>
              <CustomText
                numberOfLines={1}
                light={colors.back_dark}
                dark={colors.white}
                style={styles.title}>
                {data.usuario.nombre} {data.usuario.apellidos}
              </CustomText>
              <CustomText
                style={{
                  color: colors.rgb_102,
                  fontWeight: '300',
                  fontSize: dimensions.FontSize(18),
                  marginLeft: 'auto',
                  marginRight: dimensions.Width(2),
                }}>
                {data.profesional.Precio}€ /Total
              </CustomText>
            </View>

            <View
              style={{
                flexDirection: 'row',
                paddingBottom: dimensions.Height(2),
              }}>
              <CustomText
                style={{
                  color: colors.rgb_102,
                  fontWeight: '300',
                  fontSize: dimensions.FontSize(14),
                  marginLeft: 'auto',
                  marginRight: dimensions.Width(2),
                }}>
                <Icon
                  name="enviromento"
                  type="AntDesign"
                  size={14}
                  color={colors.rgb_102}
                />{' '}
                {data.usuario.ciudad}
              </CustomText>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: dimensions.Width(4),
              borderTopColor: colors.rgb_235,
              borderTopWidth: 0.5,
            }}>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={styles.prof}>
              Estado de la consulta
            </CustomText>
            <CustomText
              style={{
                color: colors.rgb_102,
                fontWeight: '300',
                fontSize: dimensions.FontSize(14),
                marginLeft: 'auto',
                marginRight: dimensions.Width(2),
                marginTop: 8,
              }}>
              {data.estado}
            </CustomText>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: dimensions.Width(4),
              borderBottomColor: colors.rgb_235,
              borderBottomWidth: 0.5,
              paddingBottom: 10,
            }}>
            <CustomText
              light={colors.rgb_153}
              dark={colors.rgb_153}
              style={styles.prof}>
              Fecha
            </CustomText>
            <CustomText
              style={{
                color: colors.rgb_102,
                fontWeight: '300',
                fontSize: dimensions.FontSize(14),
                marginLeft: 'auto',
                marginRight: dimensions.Width(2),
                marginTop: 8,
              }}>
              {moment(Number(data.created_at)).format('LLL')}
            </CustomText>
          </View>

          <View
            style={{
              marginTop: dimensions.Height(3),
              paddingHorizontal: dimensions.Width(4),
              alignSelf: 'center',
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.white}
              style={styles.subtitle}>
              Mascota
            </CustomText>

            <TouchableOpacity
              style={styles.cardinfo}
              onPress={() =>
                Navigation.navigate('DetailsMascota', {data: items})
              }>
              <View
                style={{
                  alignSelf: 'center',
                  marginLeft: dimensions.Width(4),
                }}>
                <Avatar
                  rounded
                  size={50}
                  source={{
                    uri: NETWORK_INTERFACE_LINK_AVATAR + data.mascota.avatar,
                  }}
                  containerStyle={styles.avatarmascota}
                />
              </View>
              <View
                style={{
                  alignSelf: 'center',
                  marginLeft: dimensions.Width(4),
                }}>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.white}
                  style={styles.name}>
                  {data.mascota.name}
                </CustomText>
                <CustomText light={colors.back_dark} dark={colors.rgb_153}>
                  {moment(data.mascota.age).format('ll')}
                </CustomText>
              </View>
              <View
                style={{
                  alignSelf: 'center',
                  marginLeft: 'auto',
                  marginRight: dimensions.Width(4),
                }}>
                <Icon
                  name="right"
                  type="AntDesign"
                  size={20}
                  color={colors.rgb_153}
                />
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              paddingHorizontal: dimensions.Width(4),
              marginTop: dimensions.Height(3),
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.white}
              style={styles.subtitle}>
              Sintomas o problema con la mascota
            </CustomText>

            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={{
                marginTop: dimensions.Height(2),
                fontSize: dimensions.FontSize(16),
              }}>
              {data.nota}
            </CustomText>
          </View>
        </View>
      </ParallaxScrollView>
      {data.estado === 'Completada' ? (
        <View style={styles.fixs}>
          {data.estado === 'Completada' ? (
            <View style={styles.signupButtonContainer}>
              <Button
                light={colors.white}
                dark={colors.white}
                containerStyle={styles.buttonView}
                onPress={() => openModal()}
                title="Diagnósticar"
                titleStyle={styles.buttonTitle}
              />
            </View>
          ) : null}

          <Modal
            style={{height: dimensions.Height(80)}}
            animationType="slide"
            presentationStyle="formSheet"
            transparent={false}
            visible={modalVisible}
            onRequestClose={() => Alert.alert('Seguro que deseas salir')}>
            <View style={styles.container}>
              <View style={{height: dimensions.Height(100)}}>
                <SafeAreaView style={styles.headers}>
                  <View style={{flexDirection: 'row', marginTop: 20}}>
                    <View style={{alignItems: 'flex-start', marginLeft: 10}} />
                    <View style={{marginLeft: 'auto', flexDirection: 'row'}}>
                      <TouchableOpacity
                        onPress={() => setModalVisible(!modalVisible)}>
                        <CustomText
                          light={colors.rgb_153}
                          dark={colors.rgb_153}
                          style={{marginRight: 0}}>
                          <Icon
                            type="AntDesign"
                            name="close"
                            size={25}
                            color={colors.ERROR}
                          />
                        </CustomText>
                      </TouchableOpacity>
                    </View>
                  </View>
                </SafeAreaView>
                <ScrollView showsVerticalScrollIndicator={false}>
                  <View
                    style={{
                      flex: 1,
                      marginTop: dimensions.Height(4),
                      alignItems: 'center',
                    }}>
                    <CustomText
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={{
                        fontSize: dimensions.FontSize(18),
                        fontWeight: '200',
                        paddingHorizontal: 15,
                      }}>
                      Aquí puede dejar apuntes sobre la consulta
                    </CustomText>
                    <View
                      style={{
                        marginTop: 20,
                        borderBottomWidth: 0.5,
                        borderBottomColor: colors.rgb_153,
                        paddingBottom: 15,
                      }}>
                      <Avatar
                        size={60}
                        rounded
                        source={{
                          uri:
                            NETWORK_INTERFACE_LINK_AVATAR + data.mascota.avatar,
                        }}
                      />

                      <CustomText
                        light={colors.rgb_153}
                        dark={colors.rgb_153}
                        style={{
                          fontSize: dimensions.FontSize(18),
                          fontWeight: '200',
                          marginTop: 10,
                          textAlign: 'center',
                        }}>
                        {data.mascota.name}
                      </CustomText>
                    </View>

                    <Mutation mutation={NUEVO_DIAGNOSTICO}>
                      {(crearDiagnostico, {loading, error, data}) => {
                        if (error)
                          console.log('NUEVO_PRODUCT in error : ', error);
                        return (
                          <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
                            <View style={{marginTop: dimensions.Height(5)}}>
                              <TextareaItem
                                rows={5}
                                placeholder="Diagnóstico"
                                count={260}
                                value={val}
                                onChange={onChange}
                                style={{
                                  backgroundColor: 'transparent',
                                  color: colors.rgb_153,
                                  width: dimensions.Width(90),
                                }}
                                placeholderTextColor={colors.rgb_153}
                              />
                            </View>
                            <View style={{alignSelf: 'center'}}>
                              <View style={styles.signupButtonContainer1}>
                                <Button
                                  light={colors.white}
                                  dark={colors.white}
                                  containerStyle={styles.buttonView}
                                  onPress={() =>
                                    handlePublish(crearDiagnostico)
                                  }
                                  title="Enviar"
                                  titleStyle={styles.buttonTitle}
                                />
                              </View>
                            </View>
                          </KeyboardAwareScrollView>
                        );
                      }}
                    </Mutation>
                  </View>
                </ScrollView>
              </View>
            </View>
          </Modal>
        </View>
      ) : null}
    </View>
  );
}

const AVATAR_SIZE = 140;
const ROW_HEIGHT = 140;
const PARALLAX_HEADER_HEIGHT = 330;
const STICKY_HEADER_HEIGHT = 90;

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.Width(100),
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  imagenbg: {
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: PARALLAX_HEADER_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    width: dimensions.ScreenWidth,
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'flex-end',
  },
  stickySectionText: {
    fontSize: 20,
    margin: 10,
    ...ifIphoneX(
      {
        left: 50,
      },
      {
        left: 45,
      },
    ),
    marginTop: 0,
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 5,
      },
    ),
  },
  fixedSection: {
    position: 'absolute',
    bottom: 0,
    right: 'auto',
    left: 10,
    flexDirection: 'row',
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 10,
      },
    ),
  },
  fixedSectionText: {
    fontSize: 16,
    marginRight: dimensions.Width(2),
  },

  fixedSectionText1: {
    fontSize: 16,
    marginRight: 15,
    width: 35,
    height: 35,
    borderRadius: 25,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  parallaxHeader: {
    alignItems: 'center',
    flex: 1,
    width: dimensions.ScreenWidth,
    flexDirection: 'column',
    paddingTop: dimensions.Height(8),
    paddingHorizontal: dimensions.Width(2),
  },
  avatar: {
    borderRadius: 20,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    ...ifIphoneX(
      {
        marginTop: dimensions.Height(2),
      },
      {
        marginTop: dimensions.Height(6),
      },
    ),
  },
  sectionSpeakerText: {
    color: 'white',
    fontSize: 24,
    paddingVertical: 5,
  },
  sectionTitleText: {
    color: 'white',
    fontSize: 14,
    textAlign: 'left',
    paddingVertical: 5,
  },
  name: {
    marginTop: dimensions.Height(0),
    fontSize: dimensions.FontSize(24),
  },
  row: {
    overflow: 'hidden',
    paddingHorizontal: 10,
    height: ROW_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderColor: '#ccc',
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  contenedor: {
    width: dimensions.ScreenWidth,
    height: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
    marginTop: dimensions.Height(-3),
  },

  icon: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  title: {
    fontSize: dimensions.FontSize(18),
    fontWeight: 'bold',
    width: dimensions.Width(50),
  },

  subtitle: {
    fontSize: dimensions.FontSize(20),
    fontWeight: '400',
  },
  prof: {
    fontSize: dimensions.FontSize(18),
    fontWeight: '300',
    marginTop: dimensions.Height(1),
  },

  des: {
    fontSize: dimensions.FontSize(16),
    fontWeight: '300',
    marginTop: dimensions.Height(1),
  },

  qubo: {
    flexDirection: 'column',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    padding: 10,
    width: 90,
    height: 90,
    borderRadius: 15,
    marginRight: 20,
  },

  cardop: {
    width: dimensions.Width(92),
    height: dimensions.Height(10),
    paddingHorizontal: dimensions.Width(2),
    flexDirection: 'row',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginTop: dimensions.Height(3),
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  rating: {
    alignSelf: 'center',
    fontSize: dimensions.FontSize(50),
    fontWeight: 'bold',
  },

  sub_item_text: {
    fontSize: dimensions.FontSize(20),
    fontWeight: '300',
    marginLeft: 10,
  },

  back: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    padding: 10,
    borderRadius: 10,
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(80),
    borderRadius: dimensions.Width(8),
    marginHorizontal: dimensions.Height(1),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1.5),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },

  cardinfo: {
    width: dimensions.Width(94),
    height: 90,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginTop: dimensions.Height(2),
    borderRadius: 10,
    flexDirection: 'row',
  },

  fixs: {
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    height: dimensions.Height(15),
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 15,
    shadowOpacity: 0.2,
    elevation: 3,
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: dimensions.Height(1),
  },

  signupButtonContainer1: {
    marginTop: dimensions.Height(3),
  },
  signupButtonContainer: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
});

export default DetailsConsulta;
