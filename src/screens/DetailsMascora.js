import React, {useState} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList,
  ActivityIndicator,
  Modal,
  SafeAreaView,
  ScrollView,
  Alert,
} from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import {useNavigationParam} from 'react-navigation-hooks';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import {CustomText} from '../components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import moment from 'moment';
import {LineDotsLoader} from 'react-native-indicator';
import {GET_DIAGNOSTICO} from '../query/index';
import {Query, Mutation} from 'react-apollo';
import NoData from '../components/NoData';
import {Button} from './../components/Button';
import Toast from 'react-native-simple-toast';
import {NUEVO_DIAGNOSTICO} from './../mutations';
import {TextareaItem} from '@ant-design/react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Avatar} from 'react-native-elements';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const window = Dimensions.get('window');

export default function DetailsPro() {
  const [val, setVal] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const styles = useDynamicStyleSheet(dynamicStyles);
  const data = useNavigationParam('data');

  const onChange = val => {
    console.log(val);
    setVal(val);
  };

  const openModal = () => {
    setModalVisible(true);
  };

  const handlePublish = async crearDiagnostico => {
    if (!val) {
      Alert.alert(
        'Error debes añadir un diagnóstico',
        'Error debes añadir un diagnóstico',
        [
          {
            text: 'Ok',
            onPress: () => console.log('calcel'),
          },
        ],

        {cancelable: false},
      );
      return null;
    } else {
      await crearDiagnostico({
        variables: {
          input: {
            profesional: {
              nombre: data.prof.nombre,
              apellidos: data.prof.apellidos,
              profesion: data.prof.profesion,
              avatar: data.prof.avatar,
            },
            mascota: data.item.id,
            diagnostico: val,
          },
        },
      })
        .then(async () => {
          Toast.showWithGravity(
            'Consulta diagnosticada éxitosamente',
            Toast.LONG,
            Toast.TOP,
          );
          setTimeout(() => {
            setTimeout(() => {
              setModalVisible(!modalVisible);
            }, 200);
          });
          console.log('todo listo');
        })
        .catch(err => {
          console.log(
            'Algo salió mal. Por favor intente nuevamente en un momento.',
            err,
          );
        });
    }
  };

  const _renderIntem = ({item}) => {
    return (
      <View style={styles.card}>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: dimensions.Width(2),
          }}>
          <View style={styles.console}>
            <Image
              source={{
                uri: NETWORK_INTERFACE_LINK_AVATAR + item.profesional.avatar,
              }}
              style={styles.avatarpro}
            />
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginLeft: dimensions.Width(0)}}>
              <CustomText
                numberOfLines={1}
                light={colors.rgb_102}
                dark={colors.light_white}
                style={styles.name_pro}>
                {item.profesional.nombre} {item.profesional.apellidos}
              </CustomText>
              <CustomText
                numberOfLines={1}
                light={colors.rgb_102}
                dark={colors.light_white}
                style={[
                  styles.title_pof,
                  {color: colors.rgb_153, fontWeight: '200'},
                ]}>
                {item.profesional.profesion}
              </CustomText>
              <CustomText style={[styles.text, {color: colors.main1}]}>
                {moment(Number(item.created_at)).format('LL')}
              </CustomText>
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: dimensions.Width(2),
          }}>
          <CustomText
            numberOfLines={5}
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={styles.des}>
            {item.diagnostico}
          </CustomText>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <ParallaxScrollView
        showsVerticalScrollIndicator={false}
        headerBackgroundColor={new DynamicValue(colors.white, colors.back_dark)}
        stickyHeaderHeight={STICKY_HEADER_HEIGHT}
        parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
        backgroundSpeed={10}
        renderBackground={() => (
          <View key="background">
            <Image
              blurRadius={10}
              style={styles.background}
              source={{uri: NETWORK_INTERFACE_LINK_AVATAR + data.item.avatar}}
            />
          </View>
        )}
        renderForeground={() => (
          <View key="parallax-header" style={styles.parallaxHeader}>
            <Image
              style={styles.avatar}
              source={{uri: NETWORK_INTERFACE_LINK_AVATAR + data.item.avatar}}
            />
          </View>
        )}
        renderFixedHeader={() => (
          <View key="fixed-header" style={styles.fixedSection}>
            <View style={styles.fixedSectionText}>
              <TouchableOpacity
                onPress={() => Navigation.goBack(null)}
                style={styles.back}>
                <Icon
                  name="close"
                  type="AntDesign"
                  size={20}
                  style={styles.icon}
                />
              </TouchableOpacity>
            </View>
          </View>
        )}
        renderStickyHeader={() => (
          <View key="sticky-header" style={styles.stickySection}>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={styles.stickySectionText}>
              {data.item.name}
            </CustomText>
          </View>
        )}>
        <View style={styles.contenedor}>
          <View
            style={{
              marginTop: dimensions.Height(2),
              marginBottom: dimensions.Height(4),
              paddingHorizontal: dimensions.Height(2),
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.light_white}
              style={styles.title}>
              {data.item.name}
            </CustomText>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={styles.name1}>
              {moment(data.item.age).format('ll')}
            </CustomText>
          </View>

          <View style={{justifyContent: 'center', alignSelf: 'center'}}>
            <View style={styles.item}>
              <Icon
                name="dog"
                type="MaterialCommunityIcons"
                size={30}
                color={colors.main}
              />
              <CustomText
                light={colors.back_dark}
                dark={colors.rgb_153}
                style={styles.sub_item_text}>
                {data.item.especie}
              </CustomText>
              <Icon
                name="right"
                type="AntDesign"
                size={15}
                color={colors.rgb_153}
                style={{marginLeft: 'auto'}}
              />
            </View>
            <View style={styles.item}>
              <Icon
                name="tago"
                type="AntDesign"
                size={30}
                color={colors.main}
              />
              <CustomText
                light={colors.back_dark}
                dark={colors.rgb_153}
                style={styles.sub_item_text}>
                {data.item.raza}
              </CustomText>
              <Icon
                name="right"
                type="AntDesign"
                size={15}
                color={colors.rgb_153}
                style={{marginLeft: 'auto'}}
              />
            </View>
            <View style={styles.item}>
              <Icon
                name="transgender"
                type="FontAwesome"
                size={30}
                color={colors.main}
              />
              <CustomText
                light={colors.back_dark}
                dark={colors.rgb_153}
                style={styles.sub_item_text}>
                {data.item.genero}
              </CustomText>
              <Icon
                name="right"
                type="AntDesign"
                size={15}
                color={colors.rgb_153}
                style={{marginLeft: 'auto'}}
              />
            </View>

            <View style={styles.item}>
              <Icon
                name="weight-kilogram"
                type="MaterialCommunityIcons"
                size={30}
                color={colors.main}
              />
              <CustomText
                light={colors.back_dark}
                dark={colors.rgb_153}
                style={styles.sub_item_text}>
                {data.item.peso} kg
              </CustomText>
              <Icon
                name="right"
                type="AntDesign"
                size={15}
                color={colors.rgb_153}
                style={{marginLeft: 'auto'}}
              />
            </View>
            <View style={styles.item}>
              <Icon
                name="pushpin"
                type="AntDesign"
                size={30}
                color={colors.main}
              />
              <CustomText
                light={colors.back_dark}
                dark={colors.rgb_153}
                style={styles.sub_item_text}>
                {data.item.vacunas} (Vacunas)
              </CustomText>
              <Icon
                name="right"
                type="AntDesign"
                size={15}
                color={colors.rgb_153}
                style={{marginLeft: 'auto'}}
              />
            </View>
            <View style={styles.item}>
              <Icon
                name="closecircle"
                type="AntDesign"
                size={26}
                color={colors.main}
              />
              <CustomText
                light={colors.back_dark}
                dark={colors.rgb_153}
                style={styles.sub_item_text}>
                Alergía a {data.item.alergias}
              </CustomText>
              <Icon
                name="right"
                type="AntDesign"
                size={15}
                color={colors.rgb_153}
                style={{marginLeft: 'auto'}}
              />
            </View>
          </View>
          <View style={styles.consul}>
            <View
              style={{
                marginTop: dimensions.Height(3),
                paddingHorizontal: dimensions.Width(4),
                marginBottom: dimensions.Height(10),
              }}>
              <CustomText
                light={colors.back_dark}
                dark={colors.white}
                style={styles.subtitle}>
                Historial de consultas
              </CustomText>
              <Query query={GET_DIAGNOSTICO} variables={{id: data.item.id}}>
                {({loading, error, data, refetch}) => {
                  refetch = refetch;
                  if (loading) {
                    return <ActivityIndicator />;
                  }
                  if (error) {
                    return console.log('error in mascota', error);
                  }
                  if (data) {
                    return (
                      <FlatList
                        data={
                          data && data.getDiagnostico
                            ? data.getDiagnostico.list
                            : ''
                        }
                        renderItem={item => _renderIntem(item)}
                        keyExtractor={item => item.profesional}
                        showsVerticalScrollIndicator={false}
                        ListEmptyComponent={
                          <NoData menssge="Aún no tenemos ningún historial" />
                        }
                      />
                    );
                  } else {
                    return <LineDotsLoader color={colors.main} />;
                  }
                }}
              </Query>
            </View>
          </View>
        </View>
      </ParallaxScrollView>
      {data.sus ? (
        <View style={styles.fixs}>
          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView}
              onPress={() => openModal()}
              title="Diagnósticar"
              titleStyle={styles.buttonTitle}
            />
          </View>
          <Modal
            style={{height: dimensions.Height(80)}}
            animationType="slide"
            presentationStyle="formSheet"
            transparent={false}
            visible={modalVisible}
            onRequestClose={() => Alert.alert('Seguro que deseas salir')}>
            <View style={styles.container}>
              <View style={{height: dimensions.Height(100)}}>
                <SafeAreaView style={styles.headers}>
                  <View style={{flexDirection: 'row', marginTop: 20}}>
                    <View style={{alignItems: 'flex-start', marginLeft: 10}} />
                    <View style={{marginLeft: 'auto', flexDirection: 'row'}}>
                      <TouchableOpacity
                        onPress={() => setModalVisible(!modalVisible)}>
                        <CustomText
                          light={colors.rgb_153}
                          dark={colors.rgb_153}
                          style={{marginRight: 15}}>
                          <Icon
                            type="AntDesign"
                            name="close"
                            size={25}
                            color={colors.ERROR}
                          />
                        </CustomText>
                      </TouchableOpacity>
                    </View>
                  </View>
                </SafeAreaView>
                <ScrollView showsVerticalScrollIndicator={false}>
                  <View
                    style={{
                      flex: 1,
                      marginTop: dimensions.Height(4),
                      alignItems: 'center',
                    }}>
                    <CustomText
                      light={colors.rgb_153}
                      dark={colors.rgb_153}
                      style={{
                        fontSize: dimensions.FontSize(18),
                        fontWeight: '200',
                        paddingHorizontal: 15,
                      }}>
                      Aquí puede dejar apuntes sobre la consulta
                    </CustomText>
                    <View
                      style={{
                        marginTop: 20,
                        paddingBottom: 15,
                      }}>
                      <Avatar
                        size={60}
                        rounded
                        source={{
                          uri: NETWORK_INTERFACE_LINK_AVATAR + data.item.avatar,
                        }}
                      />

                      <CustomText
                        light={colors.rgb_153}
                        dark={colors.rgb_153}
                        style={{
                          fontSize: dimensions.FontSize(18),
                          fontWeight: '200',
                          marginTop: 10,
                          textAlign: 'center',
                        }}>
                        {data.item.name}
                      </CustomText>
                    </View>

                    <Mutation mutation={NUEVO_DIAGNOSTICO}>
                      {(crearDiagnostico, {loading, error, data}) => {
                        if (error)
                          console.log('NUEVO_PRODUCT in error : ', error);
                        return (
                          <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
                            <View
                              style={{
                                marginTop: dimensions.Height(5),
                              }}>
                              <TextareaItem
                                rows={5}
                                placeholder="Diagnóstico"
                                count={260}
                                value={val}
                                onChange={onChange}
                                style={{
                                  backgroundColor: 'transparent',
                                  color: colors.rgb_153,
                                  width: dimensions.Width(90),
                                }}
                                placeholderTextColor={colors.rgb_153}
                              />
                            </View>
                            <View style={{alignSelf: 'center'}}>
                              <View style={styles.signupButtonContainer1}>
                                <Button
                                  light={colors.white}
                                  dark={colors.white}
                                  containerStyle={styles.buttonView}
                                  onPress={() =>
                                    handlePublish(crearDiagnostico)
                                  }
                                  title="Enviar"
                                  titleStyle={styles.buttonTitle}
                                />
                              </View>
                            </View>
                          </KeyboardAwareScrollView>
                        );
                      }}
                    </Mutation>
                  </View>
                </ScrollView>
              </View>
            </View>
          </Modal>
        </View>
      ) : null}
    </View>
  );
}

const AVATAR_SIZE = 140;
const ROW_HEIGHT = 140;
const PARALLAX_HEADER_HEIGHT = 330;
const STICKY_HEADER_HEIGHT = 90;

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.ScreenWidth,
    height: 'auto',
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  imagenbg: {
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'flex-end',
  },

  btncont: {
    width: dimensions.Width(91),
    padding: dimensions.Height(2),
    backgroundColor: new DynamicValue(colors.white, colors.main),
    borderWidth: 1,
    borderColor: colors.main,
    margin: dimensions.Width(4),
    borderRadius: dimensions.Width(10),
  },

  te: {
    textAlign: 'center',
    fontSize: dimensions.FontSize(18),
  },
  stickySectionText: {
    fontSize: 20,
    margin: 10,
    ...ifIphoneX(
      {
        left: 50,
      },
      {
        left: 45,
      },
    ),
    marginTop: 0,
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 5,
      },
    ),
  },
  fixedSection: {
    position: 'absolute',
    bottom: 0,
    right: 'auto',
    left: 10,
    flexDirection: 'row',
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 10,
      },
    ),
  },
  fixedSectionText: {
    fontSize: 16,
    marginRight: dimensions.Width(2),
  },

  fixedSectionText1: {
    fontSize: 16,
    marginRight: 15,
    width: 35,
    height: 35,
    borderRadius: 25,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  parallaxHeader: {
    alignItems: 'center',
    flex: 1,
    width: dimensions.ScreenWidth,
    flexDirection: 'column',
    paddingTop: dimensions.Height(8),
    paddingHorizontal: dimensions.Width(4),
  },
  avatar: {
    borderRadius: 20,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    ...ifIphoneX(
      {
        marginTop: dimensions.Height(2),
      },
      {
        marginTop: dimensions.Height(6),
      },
    ),
  },

  avatarpro: {
    borderRadius: 15,
    width: 80,
    height: 80,
  },

  console: {
    width: 90,
    height: 90,
  },
  sectionSpeakerText: {
    color: 'white',
    fontSize: 24,
    paddingVertical: 5,
  },
  sectionTitleText: {
    color: 'white',
    fontSize: 14,
    textAlign: 'left',
    paddingVertical: 5,
  },
  row: {
    overflow: 'hidden',
    paddingHorizontal: 10,
    height: ROW_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderColor: '#ccc',
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  contenedor: {
    width: dimensions.ScreenWidth,
    height: 'auto',
    minHeight: dimensions.Height(100),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
    marginTop: dimensions.Height(-3),
  },

  icon: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  title: {
    fontSize: dimensions.FontSize(28),
    fontWeight: 'bold',
    maxWidth: dimensions.Width(50),
    width: 'auto',
  },

  name_pro: {
    fontSize: dimensions.FontSize(18),
    fontWeight: 'bold',
    width: 'auto',
  },

  subtitle: {
    fontSize: dimensions.FontSize(20),
    fontWeight: '400',
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(80),
    borderRadius: dimensions.Width(8),
    marginHorizontal: dimensions.Height(1),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1.5),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },

  item: {
    width: dimensions.Width(95),
    height: 60,
    marginTop: dimensions.Height(2),
    paddingHorizontal: dimensions.Height(2),
    flexDirection: 'row',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    borderRadius: 10,
    alignContent: 'center',
    alignItems: 'center',
  },

  sub_item_text: {
    fontSize: dimensions.FontSize(20),
    fontWeight: '300',
    marginLeft: 10,
  },

  back: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    padding: 10,
    borderRadius: 10,
  },

  card: {
    width: dimensions.Width(92),
    height: 'auto',
    marginTop: dimensions.Height(2),
    paddingVertical: dimensions.Height(2),
  },
  fixs: {
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    height: dimensions.Height(12),
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 15,
    shadowOpacity: 0.2,
    elevation: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: dimensions.Height(3),
  },

  signupButtonContainer1: {
    marginTop: dimensions.Height(5),
  },
});
