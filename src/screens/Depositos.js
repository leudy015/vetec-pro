import React, {useState, useEffect} from 'react';
import {View, ScrollView, FlatList, ActivityIndicator} from 'react-native';
import {CustomText} from '../components/CustomText';
import Navigation from './../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {GET_DEPOSITOS} from '../query/index';
import {Query} from 'react-apollo';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import 'moment/locale/es';
import NoData from '../components/NoData';

export default function Depositos() {
  const [id, setId] = useState(null);

  const styles = useDynamicStyleSheet(dynamicStyles);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  const _renderItem = ({item}) => {
    return (
      <View style={styles.card}>
        <View
          style={[
            styles.subcard1,
            {
              flexDirection: 'row',
              width: dimensions.ScreenWidth,
              paddingHorizontal: dimensions.Width(4),
            },
          ]}>
          <View style={{width: dimensions.Width(30), alignItems: 'center'}}>
            <CustomText
              numberOfLines={1}
              light={colors.back_suave_dark}
              dark={colors.rgb_153}>
              {moment(Number(item.fecha)).format('LL')}
            </CustomText>
          </View>
          <View style={{width: dimensions.Width(30), alignItems: 'center'}}>
            <CustomText
              numberOfLines={1}
              light={colors.back_suave_dark}
              dark={colors.rgb_153}>
              {item.estado}
            </CustomText>
          </View>
          <View style={{width: dimensions.Width(30), alignItems: 'center'}}>
            <CustomText
              numberOfLines={1}
              light={colors.back_suave_dark}
              dark={colors.rgb_153}>
              {item.total}€
            </CustomText>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} title="Mis ingresos" back={true} />
      </View>
      <CustomText
        light={colors.black}
        dark={colors.white}
        numberOfLines={1}
        style={styles.chats}>
        Mis ingresos
      </CustomText>
      <View
        style={[
          styles.subcard,
          {
            flexDirection: 'row',
            width: dimensions.ScreenWidth,
            paddingHorizontal: dimensions.Width(4),
          },
        ]}>
        <View style={{width: dimensions.Width(30), alignItems: 'center'}}>
          <CustomText
            numberOfLines={1}
            light={colors.back_dark}
            dark={colors.light_white}>
            Fecha
          </CustomText>
        </View>
        <View style={{width: dimensions.Width(30), alignItems: 'center'}}>
          <CustomText
            numberOfLines={1}
            light={colors.back_dark}
            dark={colors.light_white}>
            Estado
          </CustomText>
        </View>
        <View style={{width: dimensions.Width(30), alignItems: 'center'}}>
          <CustomText
            numberOfLines={1}
            light={colors.back_dark}
            dark={colors.light_white}>
            Total
          </CustomText>
        </View>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{marginBottom: dimensions.Height(8)}}>
        <Query query={GET_DEPOSITOS} variables={{id: id}}>
          {(response, error, loading, refetch) => {
            refetch = refetch;
            if (response.loading) {
              return <ActivityIndicator size="large" color={colors.main} />;
            }
            if (response.error) {
              return <ActivityIndicator size="large" color={colors.main} />;
            }
            if (response) {
              console.log('Response Error-------', response);
              return (
                <FlatList
                  data={
                    response && response.data && response.data.getDeposito
                      ? response.data.getDeposito.list
                      : ''
                  }
                  renderItem={item => _renderItem(item)}
                  keyExtractor={item => item.id}
                  showsVerticalScrollIndicator={false}
                  ListEmptyComponent={
                    <NoData menssge="Aún no has recibido nigún ingreso" />
                  }
                />
              );
            }
          }}
        </Query>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  subcard: {
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    padding: dimensions.Height(2),
  },

  subcard1: {
    backgroundColor: 'transparent',
    padding: dimensions.Height(2),
    borderBottomColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    borderBottomWidth: 1,
  },

  chats: {
    fontSize: dimensions.FontSize(32),
    margin: dimensions.Width(4),
    marginTop: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
});
