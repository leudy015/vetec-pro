import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import {CustomText} from '../components/CustomText';
import {ButtomBar} from './../components/ButtomBar';
import Navigation from './../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {READ_NOTIFICATION} from '../mutations';
import moment from 'moment';
import {Avatar} from 'react-native-elements';
import {Badge} from '@ant-design/react-native';
import {GET_NOTIFICATIONS} from '../query';
import {Query, Mutation} from 'react-apollo';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import Icon from 'react-native-dynamic-vector-icons';
import NoData from '../components/NoData';
import AsyncStorage from '@react-native-community/async-storage';

export default function Notifications() {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const [id, setId] = useState(null);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
      console.log(id);
    };
    dataid();
  }, []);

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const _renderItem = (item, refetch) => {
    refetch();

    console.log(item);
    let title = '';
    let description = '';
    let avatar = '';
    switch (item.type) {
      case 'new_order':
        title = 'Nueva Consulta';
        description = `Enhorabuena ha recibido una nueva consulta de ${
          item.usuario.nombre
        } ${item.usuario.apellidos}.`;
        avatar = NETWORK_INTERFACE_LINK_AVATAR + item.usuario.avatar;
        break;
      case 'resolution_order':
        title = 'Consulta en resolución';
        description = `${item.usuario.nombre} ${
          item.usuario.apellidos
        } ha reportado un problema con la consulta estamos trabajando para solucionarlo.`;
        avatar = NETWORK_INTERFACE_LINK_AVATAR + item.usuario.avatar;
        break;
      case 'valored_order':
        title = 'Consulta valorada';
        description = `Enhorabuena ${item.usuario.nombre} ${
          item.usuario.apellidos
        } ha valorado tu trabajo.`;
        avatar = NETWORK_INTERFACE_LINK_AVATAR + item.usuario.avatar;
        break;
      case 'accept_order':
        title = 'Consulta Aceptada';
        description = `Genial ${item.profesional.nombre} ${
          item.profesional.apellidos
        } ha aceptado la consulta.`;
        avatar = NETWORK_INTERFACE_LINK_AVATAR + item.profesional.avatar;
        break;
      case 'reject_order':
        title = 'Consulta rechazada';
        description = `Opps ${item.profesional.nombre} ${
          item.profesional.apellidos
        } ha rechazado la consulta.`;
        avatar = NETWORK_INTERFACE_LINK_AVATAR + item.profesional.avatar;
        break;
      case 'finish_order':
        title = 'Consulta Completada';
        description = `Genial ${item.profesional.nombre} ${
          item.profesional.apellidos
        } ha completado la consulta es hora de valorarla.`;
        avatar = NETWORK_INTERFACE_LINK_AVATAR + item.profesional.avatar;

      case 'consulta_devuelta':
        title = 'Consulta devuelta';
        description = `Upps la consulta ha sido delvuelta debido a una reclamación por ${
          item.usuario.nombre
        } ${item.usuario.apellidos}.`;
        avatar = NETWORK_INTERFACE_LINK_AVATAR + item.usuario.avatar;
        break;
    }
    return (
      <Mutation
        mutation={READ_NOTIFICATION}
        variables={{notificationId: item._id}}>
        {(readNotification, data) => {
          return (
            <TouchableOpacity
              style={{alignSelf: 'center', marginTop: 20, width: '95%'}}
              onPress={() => {
                readNotification({variables: {notificationId: item._id}}).then(
                  () => {
                    refetch();
                    if (
                      data &&
                      data.readNotification &&
                      data.readNotification.success
                    ) {
                      console.log(data.readNotification.message);
                    } else if (
                      data &&
                      data.readNotification &&
                      !data.readNotification.success
                    )
                      console.log(data.readNotification.message);
                  },
                );
              }}>
              <View style={styles.fondo}>
                <View style={{flexDirection: 'row', width: '90%'}}>
                  <View
                    style={{
                      justifyContent: 'center',
                      alignSelf: 'center',
                      marginRight: 20,
                    }}>
                    <Avatar
                      size="medium"
                      containerStyle={{
                        borderWidth: 2,
                        borderColor: colors.rgb_235,
                      }}
                      rounded
                      source={{uri: avatar}}
                    />
                  </View>
                  <View style={{width: '95%'}}>
                    <View style={{flexDirection: 'row', width: '83%'}}>
                      <CustomText
                        light={colors.blue_main}
                        dark={colors.white}
                        style={{
                          fontWeight: '300',
                          fontSize: dimensions.FontSize(16),
                        }}>
                        {title}
                      </CustomText>
                      <Badge
                        style={{marginLeft: 'auto', marginTop: 10}}
                        text="Nueva"
                      />
                    </View>
                    <CustomText
                      style={{
                        fontWeight: '200',
                        color: colors.rgb_153,
                        width: '90%',
                        marginTop: 20,
                      }}>
                      {description}
                    </CustomText>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                      <CustomText
                        style={{marginRight: 15, color: colors.rgb_235}}>
                        <Icon
                          type="AntDesign"
                          name="clockcircleo"
                          style={{marginLeft: 'auto'}}
                          size={14}
                          color={colors.rgb_235}
                        />{' '}
                        {moment(Number(item.createdAt)).fromNow()}
                      </CustomText>
                      <CustomText style={{color: colors.rgb_235}}>
                        <Icon
                          type="AntDesign"
                          name="eye"
                          style={{marginLeft: 'auto'}}
                          size={14}
                          color={colors.rgb_235}
                        />{' '}
                        Leer
                      </CustomText>
                    </View>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          );
        }}
      </Mutation>
    );
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} title="Notificaciones" back={true} />
      </View>
      <CustomText
        light={colors.black}
        dark={colors.white}
        numberOfLines={1}
        style={styles.chats}>
        Notificaciones
      </CustomText>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }
        showsVerticalScrollIndicator={false}>
        <Query query={GET_NOTIFICATIONS} variables={{Id: id}}>
          {(data, error, loading) => {
            if (data.loading) {
              return <ActivityIndicator />;
            }
            if (error) {
              return console.log('Response Error-------', error);
            }
            if (data) {
              console.log('Response data-------', data);
              return (
                <View style={{marginBottom: dimensions.Height(15)}}>
                  <FlatList
                    data={
                      data && data.data && data.data.getNotifications
                        ? data.data.getNotifications.notifications
                        : ''
                    }
                    showsVerticalScrollIndicator={false}
                    renderItem={({item}) => _renderItem(item, data.refetch)}
                    keyExtractor={item => item._id}
                    ListEmptyComponent={
                      <NoData menssge="Bien hecho estás al día no tienes notificaciones" />
                    }
                  />
                </View>
              );
            }
          }}
        </Query>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  fondo: {
    height: 'auto',
    padding: 10,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    borderRadius: 10,
  },

  chats: {
    fontSize: dimensions.FontSize(32),
    margin: dimensions.Width(4),
    marginTop: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
});
