import React, {useEffect, useState, useCallback} from 'react';
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
} from 'react-native';
import {requestMultiple, PERMISSIONS} from 'react-native-permissions';
import {CustomText} from '../components/CustomText';
import Navigation from './../services/NavigationService';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {
  GiftedChat,
  Send,
  Bubble,
  InputToolbar,
} from 'react-native-gifted-chat';
import {
  NETWORK_INTERFACE_LINK_AVATAR,
  NETWORK_INTERFACE_LINK,
} from '../constants/config';
import Icon from 'react-native-dynamic-vector-icons';
import {Avatar} from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import {UPLOAD_FILE} from '../mutations';
import {Mutation} from 'react-apollo';
import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';
import io from 'socket.io-client';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import {useNavigationParam} from 'react-navigation-hooks';
import QB from 'quickblox-react-native-sdk';
import {showError} from '../services/FlashMessageService';
import {webrtcCall} from '../actionCreators'; // webrtc call audio & video
import {connect} from 'react-redux';
import {ifIphoneX} from 'react-native-iphone-x-helper';

function ChatsScreen({call, qbUsers}) {
  const data = useNavigationParam('data');
  console.log('datadata', data);
  const styles = useDynamicStyleSheet(dynamicStyles);
  const [image, setImage] = useState('');
  const [text, setText] = useState({text: ''});
  const [messages, setMessages] = useState(data.messagechat || []);
  const [id, setId] = useState(null);
  const [Loading, setLoading] = useState(false);
  const [LoadingAudio, setLoadingAudio] = useState(false);

  const socket = io(NETWORK_INTERFACE_LINK, {
    forceNew: true,
  });

  console.log(id);
  if (Platform.OS == 'ios') {
    requestMultiple([PERMISSIONS.IOS.MICROPHONE, PERMISSIONS.IOS.CAMERA]).then(
      statuses => {
        console.log('Camera', statuses[PERMISSIONS.IOS.CAMERA]);
        console.log('FaceID', statuses[PERMISSIONS.IOS.MICROPHONE]);
      },
    );
  } else {
    requestMultiple([
      PERMISSIONS.ANDROID.MICROPHONE,
      PERMISSIONS.ANDROID.CAMERA,
    ]).then(statuses => {
      console.log('Camera', statuses[PERMISSIONS.IOS.CAMERA]);
      console.log('FaceID', statuses[PERMISSIONS.IOS.MICROPHONE]);
    });
  }

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();

    socket.on('connect', () => {
      socket.emit('online_user', {
        usuario: data.usuario.id,
        profesional: data.profesional.id,
        id: data.profesional.id,
      });
    });

    socket.on('messages', datas => {
      let old_messages = [...messages]
      setMessages(messages => [...datas.messagechat || [], ...old_messages]);
    });
  }, []);

  const SendPushNotificationMessage = (id, messages, names) => {
    console.log(id, messages, names)
    fetch(
      `${NETWORK_INTERFACE_LINK}/send-push-notification-message?IdOnesignal=${
        id
      }&messages=${messages}&name=${names}`,
    ).catch(err => console.log(err));
  };

  const onSend = (newMessages) => {
    let old_messages = [...messages]
    setMessages(messages => GiftedChat.append(old_messages, newMessages));
    const dats = newMessages[0];
    SendPushNotificationMessage(data.usuario.UserID, dats.text, dats.user.name)
    const messagechat = [
      {
        text: dats.text,
        read: false,
        user: {
          _id: dats.user._id,
          name: dats.user.name,
          avatar: dats.user.avatar,
        },
        createdAt: dats.createdAt,
        _id: dats._id,
      },
    ];
    socket.emit('private_message', {
      messagechat,
      Userid: data.usuario.UserID,
      receptor: data.usuario.id,
      prof: data.profesional.id,
      user: data.usuario.id,
    });
  };

  const renderInputToolbar = props => {
    return <InputToolbar {...props} containerStyle={styles.inputs} />;
  };

  const renderSend = props => {
    return (
      <Send {...props}>
       <View style={{marginRight: 5, marginTop: 5}}>
          <Icon
            type="MaterialCommunityIcons"
            name="send-circle"
            size={40}
            color={colors.main}
            style={styles.Send}
          />
        </View>
      </Send>
    );
  };

  const renderBubble = props => {
    return (
      <Bubble
        {...props}
        textStyle={{
          right: {
            color: colors.black,
            fontWeight: '200',
          },
          left: {
            color: colors.black,
            fontWeight: '200',
          },
        }}
        wrapperStyle={{
          left: {
            backgroundColor: colors.rgb_235,
          },
          right: {
            backgroundColor: colors.main,
          },
        }}
      />
    );
  };

  const SendPushNotificationvideo = () => {
    fetch(
      `${NETWORK_INTERFACE_LINK}/send-push-notification?IdOnesignal=${
        data.usuario.UserID
      }&textmessage= Video llamada de ${data.profesional.nombre} ${
        data.profesional.apellidos
      }`,
    ).catch(err => console.log(err));
  };

  const SendPushNotificationaudio = () => {
    fetch(
      `${NETWORK_INTERFACE_LINK}/send-push-notification?IdOnesignal=${
        data.usuario.UserID
      }&textmessage= Llamada entrante de ${data.profesional.nombre} ${
        data.profesional.apellidos
      }`,
    ).catch(err => console.log(err));
  };

  const videoCall = () => {
    setLoading(true);
    const emails = data && data.usuario ? data.usuario.email : '';
    console.log('qbUsers', qbUsers);
    let users = qbUsers.filter(opponentUser => {
      if (opponentUser.login === emails) {
        setLoading(false);
        return true;
      } else {
        setLoading(false);
        return false;
      }
    });
    console.log('users', users);
    if(users[0]){
      const opponentsIds = [users[0].id];
      try {
        call({opponentsIds, type: QB.webrtc.RTC_SESSION_TYPE.VIDEO});
        //SendPushNotificationvideo();
        Navigation.navigate('CheckConnection', {data: data.usuario});
      } catch (e) {
        showError('Error', e.message);
      }
    }
    
  };

  const voiceCall = () => {
    console.log('qbUsers', qbUsers);
    setLoadingAudio(true);
    const emails = data && data.usuario ? data.usuario.email : '';
    let users = qbUsers.filter(opponentUser => {
      if (opponentUser.login === emails) {
        setLoadingAudio(false);
        return true;
      } else {
        setLoadingAudio(false);
        return false;
      }
    });
    console.log('users', users);
    if(users[0]){
      const opponentsIds = [users[0].id];
      try {
        call({opponentsIds, type: QB.webrtc.RTC_SESSION_TYPE.AUDIO});
        //SendPushNotificationaudio();
        Navigation.navigate('CheckConnection', {data: data.usuario});
      } catch (e) {
        showError('Error', e.message);
      }
    }
    
  };

  const selectPhotoTapped = singleUpload => {
    const options = {
      quality: 0.5,
      title: 'Seleccionar imagen',
      cancelButtonTitle: 'Cancelar',
      takePhotoButtonTitle: 'Hacer foto',
      chooseFromLibraryButtonTitle: 'Seleccionar foto existente',
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        storageOptions: {
          skipBackup: true,
        },
      },
    };
    ImagePicker.showImagePicker(options, async response => {
      if (response.data) {
        const imgBlob = 'data:image/jpeg;base64,' + response.data;
        if (imgBlob) {
          singleUpload({variables: {imgBlob}})
            .then(res => {
              console.log('filemane: ', res.data.singleUpload.filename);
              setImage(res.data.singleUpload.filename);

              const messagechat = [
                {
                  image:
                    NETWORK_INTERFACE_LINK_AVATAR +
                    res.data.singleUpload.filename,
                  text: 'Imagen',
                  read: false,
                  user: {
                    _id: data.profesional.id,
                    name: data.profesional.nombre,
                    avatar:
                      NETWORK_INTERFACE_LINK_AVATAR + data.profesional.avatar,
                  },
                  createdAt: Date.now(),
                  _id: uuidv4(),
                },
              ];

              setMessages(messages => [...messagechat, ...messages]);
              const text = 'Imagen'
              SendPushNotificationMessage(data.usuario.UserID, text, data.profesional.nombre)
              socket.emit('private_message', {
                messagechat,
                Userid: data.usuario.UserID,
                receptor: data.usuario.id,
                prof: data.profesional.id,
                user: data.usuario.id,
              });

              console.log('send message image', messagechat);
            })
            .catch(error => {
              console.log('fs error: que me arroja', error);
            });
        }
      }
    });
  };


  const disconetc = () => {
    socket.emit('disconnectProf', {
      profesional: data.profesional.id,
    });
    Navigation.goBack(null);
  };

  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.headers}>
        <View style={{flexDirection: 'row', marginTop: dimensions.Height(2)}}>
          <TouchableOpacity
            onPress={() => disconetc()}
            style={{marginLeft: 15}}>
            <Icon
              name="arrowleft"
              type="AntDesign"
              size={25}
              style={styles.bacs}
            />
          </TouchableOpacity>
          <View style={{marginLeft: dimensions.Width(4), flexDirection: 'row'}}>
            <Avatar
              rounded
              size={30}
              source={{
                uri: NETWORK_INTERFACE_LINK_AVATAR + data.usuario.avatar,
              }}
              containerStyle={styles.avatar}
            />
            <View>
              <View style={{flexDirection: 'row'}}>
                <CustomText
                  numberOfLines={1}
                  light={colors.back_dark}
                  dark={colors.white}
                  style={[
                    styles.title,
                    {
                      marginLeft: 8,
                      width: 'auto',
                      maxWidth: dimensions.Width(25),
                    },
                  ]}>
                  {data.usuario.nombre} {data.usuario.apellidos}
                </CustomText>
                {data.usuario.isPlus ? (
                  <Icon
                    name="verified"
                    type="Octicons"
                    size={14}
                    style={{
                      alignSelf: 'center',
                      marginTop: 3,
                      marginLeft: 7,
                      color: colors.orange,
                    }}
                  />
                ) : null}
              </View>
              {data.usuario.connected ? (
                <CustomText
                  light={colors.back_dark}
                  dark={colors.white}
                  style={styles.title1}>
                  En linea
                </CustomText>
              ) : (
                <CustomText
                  light={colors.back_dark}
                  dark={colors.white}
                  style={styles.title1}>
                  Hace {''}{' '}
                  {moment(Number(data.usuario.lastTime)).fromNow('ss')}
                  {''} última vez
                </CustomText>
              )}
            </View>
          </View>
          <View
            style={{
              marginLeft: 'auto',
              flexDirection: 'row',
              marginRight: 15,
            }}>
            {LoadingAudio ? (
              <ActivityIndicator style={{marginRight: 20}} />
            ) : (
              <TouchableOpacity
                onPress={() => voiceCall()}
                style={{marginRight: 20}}>
                <Icon
                  name="phone"
                  type="AntDesign"
                  size={25}
                  style={styles.bacs}
                />
              </TouchableOpacity>
            )}
            {Loading ? (
              <ActivityIndicator style={{marginRight: 20}} />
            ) : (
              <TouchableOpacity
                onPress={() => videoCall()}
                style={{marginRight: 20}}>
                <Icon
                  name="videocamera"
                  type="AntDesign"
                  size={25}
                  style={styles.bacs}
                />
              </TouchableOpacity>
            )}
            <Mutation mutation={UPLOAD_FILE}>
          {singleUpload => (
            <TouchableOpacity
              onPress={() => selectPhotoTapped(singleUpload)}
              style={{marginRight: 5}}>
              <Icon
                name="camerao"
                type="AntDesign"
                size={25}
                style={styles.bacs}
              />
            </TouchableOpacity>
          )}
        </Mutation>
          </View>
        </View>
      </SafeAreaView>
      <GiftedChat
        messages={messages}
        placeholder="Escribe algo ...."
        onSend={newMessages => onSend(newMessages)}
        onInputTextChanged={text => setText({text})}
        text={text.text}
        isTyping={true}
        renderInputToolbar={renderInputToolbar}
        renderSend={renderSend}
        textInputStyle={{color: colors.rgb_153}}
        renderBubble={renderBubble}
        messageRead={true}
        messageDelivered={true}
        renderUsernameOnMessage={true}
        showUserAvatar={true}
        renderAvatarOnTop={true}
        user={{
          _id: data.profesional.id,
          name: data.profesional.nombre,
          avatar: NETWORK_INTERFACE_LINK_AVATAR + data.profesional.avatar,
        }}
      />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  inputs: {
    borderTopWidth: 0.2,
    borderTopColor: colors.rgb_235,
    backgroundColor: 'transparent',
  },
  
  Send: {
    marginLeft: 'auto',
  },

  headers: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(12),
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    ...ifIphoneX(
      {
        marginTop: 0
      },
      {
        paddingTop: 30,
      },
    ),
  },

  title: {
    marginLeft: dimensions.Width(3),
    fontSize: dimensions.FontSize(18),
    fontWeight: '500',
  },

  title1: {
    marginLeft: dimensions.Width(3),
    fontSize: dimensions.FontSize(12),
    fontWeight: '500',
    color: colors.rgb_153,
  },

  bacs: {
    color: new DynamicValue(colors.main, colors.main),
  },
  bacs1: {
    color: new DynamicValue(colors.main, colors.main),
    marginLeft: dimensions.Width(4),
    marginTop: dimensions.Height(-4.5),
  },
});

const mapStateToProps = ({auth, users, user}, {exclude = []}) => {
  console.log('usersusers', users);
  return {
    qbUsers:
      (users &&
        users.users
          .filter(smuser => (auth.user ? smuser.id !== auth.user.id : true))
          .filter(smuser => exclude.indexOf(smuser.id) === -1)) ||
      [],
    user: user.user,
  };
};

const mapDispatchToProps = {
  call: webrtcCall,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChatsScreen);
