import React, {useState} from 'react';
import {
  View,
  ScrollView,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import {CustomText} from '../components/CustomText';
import Navigation from '../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {GET_MY_PACIENTES} from '../query';
import {Query} from 'react-apollo';
import {Button} from '../components/Button';
import {SearchBar, Avatar} from 'react-native-elements';
import {NETWORK_INTERFACE_LINK_AVATAR} from './../constants/config';
import NoData from './../components/NoData';
import {useNavigationParam} from 'react-navigation-hooks';
import Icon from 'react-native-dynamic-vector-icons';

export default function Pago() {
  const [search, setSearch] = useState('');
  const [refreshing, setRefreshing] = useState(false);

  const data = useNavigationParam('data');

  const updateSearch = search => {
    setSearch(search);
  };

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  const styles = useDynamicStyleSheet(dynamicStyles);

  const _renderItem = ({item}, refetch) => {
    refetch();
    const items = {
      item,
      data,
    };
    return (
      <TouchableOpacity
        style={styles.cardinfo}
        onPress={() => Navigation.navigate('DetailsUsuario', {data: items})}>
        <View
          style={{
            alignSelf: 'center',
            marginLeft: dimensions.Width(2),
          }}>
          <Avatar
            rounded
            size={50}
            source={{
              uri: NETWORK_INTERFACE_LINK_AVATAR + item.avatar,
            }}
            containerStyle={styles.avatar}
          />
        </View>
        <View
          style={{
            alignSelf: 'center',
            marginLeft: dimensions.Width(2),
          }}>
          <CustomText
            numberOfLines={1}
            light={colors.back_dark}
            dark={colors.white}
            style={styles.name1}>
            {item.nombre} {item.apellidos}{' '}
            {item.isPlus ? (
              <Icon
                name="verified"
                type="Octicons"
                size={14}
                style={{
                  alignSelf: 'center',
                  marginTop: 0,
                  marginLeft: 7,
                  color: colors.orange,
                }}
              />
            ) : null}
          </CustomText>
          <CustomText light={colors.back_dark} dark={colors.rgb_153}>
            {item.ciudad}
          </CustomText>
        </View>
        <View
          style={{
            alignSelf: 'center',
            marginLeft: 'auto',
            marginRight: dimensions.Width(4),
          }}>
          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView}
              onPress={() =>
                Navigation.navigate('DetailsUsuario', {data: items})
              }
              title="Ver detalles"
              titleStyle={styles.buttonTitle}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} title="Mis pacientes" back={true} />
      </View>
      <SearchBar
        placeholder="Busca un paciente"
        onChangeText={updateSearch}
        value={search}
        containerStyle={styles.search}
        inputContainerStyle={styles.input}
      />
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }
        showsVerticalScrollIndicator={false}>
        <Query
          query={GET_MY_PACIENTES}
          variables={{id: data.id, search: search}}>
          {response => {
            if (response.loading) {
              return <ActivityIndicator size="large" color={colors.main} />;
            }
            if (response.error) {
              return <ActivityIndicator size="large" color={colors.main} />;
            }
            if (response) {
              console.log(response);
              const datos =
                response && response.data && response.data.getmyPacientes
                  ? response.data.getmyPacientes.list
                  : '';
              return (
                <View>
                  <View
                    style={{
                      paddingTop: dimensions.Height(2),
                    }}>
                    <CustomText
                      light={colors.back_dark}
                      dark={colors.rgb_153}
                      style={styles.text1}>
                      Pacientes totales
                    </CustomText>
                    <CustomText
                      light={colors.back_dark}
                      dark={colors.rgb_153}
                      style={styles.textss}>
                      ({datos.length}) resultado.
                    </CustomText>
                  </View>
                  <FlatList
                    data={datos}
                    renderItem={item => _renderItem(item, response.refetch)}
                    keyExtractor={item => item.id}
                    showsVerticalScrollIndicator={false}
                    ListEmptyComponent={
                      <NoData menssge="Aún no tienes pacientes" />
                    }
                  />
                </View>
              );
            }
          }}
        </Query>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  search: {
    backgroundColor: 'transparent',
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },

  input: {
    backgroundColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    borderRadius: 40,
  },
  des: {
    fontSize: 22,
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(6),
    fontWeight: '300',
  },
  dess1: {
    fontSize: 18,
    textAlign: 'center',
    paddingHorizontal: dimensions.Width(6),
    fontWeight: 'bold',
  },
  dess: {
    fontSize: 18,
    textAlign: 'center',
    marginTop: dimensions.Height(3),
    paddingHorizontal: dimensions.Width(6),
    fontWeight: '200',
  },
  cardinfo: {
    width: dimensions.Width(95),
    height: 120,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginLeft: dimensions.Width(2.5),
    marginTop: dimensions.Height(2),
    borderRadius: 10,
    flexDirection: 'row',
  },
  name1: {
    fontSize: dimensions.FontSize(18),
    width: 150,
  },

  avatar: {
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    borderWidth: 2,
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(35),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
  },
  signupButtonContainer1: {
    marginTop: dimensions.Height(3),
  },
  textss: {
    fontSize: dimensions.FontSize(16),
    marginBottom: dimensions.Height(2),
    marginLeft: dimensions.Height(2),
  },

  text1: {
    fontSize: dimensions.FontSize(28),
    marginLeft: dimensions.Height(2),
  },
});
