import React from 'react';
import {View, ScrollView, Image} from 'react-native';
import Navigation from './../services/NavigationService';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {image} from '../constants/image';
import {CustomText} from '../components/CustomText';
import {Button} from './../components/Button';
import {connect} from 'react-redux';
import {appStart} from './../actionCreators';
import QBConfig from './../quickblox/QBConfig';

function Landing({appStart}) {
  appStart(QBConfig);

  setTimeout(()=>{
    appStart(QBConfig)
  },1000)

  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{marginTop: dimensions.Height(20)}}>
          <Image
            source={image.LandingImg}
            style={{width: 300, height: 130, alignSelf: 'center'}}
          />
        </View>

        <View style={{alignSelf: 'center'}}>
          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={styles.text}>
            Bienvenido a Vetec PRO!
          </CustomText>
        </View>

        <View
          style={{
            alignSelf: 'center',
            marginTop: 10,
            paddingHorizontal: dimensions.Width(4),
          }}>
          <CustomText
            light={colors.back_dark}
            dark={colors.rgb_153}
            style={styles.textsecond}>
            Vetec PRO, es la plataforma líder de los veterinarios, la cual te
            permite llegar a más clientes desde la palma de tus manos.
          </CustomText>
        </View>

        <View style={{marginTop: dimensions.Height(5)}}>
          <View style={styles.signupButtonContainer}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView}
              onPress={() => Navigation.navigate('Login')}
              title="Iniciar sesión"
              titleStyle={styles.buttonTitle}
            />
          </View>

          <View style={styles.signupButtonContainer1}>
            <Button
              light={colors.white}
              dark={colors.white}
              containerStyle={styles.buttonView}
              onPress={() => Navigation.navigate('Register')}
              title="Registrarme"
              titleStyle={styles.buttonTitle}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  text: {
    fontSize: dimensions.FontSize(30),
    fontWeight: 'bold',
    textAlign: 'center',
    paddingHorizontal: 15,
    marginTop: 30,
  },

  textsecond: {
    fontSize: dimensions.FontSize(16),
    fontWeight: '300',
    textAlign: 'center',
    paddingHorizontal: 15,
  },

  signupButtonContainer: {
    marginTop: dimensions.Height(3),
    alignSelf: 'center',
  },
  signupButtonContainer1: {
    marginTop: dimensions.Height(3),
    alignSelf: 'center',
    paddingBottom: 100,
  },
  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },
});

export default connect(
  null,
  {appStart},
)(Landing);
