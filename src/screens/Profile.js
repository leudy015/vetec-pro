import React, {useEffect, useState} from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  Linking,
} from 'react-native';
import {CustomText} from '../components/CustomText';
import Navigation from './../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import AsyncStorage from '@react-native-community/async-storage';
import {Query} from 'react-apollo';
import {PROFESIONAL_DETAIL} from '../query';
import {useNavigationParam} from 'react-navigation-hooks';
import {Avatar} from 'react-native-elements';
import Icon from 'react-native-dynamic-vector-icons';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import {FAVORITO_PROFESIONAL, PROFESSIONAL_RATING_QUERY} from '../query';
import PlaceHolder from './../components/placeholderprofile';

export default function Profile() {
  const [id, setId] = useState(null);

  const [userData, setUserData] = useState({
    id: '',
    nombre: '',
    apellidos: '',
    email: '',
  });

  const styles = useDynamicStyleSheet(dynamicStyles);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  const logout = async () => {
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('id');
    Navigation.navigate('Landing');
  };
  const data = useNavigationParam('data');
  return (
    <View style={styles.container}>
      <View>
        <Headers
          navigation={Navigation}
          title={`¡Hola ${userData.nombre}!`}
          back={true}
          atrasprofile={true}
        />
      </View>
      <CustomText
        light={colors.black}
        dark={colors.white}
        numberOfLines={1}
        style={styles.chats}>
        Cuenta
      </CustomText>
      <Query query={PROFESIONAL_DETAIL} variables={{id: data}}>
        {({loading, error, data, refetch}) => {
          if (error) {
            console.log('Response Error-------', error);
            return <PlaceHolder />;
          }
          if (loading) {
            return <PlaceHolder />;
          }
          if (data) {
            const userDatas = data.getProfesional;
            setUserData(userDatas);
            return (
              <ScrollView showsVerticalScrollIndicator={false}>
                <Query query={FAVORITO_PROFESIONAL} variables={{id: id}}>
                  {(response, error, loading) => {
                    if (loading) {
                      return (
                        <ActivityIndicator size="large" color={colors.main} />
                      );
                    }
                    if (error) {
                      return console.log('Response Error-------', error);
                    }
                    if (response) {
                      const datas =
                        response &&
                        response.data &&
                        response.data.getUsuarioFavoritoall
                          ? response.data.getUsuarioFavoritoall.list
                          : '';
                      return (
                        <View style={styles.ratin}>
                          <View style={styles.item}>
                            <Icon
                              name="heart"
                              type="AntDesign"
                              size={30}
                              color={colors.ERROR}
                            />
                            <CustomText
                              light={colors.back_dark}
                              dark={colors.light_white}
                              style={{
                                textAlign: 'center',
                                marginTop: 10,
                              }}>
                              {datas.length} Te han añadido a favoritos
                            </CustomText>
                          </View>
                          <Query
                            query={PROFESSIONAL_RATING_QUERY}
                            variables={{id: id}}>
                            {(response, error, loading) => {
                              if (loading) {
                                return (
                                  <ActivityIndicator
                                    size="large"
                                    color={colors.green_main}
                                  />
                                );
                              }
                              if (error) {
                                return console.log(
                                  'Response Error-------',
                                  error,
                                );
                              }
                              if (response) {
                                const dass =
                                  response &&
                                  response.data &&
                                  response.data.getProfessionalRating
                                    ? response.data.getProfessionalRating.list
                                    : '';
                                return (
                                  <View style={styles.item}>
                                    <Icon
                                      name="star"
                                      type="AntDesign"
                                      size={30}
                                      color={colors.orange}
                                    />
                                    <CustomText
                                      light={colors.back_dark}
                                      dark={colors.light_white}
                                      style={{
                                        textAlign: 'center',
                                        marginTop: 10,
                                      }}>
                                      {dass && dass.length} Total de
                                      valoraciones de pacientes
                                    </CustomText>
                                  </View>
                                );
                              }
                            }}
                          </Query>
                        </View>
                      );
                    }
                  }}
                </Query>

                <View
                  style={{
                    marginTop: dimensions.Height(3),
                    marginBottom: dimensions.Height(15),
                  }}>
                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() =>
                      Navigation.navigate('UpdateProfile', {
                        data: userDatas,
                      })
                    }>
                    <View
                      style={{
                        alignSelf: 'center',
                      }}>
                      <Avatar
                        rounded
                        size={40}
                        source={{
                          uri: NETWORK_INTERFACE_LINK_AVATAR + userDatas.avatar,
                        }}
                        containerStyle={styles.avatar}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <View style={{flexDirection: 'row'}}>
                        <CustomText
                          light={colors.back_dark}
                          dark={colors.white}
                          style={styles.name1}>
                          {userData.nombre} {userData.apellidos}
                        </CustomText>
                        {userData.isVerified ? (
                          <Icon
                            name="verified"
                            type="Octicons"
                            size={14}
                            style={{
                              alignSelf: 'center',
                              marginTop: 5,
                              marginLeft: 7,
                              color: colors.twitter_color,
                            }}
                          />
                        ) : null}
                      </View>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Edita tus datos del perfil
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() =>
                      Navigation.navigate('Chats', {
                        data: userData.id,
                      })
                    }>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(2),
                      }}>
                      <Icon
                        name="message1"
                        type="AntDesign"
                        size={30}
                        style={styles.cons}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(6),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={styles.name1}>
                        Chats
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Chats con tus pacientes
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>
                  {userData.isPlusVet ? (
                    <TouchableOpacity
                      style={styles.cardinfo}
                      onPress={() =>
                        Navigation.navigate('Pacientes', {
                          data: userData,
                        })
                      }>
                      <View
                        style={{
                          alignSelf: 'center',
                          marginLeft: dimensions.Width(1),
                        }}>
                        <Icon
                          name="dog"
                          type="MaterialCommunityIcons"
                          size={30}
                          style={styles.cons}
                        />
                      </View>

                      <View
                        style={{
                          alignSelf: 'center',
                          marginLeft: dimensions.Width(4),
                        }}>
                        <CustomText
                          light={colors.back_dark}
                          dark={colors.white}
                          style={styles.name1}>
                          Mis pacientes
                        </CustomText>
                        <CustomText
                          light={colors.back_dark}
                          dark={colors.rgb_153}
                          style={styles.sub}>
                          Administra tus pacientes
                        </CustomText>
                      </View>

                      <View
                        style={{
                          alignSelf: 'center',
                          marginLeft: 'auto',
                          marginRight: dimensions.Width(4),
                        }}>
                        <Icon
                          name="right"
                          type="AntDesign"
                          size={20}
                          color={colors.rgb_153}
                        />
                      </View>
                    </TouchableOpacity>
                  ) : null}

                  <View style={styles.separator} />

                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() => Navigation.navigate('Deposito')}>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(2),
                      }}>
                      <Icon
                        name="euro"
                        type="FontAwesome"
                        size={30}
                        style={styles.cons}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(6),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={styles.name1}>
                        Mis ingresos
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Mira las transacciones de tu cuenta
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() => Navigation.navigate('Cobros')}>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(1),
                      }}>
                      <Icon
                        name="bank"
                        type="MaterialCommunityIcons"
                        size={30}
                        style={styles.cons}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={styles.name1}>
                        Datos bancarios
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Configura tu datos para retiros
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>

                  <View style={styles.separator} />

                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() =>
                      Navigation.navigate('Opiniones', {
                        data: userDatas.id,
                      })
                    }>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(1),
                      }}>
                      <Icon
                        name="staro"
                        type="AntDesign"
                        size={30}
                        style={styles.cons}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={styles.name1}>
                        Opiniones
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Mira las opiniones de clientes
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() =>
                      Linking.canOpenURL(
                        'https://profesionales.vetec.es/profile-professional',
                      ).then(supported => {
                        if (supported) {
                          Linking.openURL(
                            'https://profesionales.vetec.es/profile-professional',
                          );
                        } else {
                          console.log(
                            "Don't know how to open URI: https://profesionales.vetec.es/profile-professional ",
                          );
                        }
                      })
                    }>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(1),
                      }}>
                      <Icon
                        name="barschart"
                        type="AntDesign"
                        size={30}
                        style={styles.cons}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={styles.name1}>
                        Estadísticas
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Echemos un vistazo a los números
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() => Navigation.navigate('Configuraciones')}>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(1),
                      }}>
                      <Icon
                        name="setting"
                        type="AntDesign"
                        size={30}
                        style={styles.cons}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={styles.name1}>
                        Configuración y ayuda
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}
                        style={styles.sub}>
                        Configura tu privacidad
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.cardinfo}
                    onPress={() =>
                      Alert.alert(
                        'Cerrar sesión',
                        '¿Seguro que quieres cerrar sesión?',
                        [
                          {
                            text: 'Cancelar',
                            onPress: () => console.log('calcel'),
                          },
                          {text: 'Si', onPress: () => logout()},
                        ],

                        {cancelable: false},
                      )
                    }>
                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(1),
                      }}>
                      <Icon
                        name="logout"
                        type="AntDesign"
                        size={30}
                        color={colors.ERROR}
                      />
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: dimensions.Width(4),
                      }}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.white}
                        style={styles.name1}>
                        Cerrar sesión
                      </CustomText>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.rgb_153}>
                        Te esperamos pronto
                      </CustomText>
                    </View>

                    <View
                      style={{
                        alignSelf: 'center',
                        marginLeft: 'auto',
                        marginRight: dimensions.Width(4),
                      }}>
                      <Icon
                        name="right"
                        type="AntDesign"
                        size={20}
                        color={colors.rgb_153}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
              </ScrollView>
            );
          }
        }}
      </Query>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
  },
  containerModal: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    height: dimensions.ScreenHeight,
    alignSelf: 'center',
  },

  ratin: {
    width: dimensions.Width(90),
    height: 'auto',
    padding: dimensions.Width(2),
    marginTop: dimensions.Height(4),
    margin: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  item: {
    width: dimensions.Width(40),
    height: 'auto',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(15),
    borderTopEndRadius: 30,
    borderTopStartRadius: 30,
  },

  cardinfo: {
    width: dimensions.Width(100),
    height: 'auto',
    marginTop: dimensions.Height(1),
    borderRadius: 10,
    flexDirection: 'row',
    padding: dimensions.Width(4),
  },

  avatar: {
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    borderWidth: 4,
  },

  chats: {
    fontSize: dimensions.FontSize(32),
    margin: dimensions.Width(4),
    marginTop: 10,
    fontWeight: 'bold',
    marginBottom: 0,
  },

  name: {
    marginTop: dimensions.Height(1),
    alignSelf: 'center',
    fontSize: dimensions.FontSize(24),
  },

  name1: {
    fontSize: dimensions.FontSize(18),
    fontWeight: '400',
    fontFamily: 'Helvetica',
  },

  cons: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  separator: {
    backgroundColor: new DynamicValue(colors.light_grey, colors.rgb_153),
    width: dimensions.Width(93),
    height: 0.5,
    marginTop: dimensions.Height(4),
  },

  formView: {
    marginHorizontal: dimensions.Width(6),
    marginTop: dimensions.Height(1),
    marginBottom: dimensions.Height(15),
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
    marginTop: dimensions.Height(4),
    marginBottom: dimensions.Height(1),
    alignSelf: 'center',
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },
});
