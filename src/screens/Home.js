import React, {useState, useEffect} from 'react';
import {
  View,
  RefreshControl,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Navigation from './../services/NavigationService';
import {dimensions} from '../themes/dimensions';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {colors} from '../themes/colors';
import Headers from '../components/Header';
import Consultas from '../components/Consultas';
import {ScrollView} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import {appStart} from './../actionCreators';
import QBConfig from './../quickblox/QBConfig';
import AsyncStorage from '@react-native-community/async-storage';
import {Query} from 'react-apollo';
import {PROFESIONAL_DETAIL} from '../query';
import {CustomText} from '../components/CustomText';

function Home({appStart}) {
  appStart(QBConfig);
  const [refreshing, setRefreshing] = useState(false);
  const [id, setId] = useState(null);
  const styles = useDynamicStyleSheet(dynamicStyles);

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
      appStart(QBConfig);
    }, 2000);
  };

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} title="Consultas" />
      </View>

      {id ? (
        <Query query={PROFESIONAL_DETAIL} variables={{id: id}}>
          {({loading, error, data, refetch}) => {
            if (error) {
              console.log('Response Error-------', error);
              return <ActivityIndicator />;
            }
            if (loading) {
              return <ActivityIndicator />;
            }
            if (data) {
              const userDatas = data.getProfesional;
              let very = {
                id: id,
                fromSocial: true,
              };
              return (
                <View>
                  {!userDatas.verifyPhone ? (
                    <View style={styles.noverify}>
                      <CustomText
                        style={[
                          styles.text,
                          {
                            width: 200,
                            color: colors.white,
                            paddingTop: 15,
                          },
                        ]}>
                        Aún no has verificado número de teléfono,
                      </CustomText>
                      <TouchableOpacity
                        style={styles.btns}
                        onPress={() =>
                          Navigation.navigate('VerifyPhone', {
                            data: very,
                          })
                        }>
                        <CustomText
                          style={[
                            styles.text,
                            ,
                            {
                              color: colors.white,
                              textAlign: 'center',
                              padding: 8,
                              paddingHorizontal: 15,
                            },
                          ]}>
                          ¡Verificar ahora!
                        </CustomText>
                      </TouchableOpacity>
                    </View>
                  ) : null}
                </View>
              );
            }
          }}
        </Query>
      ) : null}

      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }
        showsVerticalScrollIndicator={false}>
        <View style={{marginBottom: dimensions.Height(15)}}>
          <Consultas />
        </View>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  noverify: {
    width: dimensions.Width(100),
    height: 70,
    backgroundColor: colors.ERROR,
    marginTop: 10,
    marginBottom: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    paddingLeft: 15,
    flexDirection: 'row',
  },
  btns: {
    borderColor: colors.white,
    borderWidth: 2,
    alignSelf: 'center',
    justifyContent: 'center',
    marginLeft: 'auto',
    marginRight: 15,
    borderRadius: 55,
  },

  text: {
    fontSize: dimensions.FontSize(16),
    fontWeight: '300',
    fontFamily: 'Helvetica',
  },
});

const mapDispatchToProps = {appStart};

export default connect(
  null,
  mapDispatchToProps,
)(Home);
