import React, {useState} from 'react';
import {
  View,
  FlatList,
  ActivityIndicator,
  RefreshControl,
  ScrollView,
} from 'react-native';
import {CustomText} from '../components/CustomText';
import Navigation from './../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {PROFESSIONAL_RATING_QUERY} from '../query/index';
import {Query} from 'react-apollo';
import moment from 'moment';
import 'moment/locale/es';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import {Avatar} from 'react-native-elements';
import Icon from 'react-native-dynamic-vector-icons';
import NoData from '../components/NoData';
import {useNavigationParam} from 'react-navigation-hooks';

export default function Depositos() {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const [refreshing, setRefreshing] = useState(false);

  const data = useNavigationParam('data');

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  console.log('data pro', data);

  const _renderItem = ({item}) => {
    return (
      <View style={styles.card}>
        <View style={{width: '100%', padding: 5, flexDirection: 'row'}}>
          <Avatar
            containerStyle={{borderWidth: 3, borderColor: colors.rgb_235}}
            rounded
            size={40}
            source={{uri: NETWORK_INTERFACE_LINK_AVATAR + item.customer.avatar}}
          />
          <View>
            <CustomText
              numberOfLines={1}
              light={colors.back_dark}
              dark={colors.light_white}
              style={{
                fontSize: 20,
                fontWeight: '300',
                marginLeft: dimensions.Width(3),
              }}>
              {item.customer.nombre} {item.customer.apellidos}
            </CustomText>
            <View
              style={{flexDirection: 'row', marginLeft: dimensions.Width(2)}}>
              <CustomText
                style={{
                  fontSize: 14,
                  marginLeft: 5,
                  fontWeight: '300',
                  color: colors.rgb_153,
                }}>
                {moment(Number(item.updated_at)).fromNow()}
              </CustomText>
              <CustomText
                style={{
                  fontSize: 12,
                  marginLeft: 10,
                  fontWeight: '300',
                  color: colors.rgb_153,
                }}>
                {item.customer.ciudad}
              </CustomText>
            </View>
          </View>

          <CustomText
            style={{
              fontSize: 12,
              fontWeight: '400',
              color: colors.rgb_153,
              marginLeft: 'auto',
              alignItems: 'flex-end',
              marginTop: 5,
            }}>
            <Icon
              type="AntDesign"
              name="staro"
              size={15}
              color={colors.orange}
            />{' '}
            ({item.rate}) Valoración
          </CustomText>
        </View>
        <CustomText
          numberOfLines={5}
          style={{
            fontSize: 14,
            fontWeight: '400',
            color: colors.rgb_153,
            textAlign: 'justify',
            padding: 15,
          }}>
          {item.coment}
        </CustomText>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers
          navigation={Navigation}
          title="Opiniones de clientes"
          back={true}
        />
      </View>
      <CustomText
        light={colors.black}
        dark={colors.white}
        numberOfLines={1}
        style={styles.chats}>
        Opiniones de clientes
      </CustomText>
      <Query query={PROFESSIONAL_RATING_QUERY} variables={{id: data}}>
        {(response, error, loading, refetch) => {
          if (loading) {
            return <ActivityIndicator size="large" color={colors.green_main} />;
          }
          if (error) {
            return console.log('Response Error-------', error);
          }
          if (response) {
            console.log(response);
            return (
              <ScrollView
                refreshControl={
                  <RefreshControl
                    refreshing={refreshing}
                    onRefresh={_onRefresh}
                  />
                }
                showsVerticalScrollIndicator={false}>
                <View style={{marginBottom: dimensions.Height(15)}}>
                  <FlatList
                    data={
                      response &&
                      response.data &&
                      response.data.getProfessionalRating
                        ? response.data.getProfessionalRating.list
                        : ''
                    }
                    renderItem={item => _renderItem(item)}
                    keyExtractor={item => item.id}
                    showsVerticalScrollIndicator={false}
                    ListEmptyComponent={
                      <NoData menssge="Aún no tienes valoraciones" />
                    }
                  />
                </View>
              </ScrollView>
            );
          }
        }}
      </Query>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  card: {
    width: dimensions.Width(95),
    height: 'auto',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    alignSelf: 'center',
    borderRadius: dimensions.Width(3),
    margin: dimensions.Width(3),
    padding: dimensions.Height(2),
  },

  chats: {
    fontSize: dimensions.FontSize(32),
    margin: dimensions.Width(4),
    marginTop: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
});
