import React from 'react'
import {connect} from 'react-redux';
import {Text} from 'react-native';
import Loading from './../components/Loading'
import { chatConnectAndSubscribe } from '../actionCreators'

class CheckConnection extends React.PureComponent {

  state={
    prof: {}
  }

  async componentDidMount() {
    const { navigation } = this.props;
    this.checkConnectionAndRedirect()
    const data = navigation.state.params.data;
    this.setState({prof: data})
  }

  componentDidUpdate(prevProps) {
    const { connected, loading } = this.props
    if (connected !== prevProps.connected || loading !== prevProps.loading) {
      this.checkConnectionAndRedirect()
    }
  }

  checkConnectionAndRedirect = () => {
    const {
      connectAndSubscribe,
      connected,
      loading,
      navigation,
      session,
    } = this.props
    if (!connected && !loading) {
      connectAndSubscribe()
    } else {
      if (connected && !loading) {
        // navigation.navigate(session ? 'CallScreen' : 'Main')
        if(session){
            navigation.navigate('CallScreen')
          }
      }
    }
  }

  render() {
    return (
      <Loading data={this.state.prof} />
    )
  }

}
const mapStateToProps = ({ chat, webrtc }) => ({
    connected: chat.connected,
    loading: chat.loading,
    session: webrtc.session,
  })
  
  const mapDispatchToProps = {
    connectAndSubscribe: chatConnectAndSubscribe
  }

export default  connect(mapStateToProps,mapDispatchToProps)(CheckConnection)