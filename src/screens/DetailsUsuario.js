import React from 'react';
import {View, Image, TouchableOpacity, Dimensions} from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import {useNavigationParam} from 'react-navigation-hooks';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import {CustomText} from '../components/CustomText';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from '../services/NavigationService';
import {NETWORK_INTERFACE_LINK_AVATAR} from './../constants/config';
import moment from 'moment';
import Mascotas from './Mascotas';
import {ifIphoneX} from 'react-native-iphone-x-helper';

const window = Dimensions.get('window');

export default function DetailsPro() {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const data = useNavigationParam('data');

  var day = moment.unix(
    data && data.item && data.item.suscription
      ? data.item.suscription.current_period_start
      : '',
  );
  var day1 = moment.unix(
    data && data.item && data.item.suscription
      ? data.item.suscription.current_period_end
      : '',
  );

  const act =
    data && data.item && data.item.suscription
      ? data.item.suscription.status
      : '';
  return (
    <View style={styles.container}>
      <ParallaxScrollView
        showsVerticalScrollIndicator={false}
        headerBackgroundColor={new DynamicValue(colors.white, colors.back_dark)}
        stickyHeaderHeight={STICKY_HEADER_HEIGHT}
        parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
        backgroundSpeed={10}
        renderBackground={() => (
          <View key="background">
            <Image
              blurRadius={10}
              style={styles.background}
              source={{
                uri: NETWORK_INTERFACE_LINK_AVATAR + data.item.avatar,
              }}
            />
          </View>
        )}
        renderForeground={() => (
          <View key="parallax-header" style={styles.parallaxHeader}>
            <Image
              style={styles.avatar}
              source={{uri: NETWORK_INTERFACE_LINK_AVATAR + data.item.avatar}}
            />
          </View>
        )}
        renderFixedHeader={() => (
          <View key="fixed-header" style={styles.fixedSection}>
            <View style={styles.fixedSectionText}>
              <TouchableOpacity
                onPress={() => Navigation.goBack(null)}
                style={styles.back}>
                <CustomText>
                  <Icon
                    name="close"
                    type="AntDesign"
                    size={20}
                    style={styles.icon}
                  />
                </CustomText>
              </TouchableOpacity>
            </View>
          </View>
        )}
        renderStickyHeader={() => (
          <View key="sticky-header" style={styles.stickySection}>
            <CustomText
              light={colors.back_dark}
              dark={colors.rgb_153}
              style={styles.stickySectionText}>
              {data.item.nombre} {data.item.apellidos}
            </CustomText>
          </View>
        )}>
        <View style={styles.contenedor}>
          <View
            style={{
              marginTop: dimensions.Height(5),
              paddingHorizontal: dimensions.Width(4),
            }}>
            <View style={{flexDirection: 'row'}}>
              <CustomText
                numberOfLines={1}
                light={colors.back_dark}
                dark={colors.white}
                style={styles.title}>
                {data.item.nombre} {data.item.apellidos}{' '}
                {data.item.isPlus ? (
                  <Icon
                    name="verified"
                    type="Octicons"
                    size={14}
                    style={{
                      alignSelf: 'center',
                      marginTop: 0,
                      marginLeft: 7,
                      color: colors.orange,
                    }}
                  />
                ) : null}
              </CustomText>

              <View style={{marginLeft: 'auto'}}>
                <CustomText
                  style={{
                    color: colors.rgb_102,
                    fontWeight: '300',
                    fontSize: dimensions.FontSize(14),
                    marginLeft: 'auto',
                    marginRight: dimensions.Width(2),
                  }}>
                  <Icon
                    name="enviromento"
                    type="AntDesign"
                    size={14}
                    color={colors.rgb_102}
                  />{' '}
                  {data.item.ciudad}
                </CustomText>
                {act === 'active' ? (
                  <CustomText
                    style={{
                      color: '#90C33C',
                      fontWeight: '300',
                      fontSize: dimensions.FontSize(14),
                      marginLeft: 'auto',
                      marginRight: dimensions.Width(2),
                      marginTop: 10,
                    }}>
                    <Icon
                      name="checkcircle"
                      type="AntDesign"
                      size={14}
                      color="#90C33C"
                    />{' '}
                    Activo
                  </CustomText>
                ) : (
                  <CustomText
                    style={{
                      color: colors.ERROR,
                      fontWeight: '300',
                      fontSize: dimensions.FontSize(14),
                      marginLeft: 'auto',
                      marginRight: dimensions.Width(2),
                      marginTop: 10,
                    }}>
                    <Icon
                      name="closecircle"
                      type="AntDesign"
                      size={14}
                      color={colors.ERROR}
                    />{' '}
                    Inactivo
                  </CustomText>
                )}
              </View>
            </View>
          </View>

          <View style={{width: '100%', justifyContent: 'flex-start'}}>
            <View
              style={{
                justifyContent: 'flex-start',
                flexDirection: 'row',
                marginTop: 20,
                marginLeft: 15,
              }}>
              <View style={styles.qubo}>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.light_white}
                  style={{
                    fontSize: dimensions.FontSize(14),
                    fontWeight: '300',
                    marginBottom: 5,
                  }}>
                  <Icon
                    name="dog"
                    type="MaterialCommunityIcons"
                    size={20}
                    color={colors.main}
                  />
                </CustomText>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.light_white}
                  style={{
                    fontSize: dimensions.FontSize(14),
                    fontWeight: '300',
                    textAlign: 'center',
                  }}>
                  1 Mascota
                </CustomText>
              </View>
              <View style={styles.qubo}>
                <CustomText
                  style={{
                    fontSize: dimensions.FontSize(14),
                    fontWeight: '300',
                    marginBottom: 5,
                  }}>
                  <Icon
                    name="videocamera"
                    type="AntDesign"
                    size={20}
                    color={colors.main1}
                  />
                </CustomText>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.light_white}
                  style={{
                    fontSize: dimensions.FontSize(14),
                    fontWeight: '300',
                    textAlign: 'center',
                  }}>
                  {data.item.setVideoConsultas} Llamada realizada
                </CustomText>
              </View>
            </View>
          </View>

          <View
            style={{
              marginTop: dimensions.Height(3),
              paddingHorizontal: dimensions.Width(4),
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.white}
              style={styles.subtitle}>
              Detalles de la suscripción
            </CustomText>
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: dimensions.Width(0),
                borderTopColor: colors.rgb_235,
                borderTopWidth: 0.5,
                marginTop: 10,
              }}>
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={styles.prof}>
                Inicio
              </CustomText>
              <CustomText
                style={{
                  color: colors.rgb_102,
                  fontWeight: '300',
                  fontSize: dimensions.FontSize(14),
                  marginLeft: 'auto',
                  marginRight: dimensions.Width(2),
                  marginTop: 8,
                }}>
                {moment(Number(day)).format('LL')}
              </CustomText>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: dimensions.Width(0),
                borderTopColor: colors.rgb_235,
                borderTopWidth: 0.5,
                marginTop: 10,
              }}>
              <CustomText
                light={colors.rgb_153}
                dark={colors.rgb_153}
                style={styles.prof}>
                Fin
              </CustomText>
              <CustomText
                style={{
                  color: colors.rgb_102,
                  fontWeight: '300',
                  fontSize: dimensions.FontSize(14),
                  marginLeft: 'auto',
                  marginRight: dimensions.Width(2),
                  marginTop: 8,
                }}>
                {moment(Number(day1)).format('LL')}
              </CustomText>
            </View>
          </View>

          <View
            style={{
              marginTop: dimensions.Height(3),
              paddingHorizontal: dimensions.Width(4),
            }}>
            <CustomText
              light={colors.back_dark}
              dark={colors.white}
              style={styles.subtitle}>
              Mascotas
            </CustomText>
            <Mascotas id={data.item.id} pro={data.data} />
          </View>
        </View>
      </ParallaxScrollView>
    </View>
  );
}

const AVATAR_SIZE = 140;
const ROW_HEIGHT = 140;
const PARALLAX_HEADER_HEIGHT = 330;
const STICKY_HEADER_HEIGHT = 90;

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    alignItems: 'center',
    width: dimensions.ScreenWidth,
    height: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  imagenbg: {
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    justifyContent: 'flex-end',
  },

  back: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    padding: 10,
    borderRadius: 10,
  },

  fixedSectionText: {
    fontSize: 16,
    marginRight: dimensions.Width(2),
  },

  stickySectionText: {
    fontSize: 20,
    margin: 10,
    ...ifIphoneX(
      {
        left: 50,
      },
      {
        left: 45,
      },
    ),
    marginTop: 0,
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 5,
      },
    ),
  },
  fixedSection: {
    position: 'absolute',
    bottom: 0,
    right: 'auto',
    left: 10,
    flexDirection: 'row',
    ...ifIphoneX(
      {
        bottom: 0,
      },
      {
        bottom: 10,
      },
    ),
  },

  fixedSectionText1: {
    fontSize: 16,
    marginRight: 15,
    width: 35,
    height: 35,
    borderRadius: 25,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  parallaxHeader: {
    alignItems: 'center',
    flex: 1,
    width: dimensions.ScreenWidth,
    flexDirection: 'column',
    paddingTop: dimensions.Height(8),
    paddingHorizontal: dimensions.Width(4),
  },
  avatar: {
    borderRadius: 20,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    ...ifIphoneX(
      {
        marginTop: dimensions.Height(2),
      },
      {
        marginTop: dimensions.Height(6),
      },
    ),
  },
  sectionSpeakerText: {
    color: 'white',
    fontSize: 24,
    paddingVertical: 5,
  },
  sectionTitleText: {
    color: 'white',
    fontSize: 14,
    textAlign: 'left',
    paddingVertical: 5,
  },
  row: {
    overflow: 'hidden',
    paddingHorizontal: 10,
    height: ROW_HEIGHT,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderColor: '#ccc',
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  contenedor: {
    width: dimensions.ScreenWidth,
    height: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
    marginTop: dimensions.Height(-3),
  },

  icon: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },

  title: {
    fontSize: dimensions.FontSize(28),
    fontWeight: 'bold',
    width: dimensions.Width(60),
  },

  subtitle: {
    fontSize: dimensions.FontSize(20),
    fontWeight: '400',
  },
  prof: {
    fontSize: dimensions.FontSize(18),
    fontWeight: '300',
    marginTop: dimensions.Height(1),
  },

  des: {
    fontSize: dimensions.FontSize(16),
    fontWeight: '300',
    marginTop: dimensions.Height(1),
  },

  qubo: {
    flexDirection: 'column',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    padding: 10,
    width: 90,
    height: 90,
    borderRadius: 15,
    marginRight: 20,
  },

  cardop: {
    width: dimensions.Width(92),
    height: dimensions.Height(10),
    paddingHorizontal: dimensions.Width(4),
    flexDirection: 'row',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginTop: dimensions.Height(3),
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  rating: {
    alignSelf: 'center',
    fontSize: dimensions.FontSize(50),
    fontWeight: 'bold',
  },

  fixs: {
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    height: dimensions.Height(12),
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 15,
    shadowOpacity: 0.2,
    elevation: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: dimensions.Height(3),
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(40),
    borderRadius: dimensions.Width(8),
    marginHorizontal: dimensions.Height(1),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1.5),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },
});
