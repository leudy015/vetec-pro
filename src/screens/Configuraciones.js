import React from 'react';
import {View, FlatList, TouchableOpacity, Linking} from 'react-native';
import {CustomText} from '../components/CustomText';
import Navigation from '../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import Icon from 'react-native-dynamic-vector-icons';

const datos = [
  {
    name: 'twitter',
    type: 'AntDesign',
    color: colors.twitter_color,
    text: 'Siguenos en twitter',
    link: 'https://twitter.com/vetecapp',
  },

  {
    name: 'facebook-square',
    type: 'AntDesign',
    color: '#3b5998',
    text: 'Siguenos en facebook',
    link: 'https://facebook.com/vetecapp',
  },
  {
    name: 'instagram',
    type: 'AntDesign',
    color: '#c08d64',
    text: 'Siguenos en instagram',
    link: 'https://instagram.com/vetecapp',
  },
  {
    name: 'linkedin-square',
    type: 'AntDesign',
    color: '#0072b1',
    text: 'Siguenos en Linkedin',
    link: 'https://www.linkedin.com/company/vetec-app/',
  },
  {
    name: 'aliwangwang-o1',
    type: 'AntDesign',
    color: colors.main,
    text: 'Blog',
    link: 'https://blog.vetec.es',
  },

  {
    name: 'Safety',
    type: 'AntDesign',
    color: colors.main,
    text: 'Política de privacidad',
    link: 'https://vetec.es/privacidad',
  },

  {
    name: 'filetext1',
    type: 'AntDesign',
    color: colors.main,
    text: 'Condiciones de uso',
    link: 'https://vetec.es/condiciones',
  },

  {
    name: 'mail',
    type: 'AntDesign',
    color: colors.main,
    text: 'Contacto',
    link: 'mailto:info@vetec.es',
  },
];

export default function Configuraciones() {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const _renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={styles.cardinfo}
        onPress={() =>
          Linking.canOpenURL(item.link).then(supported => {
            if (supported) {
              Linking.openURL(item.link);
            } else {
              console.log("Don't know how to open URI: " + item.link);
            }
          })
        }>
        <View style={{alignSelf: 'center', marginLeft: dimensions.Width(4)}}>
          <Icon
            name={item.name}
            type={item.type}
            size={18}
            color={item.color}
          />
        </View>
        <View style={{alignSelf: 'center', marginLeft: dimensions.Width(4)}}>
          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={styles.name1}>
            {item.text}
          </CustomText>
        </View>

        <View
          style={{
            alignSelf: 'center',
            marginLeft: 'auto',
            marginRight: dimensions.Width(4),
          }}>
          <Icon
            name="right"
            type="AntDesign"
            size={20}
            color={colors.rgb_153}
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers navigation={Navigation} title="Configuraciones" back={true} />
      </View>
      <CustomText
        light={colors.black}
        dark={colors.white}
        numberOfLines={1}
        style={styles.chats}>
        Contacto
      </CustomText>
      <View style={{marginBottom: dimensions.Height(25)}}>
        <FlatList
          data={datos}
          renderItem={item => _renderItem(item)}
          keyExtractor={item => item.id}
          showsVerticalScrollIndicator={false}
        />
      </View>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: '100%',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  cardinfo: {
    width: dimensions.Width(95),
    height: 80,
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginLeft: dimensions.Width(2.5),
    marginTop: dimensions.Height(2),
    borderRadius: 10,
    flexDirection: 'row',
  },

  chats: {
    fontSize: dimensions.FontSize(32),
    margin: dimensions.Width(4),
    marginTop: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
});
