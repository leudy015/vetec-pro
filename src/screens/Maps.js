import React from 'react';
import {View} from 'react-native';
import {ButtomBar} from './../components/ButtomBar';
import Navigation from './../services/NavigationService';
import MapView, {Marker} from 'react-native-maps';
import {dimensions, colors} from '../themes';
import ProfesionalMaps from '../components/Profesionalmaps';
import LinearGradient from 'react-native-linear-gradient';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  useDarkModeContext,
} from 'react-native-dark-mode';

const backgroundColors = {
  light: 'transparent',
  dark: 'rgba(0,0,0,0.2)',
};

const backgroundColors1 = {
  light: 'transparent',
  dark: colors.back_dark,
};

export default function Maps() {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const mode = useDarkModeContext();
  const backgroundColor = backgroundColors[mode];
  const backgroundColor1 = backgroundColors1[mode];

  const pinkyGradient = ['transparent', backgroundColor, backgroundColor1];

  return (
    <View style={styles.container}>
      <View
        style={{height: dimensions.Height(90), width: dimensions.Width(100)}}>
        <MapView
          style={{height: dimensions.Height(100), width: dimensions.Width(100)}}
          initialRegion={{
            latitude: 42.346325,
            longitude: -3.7310873,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        />
        <Marker coordinate={{latitude: 42.346325, longitude: -3.7310873}} />
        <LinearGradient colors={pinkyGradient} style={styles.cars}>
          <ProfesionalMaps navigation={Navigation} />
        </LinearGradient>
      </View>
      <ButtomBar navigation={Navigation} />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
  },
  cars: {
    width: dimensions.ScreenWidth,
    height: dimensions.Height(40),
    position: 'absolute',
    marginTop: dimensions.Height(55),
  },
});
