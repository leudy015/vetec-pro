import React, {useState} from 'react';
import {View, Alert, TouchableOpacity} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {connect} from 'react-redux';
import {colors, dimensions} from './../themes';
import {CustomText} from './../components/CustomText';
import {Button} from './../components/Button';
import AsyncStorage from '@react-native-community/async-storage';
import {Mutation, withApollo} from 'react-apollo';
import Navigation from './../services/NavigationService';
import {AUTENTICAR_PROFESIONAL} from './../mutations';
import {setUser} from './../actionCreators/user';
import {loginRequest, usersCreate, usersUpdate} from './../actionCreators';
import Headers from '../components/Header';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {showError} from '../services/FlashMessageService';
import {OutlinedTextField} from 'react-native-material-textfield';
import Icon from 'react-native-dynamic-vector-icons';
import {ifIphoneX} from 'react-native-iphone-x-helper';

function Login({setUser, createUser, updateUser, signIn}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  const [names, setNames] = useState('eye');

  const setmostrar = () => {
    if (secureTextEntry) {
      setSecureTextEntry(false);
      setNames('eye-off');
    } else {
      setSecureTextEntry(true);
      setNames('eye');
    }
  };

  const submit = (login, username) => {
    console.log('loginlogin', login, username);
    //const { createUser, signIn } = this.props
    new Promise((resolve, reject) => {
      signIn({login, resolve, reject});
    })
      .then(action => {
        checkIfUsernameMatch(username, action.payload.user);
      })
      .catch(action => {
        const {error} = action;
        if (error.toLowerCase().indexOf('unauthorized') > -1) {
          new Promise((resolve, reject) => {
            createUser({
              fullName: username,
              login,
              password: 'quickblox',
              resolve,
              reject,
            });
          })
            .then(() => {
              this.submit(login, username);
            })
            .catch(userCreateAction => {
              const {error} = userCreateAction;
              if (error) {
                showError('Failed to create user account', error);
              }
            });
        } else {
          showError('Failed to sign in', error);
        }
      });
  };

  const checkIfUsernameMatch = (username, user) => {
    const {updateUser} = this.props;
    const update =
      user.fullName !== username
        ? new Promise((resolve, reject) =>
            updateUser({
              fullName: username,
              login: user.login,
              resolve,
              reject,
            }),
          )
        : Promise.resolve();
    update.then(this.connectAndRedirect).catch(action => {
      if (action && action.error) {
        showError('Failed to update user', action.error);
      }
    });
  };

  const onCompletedLogin = async mutation => {
    if ((!password, !email)) {
      Alert.alert(
        'Error con el inicio de sesión',
        'Todos los campos son obligatorio para el inicio de sesión',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return null;
    }

    await mutation().then(async res => {
      let response = res.data.autenticarProfesional;
      if (!response.success) {
        Alert.alert(
          'Error con el Inicio de sesión',
          response.message,
          [{text: 'OK', onPress: () => console.log('OK Pressed')}],
          {cancelable: false},
        );
        return null;
      } else {
        await AsyncStorage.setItem('token', response.data.token);
        await AsyncStorage.setItem('id', response.data.id);
        let new_email = email;
        submit(new_email, new_email);
        setUser(response.data);
        let very = {
          id: response.data.id,
          fromSocial: true,
        };

        if (response.data.verifyPhone) {
          Navigation.navigate('Home');
        } else {
          Navigation.navigate('VerifyPhone', {data: very});
        }
      }
    });
  };

  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <Mutation
      mutation={AUTENTICAR_PROFESIONAL}
      variables={{email: email, password: password}}>
      {mutation => {
        return (
          <View style={styles.container}>
            <View>
              <Headers
                navigation={Navigation}
                title="Iniciar sesión"
                back={true}
                nologin={true}
              />
            </View>
            <View style={styles.contenedor}>
              <KeyboardAwareScrollView
                keyboardShouldPersistTaps="always"
                showsVerticalScrollIndicator={false}
                style={{marginBottom: dimensions.Height(10)}}>
                <View
                  style={{
                    marginTop: dimensions.Height(5),
                    marginLeft: dimensions.Width(8),
                    marginBottom: dimensions.Height(5),
                  }}>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.rgb_153}
                    style={styles.h1}>
                    Bienvenido de
                  </CustomText>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.rgb_153}
                    style={styles.h1}>
                    nuevo a Vetec PRO!
                  </CustomText>
                </View>
                <View style={styles.formView}>
                  <OutlinedTextField
                    label="Email"
                    placeholder="Email"
                    keyboardType="default"
                    inputContainerStyle={{color: colors.rgb_153}}
                    textColor={colors.rgb_153}
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setEmail(values)}
                  />

                  <OutlinedTextField
                    label="Contraseña"
                    keyboardType="default"
                    secureTextEntry={secureTextEntry}
                    inputContainerStyle={{
                      color: colors.rgb_153,
                      marginTop: 20,
                    }}
                    textColor={colors.rgb_153}
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholder="Contraseña"
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setPassword(values)}
                  />
                  <TouchableOpacity
                    onPress={() => setmostrar()}
                    style={{
                      alignSelf: 'flex-end',
                      marginTop: dimensions.Height(0),
                      marginBottom: dimensions.Height(0),
                    }}>
                    <CustomText light={colors.back_dark} dark={colors.rgb_102}>
                      <Icon
                        name={names}
                        type="MaterialCommunityIcons"
                        size={20}
                        color={colors.rgb_102}
                      />
                    </CustomText>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => Navigation.navigate('Recovery')}
                    style={{
                      marginLeft: 'auto',
                      marginTop: dimensions.Height(3),
                    }}>
                    <CustomText light={colors.main} dark={colors.main}>
                      ¿Olvidaste tu contraseña?
                    </CustomText>
                  </TouchableOpacity>
                  <View style={styles.signupButtonContainer}>
                    <Button
                      light={colors.white}
                      dark={colors.white}
                      containerStyle={styles.buttonView}
                      onPress={() => onCompletedLogin(mutation)}
                      title="Iniciar sesión"
                      titleStyle={styles.buttonTitle}
                    />
                  </View>

                  <View
                    style={{
                      alignSelf: 'center',
                      marginTop: dimensions.Height(5),
                    }}>
                    <CustomText light={colors.back_dark} dark={colors.rgb_153}>
                      ¿Aún no tienes una cuenta?
                    </CustomText>
                  </View>

                  <TouchableOpacity
                    onPress={() => Navigation.navigate('Register')}
                    style={{
                      alignSelf: 'center',
                      marginTop: dimensions.Height(5),
                      marginBottom: dimensions.Height(25),
                    }}>
                    <CustomText light={colors.main} dark={colors.main}>
                      Regístrarme
                    </CustomText>
                  </TouchableOpacity>

                  <View />
                </View>
              </KeyboardAwareScrollView>
            </View>
          </View>
        );
      }}
    </Mutation>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
  },
  formView: {
    marginHorizontal: dimensions.Width(8),
    marginTop: dimensions.Height(4),
  },
  rememberMeView: {
    marginTop: dimensions.Height(2),
    flexDirection: 'row',
  },
  rememberText: {
    fontSize: dimensions.FontSize(18),
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
  },
  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },
  contenedor: {
    width: dimensions.ScreenWidth,
    height: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  h1: {
    fontSize: dimensions.FontSize(30),
    fontWeight: 'bold',
    textAlign: 'left',
  },
});

const mapDispatchToProps = {
  setUser: setUser,
  signIn: loginRequest,
  createUser: usersCreate,
  updateUser: usersUpdate,
};

export default connect(
  null,
  mapDispatchToProps,
)(withApollo(Login));
