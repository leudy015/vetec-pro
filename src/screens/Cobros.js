import React, {useEffect, useState} from 'react';
import {
  View,
  ScrollView,
  Alert,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {CustomText} from '../components/CustomText';
import Navigation from '../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import {NUEVO_PAGO, ELIMINAR_PAGO} from '../mutations';
import {PAGO_QUERY} from '../query';
import {Query, Mutation} from 'react-apollo';
import Icon from 'react-native-dynamic-vector-icons';
import {Button} from '../components/Button';
import {OutlinedTextField} from 'react-native-material-textfield';

export default function Pago() {
  const [id, setId] = useState(null);
  const [iban, setIbam] = useState('');
  const [nombre, setNombre] = useState('');

  const styles = useDynamicStyleSheet(dynamicStyles);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  return (
    <View style={styles.container}>
      <View>
        <Headers
          navigation={Navigation}
          title="Mis datos bancarios"
          back={true}
        />
      </View>
      <CustomText
        light={colors.black}
        dark={colors.white}
        numberOfLines={1}
        style={styles.chats}>
        Mis datos bancarios
      </CustomText>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Query query={PAGO_QUERY} variables={{id: id}}>
          {({loading, data, refetch}) => {
            refetch = refetch;
            if (loading) {
              return <ActivityIndicator />;
            } else {
              let response = {data: data};

              const datos =
                response && response.data && response.data.getPago
                  ? response.data.getPago.data
                  : '';
              const isDisableCard = datos ? datos.id : '';
              return (
                <View style={{paddingHorizontal: dimensions.Height(3)}}>
                  {isDisableCard ? (
                    <View style={styles.payment}>
                      <View>
                        <Icon type="AntDesign" name="wallet" size={20} />
                      </View>
                      <View style={{marginLeft: 10}}>
                        <CustomText
                          light={colors.blue_main}
                          dark={colors.white}
                          style={{
                            marginLeft: 15,
                            fontWeight: '500',
                            fontSize: dimensions.FontSize(16),
                          }}>
                          {datos.nombre}
                        </CustomText>
                        <CustomText
                          light={colors.rgb_153}
                          dark={colors.white}
                          style={{
                            marginLeft: 15,
                            fontWeight: '300',
                            fontSize: dimensions.FontSize(14),
                          }}>
                          {datos.iban}
                        </CustomText>
                      </View>
                      <Mutation mutation={ELIMINAR_PAGO}>
                        {eliminarPago => {
                          return (
                            <View
                              style={{
                                marginLeft: 'auto',
                                alignSelf: 'center',
                                marginRight: 15,
                              }}>
                              <TouchableOpacity
                                onPress={() => {
                                  eliminarPago({
                                    variables: {id: datos.id},
                                  }).then(async ({data: res}) => {
                                    if (
                                      res &&
                                      res.eliminarPago &&
                                      res.eliminarPago.success
                                    ) {
                                      Toast.showWithGravity(
                                        res.eliminarPago.message,
                                        Toast.LONG,
                                        Toast.TOP,
                                      );
                                      refetch();
                                      console.log(res.eliminarPago.message);
                                    } else if (
                                      res &&
                                      res.eliminarPago &&
                                      !res.eliminarPago.success
                                    )
                                      console.log(res.eliminarPago.message);
                                  });
                                }}>
                                <Icon
                                  type="AntDesign"
                                  name="delete"
                                  size={25}
                                  color={colors.ERROR}
                                />
                              </TouchableOpacity>
                            </View>
                          );
                        }}
                      </Mutation>
                    </View>
                  ) : null}

                  {!isDisableCard ? (
                    <Mutation mutation={NUEVO_PAGO}>
                      {crearPago => {
                        return (
                          <View>
                            <View style={styles.cars}>
                              <CustomText
                                light={colors.back_dark}
                                dark={colors.rgb_153}
                                style={styles.texto}>
                                Datos Bancarios
                              </CustomText>
                              <OutlinedTextField
                                label="Nombe completo"
                                keyboardType="default"
                                inputContainerStyle={{color: colors.rgb_153}}
                                textColor={colors.rgb_153}
                                labelTextStyle={{color: colors.rgb_153}}
                                placeholderTextColor={colors.rgb_153}
                                onChangeText={values => setNombre(values)}
                              />

                              <OutlinedTextField
                                label="IBAN bancarío"
                                keyboardType="default"
                                inputContainerStyle={{
                                  color: colors.rgb_153,
                                  marginTop: 20,
                                }}
                                textColor={colors.rgb_153}
                                labelTextStyle={{color: colors.rgb_153}}
                                placeholderTextColor={colors.rgb_153}
                                maxLength={24}
                                onChangeText={values => setIbam(values)}
                              />
                            </View>
                            <View>
                              <Button
                                light={colors.white}
                                dark={colors.white}
                                containerStyle={styles.buttonView1}
                                onPress={() => {
                                  if ((nombre == '', iban == '')) {
                                    Alert.alert(
                                      'Error al guardar',
                                      'Todos los campos son obligatorio para para guardar tu método de pago',
                                      [
                                        {
                                          text: 'OK',
                                          onPress: () =>
                                            console.log('OK Pressed'),
                                        },
                                      ],
                                      {cancelable: false},
                                    );
                                    return null;
                                  }

                                  const input = {
                                    iban: iban,
                                    nombre: nombre,
                                    profesionalID: id,
                                  };
                                  crearPago({variables: {input}})
                                    .then(async ({data: res}) => {
                                      if (
                                        res &&
                                        res.crearPago &&
                                        res.crearPago.success
                                      ) {
                                        Toast.showWithGravity(
                                          res.crearPago.message,
                                          Toast.LONG,
                                          Toast.TOP,
                                        );
                                        console.log(res.crearPago.message);
                                        refetch();
                                      }
                                    })
                                    .catch(err => {
                                      console.log(err);
                                    });
                                }}
                                title="Guardar cambios"
                                titleStyle={styles.buttonTitle}
                              />
                            </View>
                          </View>
                        );
                      }}
                    </Mutation>
                  ) : null}
                </View>
              );
            }
          }}
        </Query>
      </ScrollView>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(15),
    borderTopEndRadius: 30,
    borderTopStartRadius: 30,
  },

  payment: {
    height: 'auto',
    alignSelf: 'center',
    width: dimensions.Width(100),
    backgroundColor: 'transparent',
    flexDirection: 'row',
    padding: 15,
    marginTop: 20,
    borderRadius: 10,
    paddingVertical: dimensions.Width(4),
    borderBottomWidth: 0.5,
    borderBottomColor: colors.rgb_235,
    borderTopWidth: 0.5,
    borderTopColor: colors.rgb_235,
  },

  buttonView: {
    marginTop: dimensions.Height(4),
    alignSelf: 'center',
    backgroundColor: colors.ERROR,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
  },
  buttonView1: {
    marginTop: dimensions.Height(3),
    alignSelf: 'center',
    backgroundColor: colors.blue_main,
    width: dimensions.Width(85),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },
  formView: {
    marginHorizontal: dimensions.Width(8),
    flex: 1,
    marginTop: dimensions.Height(4),
  },
  ted: {
    fontSize: dimensions.FontSize(18),
    marginTop: 20,
  },

  cars: {
    width: dimensions.Width(88),
    height: 'auto',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    borderRadius: dimensions.Width(2),
    margin: 2,
    marginBottom: dimensions.Height(5),
    padding: 20,
  },

  texto: {
    fontSize: dimensions.FontSize(18),
    marginBottom: dimensions.Height(3),
  },

  chats: {
    fontSize: dimensions.FontSize(32),
    margin: dimensions.Width(4),
    marginTop: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
});
