import React, {useState} from 'react';
import {
  View,
  Linking,
  TouchableOpacity,
  Alert,
  Modal,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import {CustomText} from '../components/CustomText';
import Navigation from './../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {useNavigationParam} from 'react-navigation-hooks';
import ImagePicker from 'react-native-image-picker';
import {Avatar} from 'react-native-elements';
import {Button} from './../components/Button';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  ACTUALIZAR_PROFESIONAL,
  UPLOAD_FILE,
  ELIMINAR_PROFESIONAL,
} from '../mutations';
import {Mutation} from 'react-apollo';
import {Switch} from '@ant-design/react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import Icon from 'react-native-dynamic-vector-icons';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import {OutlinedTextField} from 'react-native-material-textfield';
import SelectInput from 'react-native-select-input-ios';

export default function UpdateProfile() {
  const data = useNavigationParam('data');

  const [email, setEmail] = useState(data.email);
  const [category_id, setCategory_id] = useState(data.category_id);
  const [nombre, setNombre] = useState(data.nombre);
  const [apellidos, setApellidos] = useState(data.apellidos);
  const [ciudad, setCiudad] = useState(data.ciudad);
  const [telefono, setTelefono] = useState(data.telefono);
  const [notificacion, setNotificacion] = useState(data.notificacion);
  const [isAvailable, setIsAvailable] = useState(data.isAvailable);
  const [avatar, setAvatar] = useState(data.avatar);
  const [profesion, setProfesion] = useState(data.profesion);
  const [experiencia, setExperiencia] = useState(data.experiencia);
  const [descricion, setDescricion] = useState(data.descricion);
  const [Precio, setPrecio] = useState(data.Precio);
  const [modalVisible, setModalVisible] = useState(false);

  const styles = useDynamicStyleSheet(dynamicStyles);

  let very = {
    id: data.id,
    fromProfile: true,
  };

  const onchangecat = values => {
    setCategory_id(values);
  };

  const openModal = () => {
    setModalVisible(true);
  };

  const handleActualizar = async mutation => {
    const input = {
      id: data.id,
      email,
      category_id,
      nombre,
      apellidos,
      ciudad,
      telefono,
      notificacion,
      isAvailable,
      avatar,
      profesion,
      experiencia,
      descricion,
      Precio,
    };

    if (!avatar) {
      Alert.alert(
        'Debes añadir una foto de perfil',
        'Para guardar los cambios en tu perfil debes añadir una foto de perfil.',
        [
          {
            text: 'Ok',
            onPress: () => console.log('calcel'),
          },
        ],

        {cancelable: false},
      );
      return null;
    } else {
      await mutation({variables: {input}})
        .then(res => {
          console.log('done', res);
          Toast.showWithGravity(
            'Perfil actualizado con éxito',
            Toast.LONG,
            Toast.TOP,
          );
        })
        .catch(err => console.log(err));
    }
  };

  const onSwitchChange = () => {
    if (notificacion === true) {
      setNotificacion(false);
    } else {
      setNotificacion(true);
    }
  };

  const onSwitchChangeAva = () => {
    if (isAvailable === true) {
      setIsAvailable(false);
    } else {
      setIsAvailable(true);
    }
  };

  const options = [
    {
      value: '5ea8a50a43a7ca27aa0e739e',
      label: 'Perros y Gatos',
    },
    {
      value: '5ea97d434ca0f0353a15aad8',
      label: 'Aves',
    },
    {
      value: '5ea9809b4ca0f0353a15aada',
      label: 'Ganadería veterinaria',
    },
    {
      value: '5ea982d24ca0f0353a15aadb',
      label: 'Apicultura',
    },
    {
      value: '5ea9847f4ca0f0353a15aadc',
      label: 'Reptiles',
    },
    {
      value: '5ea987ad4ca0f0353a15aadd',
      label: 'Peces',
    },
  ];

  const handleEliminar = eliminarProfesional => {
    return new Promise((resolve, reject) => {
      eliminarProfesional({variables: {id: data.id}}).then(async ({data}) => {
        if (
          data &&
          data.eliminarProfesional &&
          data.eliminarProfesional.success
        ) {
          setTimeout(async () => {
            await AsyncStorage.removeItem('token');
            await AsyncStorage.removeItem('id');
            console.log(data.eliminarProfesional.message);
            setTimeout(() => Navigation.navigate('Landing'), 300);
          }, 200);
          resolve();
        } else if (
          data &&
          data.eliminarProfesional &&
          !data.eliminarProfesional.success
        )
          setTimeout(() => {
            console.log(data.eliminarProfesional.message);
          }, 500);
        reject();
      });
    }).catch(() => console.log('error confirming eliminar usuario!'));
  };

  const selectPhotoTapped = singleUpload => {
    const options = {
      quality: 0.5,
      title: 'Seleccionar Avatar',
      cancelButtonTitle: 'Cancelar',
      takePhotoButtonTitle: 'Hacer foto',
      chooseFromLibraryButtonTitle: 'Seleccionar foto existente',
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        storageOptions: {
          skipBackup: true,
        },
      },
    };
    ImagePicker.showImagePicker(options, async response => {
      if (response) {
        const imgBlob = 'data:image/jpeg;base64,' + response.data;
        if (imgBlob) {
          singleUpload({variables: {imgBlob}})
            .then(res => {
              console.log('filemane: ', res.data.singleUpload.filename);
              setAvatar(res.data.singleUpload.filename);
            })
            .catch(error => {
              console.log('fs error: que me arroja', error);
            });
        }
      }
    });
  };

  const handleClick = () => {
    Linking.canOpenURL(
      `mailto:info@vetec.es?subject=Solicitud de Verificación Profesional ID No. ${
        data.id
      }`,
    ).then(supported => {
      if (supported) {
        Linking.openURL(
          `mailto:info@vetec.es?subject=Solicitud de Verificación Profesional ID No. ${
            data.id
          }`,
        );
        console.log('calling: ' + 'mailto:info@vetec.es');
      } else {
        console.log("Don't know how to open URI: " + 'mailto:info@vetec.es');
      }
    });
  };

  return (
    <Mutation mutation={ACTUALIZAR_PROFESIONAL}>
      {mutation => (
        <View style={styles.container}>
          <View>
            <Headers navigation={Navigation} title="Actualizar perfil" back={true} />
          </View>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
            <View style={{alignSelf: 'center'}}>
              <Mutation mutation={UPLOAD_FILE}>
                {singleUpload => (
                  <TouchableOpacity
                    onPress={() => selectPhotoTapped(singleUpload)}>
                    <Avatar
                      rounded
                      size={120}
                      source={{uri: NETWORK_INTERFACE_LINK_AVATAR + avatar}}
                      containerStyle={styles.avatarst}
                      showEditButton
                    />
                  </TouchableOpacity>
                )}
              </Mutation>
            </View>
            <View style={{flexDirection: 'row', alignSelf: 'center'}}>
              <CustomText
                light={colors.back_dark}
                dark={colors.light_white}
                style={styles.name}>
                {data.nombre} {data.apellidos}
              </CustomText>
              {data.isVerified ? (
                <Icon
                  name="verified"
                  type="Octicons"
                  size={14}
                  style={{
                    alignSelf: 'center',
                    marginTop: 13,
                    marginLeft: 7,
                    color: colors.twitter_color,
                  }}
                />
              ) : null}
            </View>
            <View style={styles.formView}>
              <View style={styles.cars}>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.rgb_153}
                  style={styles.texto}>
                  Información personal
                </CustomText>
                <OutlinedTextField
                  label="Nombre"
                  keyboardType="default"
                  inputContainerStyle={{color: colors.rgb_153}}
                  defaultValue={nombre}
                  textColor={colors.rgb_153}
                  labelTextStyle={{color: colors.rgb_153}}
                  placeholderTextColor={colors.rgb_153}
                  onChangeText={values => setNombre(values)}
                />

                <OutlinedTextField
                  label="Apellidos"
                  keyboardType="default"
                  defaultValue={apellidos}
                  inputContainerStyle={{color: colors.rgb_153, marginTop: 20}}
                  textColor={colors.rgb_153}
                  labelTextStyle={{color: colors.rgb_153}}
                  placeholderTextColor={colors.rgb_153}
                  onChangeText={values => setApellidos(values)}
                />

                <OutlinedTextField
                  label="Ciudad"
                  keyboardType="default"
                  defaultValue={ciudad}
                  inputContainerStyle={{color: colors.rgb_153, marginTop: 20}}
                  textColor={colors.rgb_153}
                  labelTextStyle={{color: colors.rgb_153}}
                  placeholderTextColor={colors.rgb_153}
                  onChangeText={values => setCiudad(values)}
                />
              </View>

              <View style={styles.cars}>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.rgb_153}
                  style={styles.texto}>
                  Información de contacto
                </CustomText>
                <OutlinedTextField
                  label="Número móvil"
                  keyboardType="phone-pad"
                  defaultValue={telefono}
                  inputContainerStyle={{color: colors.rgb_153}}
                  textColor={colors.rgb_153}
                  labelTextStyle={{color: colors.rgb_153}}
                  placeholderTextColor={colors.rgb_153}
                  onChangeText={values => setTelefono(values)}
                  onFocus={() =>
                    Navigation.navigate('VerifyPhone', {data: very})
                  }
                />

                <OutlinedTextField
                  label="Email"
                  keyboardType="email-address"
                  inputContainerStyle={{color: colors.rgb_153, marginTop: 20}}
                  defaultValue={email}
                  textColor={colors.rgb_153}
                  labelTextStyle={{color: colors.rgb_153}}
                  placeholderTextColor={colors.rgb_153}
                  onChangeText={values => setEmail(values)}
                />
              </View>

              <View style={styles.cars}>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.rgb_153}
                  style={styles.texto}>
                  Información profesional
                </CustomText>
                <OutlinedTextField
                  label="Profesion"
                  keyboardType="default"
                  defaultValue={profesion}
                  inputContainerStyle={{color: colors.rgb_153}}
                  textColor={colors.rgb_153}
                  labelTextStyle={{color: colors.rgb_153}}
                  placeholderTextColor={colors.rgb_153}
                  onChangeText={values => setProfesion(values)}
                />

                <OutlinedTextField
                  label="Experiencia"
                  keyboardType="default"
                  defaultValue={experiencia}
                  inputContainerStyle={{color: colors.rgb_153, marginTop: 20}}
                  textColor={colors.rgb_153}
                  labelTextStyle={{color: colors.rgb_153}}
                  placeholderTextColor={colors.rgb_153}
                  onChangeText={values => setExperiencia(values)}
                />

                <OutlinedTextField
                  label="Precio por consulta"
                  keyboardType="numeric"
                  defaultValue={Precio}
                  prefix="€"
                  maxLength={2}
                  inputContainerStyle={{color: colors.rgb_153, marginTop: 20}}
                  textColor={colors.rgb_153}
                  labelTextStyle={{color: colors.rgb_153}}
                  placeholderTextColor={colors.rgb_153}
                  onChangeText={values => setPrecio(values)}
                />

                <OutlinedTextField
                  label="Descripción"
                  keyboardType="default"
                  defaultValue={descricion}
                  multiline={true}
                  inputContainerStyle={{color: colors.rgb_153, marginTop: 20}}
                  textColor={colors.rgb_153}
                  labelTextStyle={{color: colors.rgb_153}}
                  placeholderTextColor={colors.rgb_153}
                  onChangeText={values => setDescricion(values)}
                />
              </View>

              <View style={styles.cars}>
                <CustomText
                  light={colors.back_dark}
                  dark={colors.rgb_153}
                  style={styles.texto}>
                  Especialidad
                </CustomText>

                <SelectInput
                  value={category_id}
                  cancelKeyText="Cancelar"
                  submitKeyText="Seleccionar"
                  style={styles.selec}
                  options={options}
                  onValueChange={values => onchangecat(values)}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  marginTop: dimensions.Height(4),
                }}>
                <CustomText light={colors.back_dark} dark={colors.light_white}>
                  Activar notificaciones
                </CustomText>
                <Switch
                  checked={notificacion}
                  onChange={() => onSwitchChange()}
                  style={{marginLeft: 'auto'}}
                  trackColor={colors.main1}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  marginTop: dimensions.Height(4),
                }}>
                <CustomText light={colors.back_dark} dark={colors.light_white}>
                  Disponible para consultas
                </CustomText>
                <Switch
                  checked={isAvailable}
                  onChange={() => onSwitchChangeAva()}
                  style={{marginLeft: 'auto'}}
                  trackColor={colors.main1}
                />
              </View>

              <View style={styles.signupButtonContainer}>
                <Button
                  light={colors.white}
                  dark={colors.white}
                  containerStyle={styles.buttonView}
                  onPress={() => handleActualizar(mutation)}
                  title="Guardar cambios"
                  titleStyle={styles.buttonTitle}
                />

                <View style={styles.signupButtonContainer}>
                  <Button
                    light={colors.white}
                    dark={colors.white}
                    containerStyle={styles.buttonView}
                    onPress={() => openModal()}
                    title="Solicitar verificación"
                    titleStyle={styles.buttonTitle}
                  />
                </View>

                <Modal
                  animationType="slide"
                  presentationStyle="formSheet"
                  transparent={false}
                  visible={modalVisible}
                  onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                  }}>
                  <View style={styles.containerModal}>
                    <SafeAreaView
                      style={{
                        width: dimensions.ScreenWidth,
                        height: dimensions.Height(12),
                      }}>
                      <TouchableOpacity onPress={() => setModalVisible(false)}>
                        <View
                          style={{
                            alignSelf: 'center',
                            marginLeft: 'auto',
                            marginRight: dimensions.Width(4),
                            marginTop: dimensions.Height(2),
                          }}>
                          <Icon
                            name="close"
                            type="AntDesign"
                            size={25}
                            color={colors.ERROR}
                          />
                        </View>
                      </TouchableOpacity>
                    </SafeAreaView>
                    <ScrollView showsVerticalScrollIndicator={false}>
                      <CustomText
                        light={colors.back_dark}
                        dark={colors.light_white}
                        style={styles.name}>
                        Solicitud de Verificación
                      </CustomText>
                      <Icon
                        name="verified"
                        type="Octicons"
                        size={30}
                        style={{
                          alignSelf: 'center',
                          marginTop: 13,
                          marginLeft: 7,
                          color: colors.twitter_color,
                        }}
                      />

                      <View style={styles.contetx}>
                        <CustomText
                          light={colors.back_dark}
                          dark={colors.rgb_153}
                          style={styles.ted}>
                          Gracias por tu interés en la solicitud de verificación
                          de tu cuenta Vetec PRO. ¿Qué es la verificación de
                          Vetec PRO? Es una insignia que se coloca al lado de tu
                          nombre, la cual acredita que eres un profesional
                          cualificado para realizar consultas y que has aportado
                          la documentación necesaria para dicha actividad y ha
                          sido evaluada por Locatefit S.L
                        </CustomText>

                        <CustomText
                          light={colors.back_dark}
                          dark={colors.rgb_153}
                          style={styles.ted}>
                          ¿Qué documentación necesito?
                        </CustomText>

                        <CustomText
                          light={colors.back_dark}
                          dark={colors.rgb_153}
                          style={styles.ted}>
                          - Foto de tu DNI (Ambas caras)
                        </CustomText>

                        <CustomText
                          light={colors.back_dark}
                          dark={colors.rgb_153}
                          style={styles.ted}>
                          - Título universitario
                        </CustomText>

                        <CustomText
                          light={colors.back_dark}
                          dark={colors.rgb_153}
                          style={styles.ted}>
                          - Alta de autónomo
                        </CustomText>

                        <CustomText
                          light={colors.back_dark}
                          dark={colors.rgb_153}
                          style={styles.ted}>
                          - Número de colegiado
                        </CustomText>
                      </View>
                      <View style={styles.signupButtonContainer}>
                        <Button
                          light={colors.white}
                          dark={colors.white}
                          containerStyle={styles.buttonView}
                          onPress={() => handleClick()}
                          title="Enviar solicitud"
                          titleStyle={styles.buttonTitle}
                        />
                      </View>
                    </ScrollView>
                  </View>
                </Modal>
                <View style={{alignSelf: 'center'}}>
                  <Mutation mutation={ELIMINAR_PROFESIONAL}>
                    {eliminarProfesional => {
                      return (
                        <TouchableOpacity
                          onPress={() =>
                            Alert.alert(
                              '¿Deseas eliminar tu cuenta?',
                              'Al eliminar tu cuenta se eliminarán todos los datos y historial clínico de tu mascota.',
                              [
                                {
                                  text: 'Cancelar',
                                  onPress: () => console.log('calcel'),
                                },
                                {
                                  text: 'Eliminar',
                                  onPress: () =>
                                    handleEliminar(eliminarProfesional),
                                },
                              ],

                              {cancelable: false},
                            )
                          }>
                          <CustomText
                            style={{
                              color: colors.ERROR,
                              marginTop: dimensions.Height(4),
                            }}>
                            Eliminar cuenta
                          </CustomText>
                        </TouchableOpacity>
                      );
                    }}
                  </Mutation>
                </View>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
      )}
    </Mutation>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.ScreenHeight,
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
  },

  contetx: {
    width: dimensions.ScreenWidth,
    padding: 20,
  },

  ted: {
    fontSize: dimensions.FontSize(18),
    marginTop: 20,
  },

  cars: {
    width: dimensions.Width(88),
    height: 'auto',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    borderRadius: dimensions.Width(2),
    margin: 2,
    marginBottom: dimensions.Height(5),
    padding: 20,
  },

  texto: {
    fontSize: dimensions.FontSize(18),
    marginBottom: dimensions.Height(3),
  },

  selec: {
    borderWidth: 1,
    borderColor: colors.rgb_153,
    marginBottom: dimensions.Height(2),
    marginTop: dimensions.Height(2),
    padding: dimensions.Height(2),
    borderRadius: dimensions.Width(1),
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(15),
    borderTopEndRadius: 30,
    borderTopStartRadius: 30,
  },

  cardinfo: {
    width: dimensions.Width(90),
    height: dimensions.Height(40),
    backgroundColor: new DynamicValue(colors.light_white, colors.back_dark),
    marginLeft: dimensions.Width(5),
    marginTop: dimensions.Height(3),
    borderRadius: 10,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 12,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  containerModal: {
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    height: dimensions.ScreenHeight,
    alignSelf: 'center',
  },

  avatarst: {
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    borderWidth: 4,
  },

  name: {
    marginTop: dimensions.Height(1),
    alignSelf: 'center',
    fontSize: dimensions.FontSize(24),
  },
  formView: {
    marginHorizontal: dimensions.Width(6),
    marginTop: dimensions.Height(5),
    marginBottom: dimensions.Height(15),
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
    marginTop: dimensions.Height(1),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(17),
  },

  signupButtonContainer: {
    marginTop: 30,
    alignItems: 'center',
  },
});
