import React, {useState, useEffect} from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  ScrollView,
} from 'react-native';
import {CustomText} from '../components/CustomText';
import Loadingchat from './../components/Placeholderchat';
import Navigation from './../services/NavigationService';
import Headers from '../components/Header';
import {dimensions, colors} from '../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {Avatar} from 'react-native-elements';
import {SwipeAction, Badge} from '@ant-design/react-native';
import {NETWORK_INTERFACE_LINK_AVATAR} from '../constants/config';
import NoData from '../components/NoData';
import moment from 'moment';
import {useNavigationParam} from 'react-navigation-hooks';
import Icon from 'react-native-dynamic-vector-icons';
import Toast from 'react-native-simple-toast';
import {GET_MESSAGE_NO_READ, GET_CONVERSATION_PROF} from './../query';
import {READ_MESSAGE, DELETE_CONVERSATION} from './../mutations';
import {Query, Mutation} from 'react-apollo';
import {usersGet} from '../actionCreators';
import {connect} from 'react-redux';

function Chats({getUsers,navigation}) {
  const [refreshing, setRefreshing] = useState(false);
  let refetch_list = false;

  const styles = useDynamicStyleSheet(dynamicStyles);

  const data = useNavigationParam('data');

  const _onRefresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  };

  useEffect(() => {
    getUsers();
    const navFocusListener = navigation.addListener('didFocus', () => {
      // do some API calls here
      if(refetch_list){
        refetch_list()
      }
      console.log('hooooks');
      });

      return () => {
          navFocusListener.remove();
      };
  });

  const _renderItem = ({item}, refetch) => {
    console.log(item);
    refetch_list = refetch;
    const Navegue = (readMessage, id) => {
      console.log(id);
      readMessage({variables: {conversationID: id}})
        .then(() => {
          refetch();
          console.log('leido');
        })
        .catch(err => console.log(err));
      Navigation.navigate('ChatsScreens', {data: item});
    };
    return (
      <View
      style={{
        marginBottom: dimensions.Height(0),
      }}>
      <Mutation
        mutation={DELETE_CONVERSATION}
        variables={{conversationID: item._id}}>
        {deleteConversation => {
          return (
            <Mutation
              mutation={READ_MESSAGE}
              variables={{conversationID: item._id}}>
              {readMessage => {
                return (
                  <SwipeAction
                    autoClose
                    style={{
                      backgroundColor: 'transparent',
                    }}
                    right={[
                      {
                        text: 'Eliminar',
                        style: {
                          backgroundColor: colors.ERROR,
                          color: 'white',
                        },
                        onPress: () => {
                          deleteConversation({
                            variables: {id: item._id},
                          }).then(() => {
                            Toast.showWithGravity(
                              'Conversación eliminada con éxito',
                              Toast.LONG,
                              Toast.TOP,
                            );
                            refetch();
                          });
                        },
                      },
                    ]}>
                    <Query
                      query={GET_MESSAGE_NO_READ}
                      variables={{
                        conversationID: item._id,
                        clientId: item.user,
                      }}>
                      {(response, error) => {
                        if (error) {
                          return console.log('Response Error-------', error);
                        }
                        if (response) {
                          const dat =
                            response &&
                            response.data &&
                            response.data.getMessageNoReadProf
                              ? response.data.getMessageNoReadProf
                                  .conversation[0]
                              : '';
                          response.refetch();
                          return (
                            <TouchableOpacity
                            style={styles.list}
                              onPress={() => Navegue(readMessage, item._id)}>
                              <View style={styles.chatcont}>
                                <Avatar
                                  rounded
                                  size={60}
                                  source={{
                                    uri:
                                      NETWORK_INTERFACE_LINK_AVATAR +
                                      item.usuario.avatar,
                                  }}
                                />
                                <View style={styles.supercont}>
                                  <View style={styles.text_name}>
                                    <CustomText
                                      light={colors.black}
                                      dark={colors.white}
                                      numberOfLines={1}
                                      style={{
                                        fontSize: dimensions.FontSize(20),
                                        maxWidth: '50%',
                                      }}>
                                      {item.usuario.nombre}{' '}
                                      {item.usuario.apellidos}{' '}
                                    </CustomText>
                                    {item.usuario.isPlus ? (
                                      <Icon
                                        type="Octicons"
                                        name="verified"
                                        style={{
                                          alignSelf: 'center',
                                          marginLeft: 2,
                                          marginTop: 5,
                                        }}
                                        size={12}
                                        color={colors.orange}
                                      />
                                    ) : null}
                                    <CustomText
                                      light={colors.black}
                                      dark={colors.white}
                                      numberOfLines={1}
                                      style={styles.fechas}>
                                      {moment(
                                        item.messagechat[0].createdAt,
                                      ).fromNow('LL')}
                                    </CustomText>
                                  </View>
                                  <View style={styles.text_name}>
                                    <CustomText
                                      numberOfLines={2}
                                      style={{
                                        color: colors.rgb_153,
                                        fontWeight: '300',
                                        fontSize: dimensions.FontSize(14),
                                        marginTop: 0,
                                        width: dimensions.Width(60),
                                      }}>
                                      {item.messagechat[0].text}
                                    </CustomText>
                                    <Badge
                                      text={dat && dat.count}
                                      overflowCount={9}
                                      style={styles.fechass}
                                    />
                                  </View>
                                </View>
                              </View>
                            </TouchableOpacity>
                          );
                        }
                      }}
                    </Query>
                  </SwipeAction>
                );
              }}
            </Mutation>
          );
        }}
      </Mutation>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View>
        <Headers
          navigation={Navigation}
          title="Chats"
          back={true}
          atrasprofile={true}
        />
      </View>
      <CustomText
        light={colors.black}
        dark={colors.white}
        numberOfLines={1}
        style={styles.chats}>
        Chats
      </CustomText>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }
        showsVerticalScrollIndicator={false}>
        <Query query={GET_CONVERSATION_PROF} variables={{ProfId: data}}>
          {response => {
            if (response.error) {
              return <Loadingchat />;
            }
            if (response.loading) {
              return <Loadingchat />;
            }
            if (response) {
              response.refetch();
              const Conversation =
                response && response.data && response.data.getConversationProf
                  ? response.data.getConversationProf.conversation
                  : '';
              return (
                <View style={{marginBottom: dimensions.Height(15)}}>
                  <FlatList
                    data={Conversation}
                    showsHorizontalScrollIndicator={false}
                    renderItem={item => _renderItem(item, response.refetch)}
                    keyExtractor={item => item._id}
                    ListEmptyComponent={
                      <NoData menssge="No tienes conversaciones" />
                    }
                  />
                </View>
              );
            }
          }}
        </Query>
      </ScrollView>
      <View />
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(7),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    marginTop: dimensions.Height(13),
    borderTopEndRadius: 10,
    borderTopStartRadius: 10,
  },

  chats: {
    fontSize: dimensions.FontSize(32),
    margin: dimensions.Width(4),
    marginTop: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },

  chatcont: {
    flexDirection: 'row',
    padding: 20,
    width: dimensions.ScreenWidth,
  },

  supercont: {
    marginLeft: 15,
    width: dimensions.Width(68),
  },

  text_name: {
    flexDirection: 'row',
    marginTop: 5,
  },

  fechas: {
    fontWeight: 'bold',
    marginLeft: 'auto',
  },

  fechass: {
    marginLeft: 'auto',
    marginRight: 10,
    marginTop: 10,
  },

  list: {
    borderBottomWidth: 1,
    borderBottomColor: new DynamicValue(colors.rgb_235, colors.back_suave_dark),
    minHeight: dimensions.Height(10),
    height: 100,
    justifyContent: 'center'    
  }

});

const mapDispatchToProps = {
  getUsers: usersGet,
};

export default connect(
  null,
  mapDispatchToProps,
)(Chats);
