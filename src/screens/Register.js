import React, {useState} from 'react';
import {View, TouchableOpacity, Alert} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {connect} from 'react-redux';
import {colors, dimensions} from './../themes';
import {CustomText} from './../components/CustomText';
import {Button} from './../components/Button';
import {Mutation, withApollo} from 'react-apollo';
import Navigation from './../services/NavigationService';
import {NUEVO_PROFESIONAL} from './../mutations';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import Headers from '../components/Header';
import {OutlinedTextField} from 'react-native-material-textfield';
import Icon from 'react-native-dynamic-vector-icons';
import {ifIphoneX} from 'react-native-iphone-x-helper';

function Register() {
  const [nombre, setNombre] = useState('');
  const [apellidos, setApellidos] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  const [names, setNames] = useState('eye');

  const setmostrar = () => {
    if (secureTextEntry) {
      setSecureTextEntry(false);
      setNames('eye-off');
    } else {
      setSecureTextEntry(true);
      setNames('eye');
    }
  };

  const handleSubmit = async crearProfesional => {
    if ((!password, !email, !nombre, !apellidos)) {
      Alert.alert(
        'Error con el registro',
        'Todos los campos son obligatorio para el registro de usuario',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
      return null;
    }
    await crearProfesional({
      variables: {
        input: {
          nombre: nombre,
          apellidos: apellidos,
          email: email,
          password: password,
        },
      },
    }).then(async ({data: res}) => {
      if (res && res.crearProfesional && res.crearProfesional.success) {
        const dat =
          res && res.crearProfesional ? res.crearProfesional.data : '';
        let very = {
          id: dat.id,
        };
        Navigation.navigate('VerifyPhone', {data: very});
      } else if (res && res.crearProfesional && !res.crearProfesional.success)
        console.log(res.crearProfesional.message);

      Alert.alert(
        'Registro de Veterinario',
        res.crearProfesional.message,
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: false},
      );
    });
  };

  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View style={styles.container}>
    <Mutation mutation={NUEVO_PROFESIONAL}>
      {crearProfesional => {
        return (
          <View style={styles.container}>
            <View>
              <Headers
                navigation={Navigation}
                title="Crear cuenta"
                back={true}
                nologin={true}
              />
            </View>
            <View style={styles.contenedor}>
              <KeyboardAwareScrollView
                keyboardShouldPersistTaps="always"
                showsVerticalScrollIndicator={false}
                style={{marginBottom: dimensions.Height(10)}}>
                <View
                  style={{
                    marginTop: dimensions.Height(5),
                    marginLeft: dimensions.Width(8),
                    marginBottom: dimensions.Height(5),
                  }}>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.rgb_153}
                    style={styles.h1}>
                    Bienvenido de
                  </CustomText>
                  <CustomText
                    light={colors.back_dark}
                    dark={colors.rgb_153}
                    style={styles.h1}>
                    a Vetec PRO!
                  </CustomText>
                </View>
                <View style={styles.formView}>
                  <OutlinedTextField
                    label="Nombre"
                    placeholder="Nombre"
                    keyboardType="default"
                    inputContainerStyle={{
                      color: colors.rgb_153,
                      marginTop: 20,
                    }}
                    textColor={colors.rgb_153}
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setNombre(values)}
                  />
                  <OutlinedTextField
                    label="Apellidos"
                    placeholder="Apellidos"
                    keyboardType="default"
                    inputContainerStyle={{
                      color: colors.rgb_153,
                      marginTop: 20,
                    }}
                    textColor={colors.rgb_153}
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setApellidos(values)}
                  />
                  <OutlinedTextField
                    label="Email"
                    placeholder="Email"
                    keyboardType="default"
                    inputContainerStyle={{
                      color: colors.rgb_153,
                      marginTop: 20,
                    }}
                    textColor={colors.rgb_153}
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setEmail(values)}
                  />
                  <OutlinedTextField
                    label="Contraseña"
                    keyboardType="default"
                    secureTextEntry={secureTextEntry}
                    inputContainerStyle={{
                      color: colors.rgb_153,
                      marginTop: 20,
                    }}
                    textColor={colors.rgb_153}
                    labelTextStyle={{color: colors.rgb_153}}
                    placeholder="Contraseña"
                    placeholderTextColor={colors.rgb_153}
                    onChangeText={values => setPassword(values)}
                  />
                  <TouchableOpacity
                    onPress={() => setmostrar()}
                    style={{
                      alignSelf: 'flex-end',
                      marginTop: dimensions.Height(0),
                      marginBottom: dimensions.Height(0),
                    }}>
                    <CustomText light={colors.back_dark} dark={colors.rgb_102}>
                      <Icon
                        name={names}
                        type="MaterialCommunityIcons"
                        size={20}
                        color={colors.rgb_102}
                      />
                    </CustomText>
                  </TouchableOpacity>
                  <CustomText light={colors.back_dark} dark={colors.rgb_153}>
                    Al registrarme he leído y estoy de acuerdo con los Téminos y
                    condiciones
                  </CustomText>

                  <View style={styles.signupButtonContainer}>
                    <Button
                      light={colors.white}
                      dark={colors.white}
                      containerStyle={styles.buttonView}
                      onPress={() => handleSubmit(crearProfesional)}
                      title="Registrarme"
                      titleStyle={styles.buttonTitle}
                    />
                  </View>

                  <View
                    style={{
                      alignSelf: 'center',
                      marginTop: dimensions.Height(5),
                    }}>
                    <CustomText light={colors.back_dark} dark={colors.rgb_153}>
                      ¿Ya tienes una cuenta?
                    </CustomText>
                  </View>

                  <TouchableOpacity
                    onPress={() => Navigation.navigate('Login')}
                    style={{
                      alignSelf: 'center',
                      marginTop: dimensions.Height(5),
                      marginBottom: dimensions.Height(25),
                    }}>
                    <CustomText light={colors.main} dark={colors.main}>
                      Iniciar sesión
                    </CustomText>
                  </TouchableOpacity>
                </View>
              </KeyboardAwareScrollView>
            </View>
          </View>
        );
      }}
    </Mutation>
    </View>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: dimensions.Height(100),
  },
  formView: {
    marginHorizontal: dimensions.Width(8),
    marginTop: dimensions.Height(4),
  },
  rememberMeView: {
    marginTop: dimensions.Height(2),
    flexDirection: 'row',
  },
  rememberText: {
    fontSize: dimensions.FontSize(18),
  },
  signupButtonContainer: {
    marginTop: dimensions.Height(5),
    alignSelf: 'center',
  },
  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(84),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(5),
    color: colors.white,
    fontWeight: 'bold',
    fontSize: dimensions.FontSize(17),
  },
  contenedor: {
    width: dimensions.ScreenWidth,
    height: dimensions.ScreenHeight,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
  },
  h1: {
    fontSize: dimensions.FontSize(30),
    fontWeight: 'bold',
    textAlign: 'left',
  },
});

export default connect(
  null,
  {},
)(withApollo(Register));
