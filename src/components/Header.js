import React, {useEffect, useState} from 'react';
import {View, TouchableOpacity, Linking, ActivityIndicator, Dimensions} from 'react-native';
import {
  DynamicStyleSheet,
  useDynamicStyleSheet,
  DynamicValue,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {CustomText} from './CustomText';
import {Badge} from '@ant-design/react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {GET_NOTIFICATIONS, PROFESIONAL_DETAIL} from '../query';
import {Query} from 'react-apollo';
import {
  NETWORK_INTERFACE_LINK_AVATAR,
  NETWORK_INTERFACE_LINK,
} from './../constants/config';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {Avatar} from 'react-native-elements';

const pinkyGradient = ['#0BAB64', '#3BB78F'];

export default (Headers = props => {
  const [id, setId] = useState(null);
  const [Loading, setLoading] = useState(false);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
      console.log(id);
    };
    dataid();
  }, []);

  const Isoffline = async () => {
    fetch(`${NETWORK_INTERFACE_LINK}/prof-inactive?userid=${id}`).catch(err =>
      console.log(err),
    );
    Navigation.goBack(null);
  };

  const Isonline = async data => {
    setLoading(true);
    const lastine = new Date();
    fetch(
      `${NETWORK_INTERFACE_LINK}/prof-active?userid=${id}&lastTime=${lastine}`,
    )
      .then(res => {
        if (res) {
          console.log('>>>>>>>>>>>', res);
          setLoading(false);
        } else {
          return null;
        }
      })
      .catch(err => console.log(err));

    Navigation.navigate('Profile', {data: data});
    setLoading(false);
  };

  const handleClick = () => {
    Linking.canOpenURL('mailto:info@vetec.es').then(supported => {
      if (supported) {
        Linking.openURL('mailto:info@vetec.es');
        console.log('calling: ' + 'mailto:info@vetec.es');
      } else {
        console.log("Don't know how to open URI: " + 'mailto:info@vetec.es');
      }
    });
  };

  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      colors={pinkyGradient}
      style={styles.headers}>
      <View style={styles.hei}>
        {props.back ? (
          <View>
            {props.atrasprofile ? (
              <TouchableOpacity
                onPress={() => Isoffline()}
                style={{marginLeft: 10}}>
                <Icon
                  name="arrowleft"
                  type="AntDesign"
                  size={25}
                  color="white"
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => Navigation.goBack(null)}
                style={{marginLeft: 10}}>
                <Icon
                  name="arrowleft"
                  type="AntDesign"
                  size={25}
                  color="white"
                />
              </TouchableOpacity>
            )}
          </View>
        ) : (
          <View>
            {id ? (
              <Query query={PROFESIONAL_DETAIL} variables={{id: id}}>
                {({loading, error, data, refetch}) => {
                  refetch();
                  if (error) {
                    console.log('Response Error-------', error);
                    return <ActivityIndicator style={{marginLeft: 10}} />;
                  }
                  if (loading) {
                    return <ActivityIndicator style={{marginLeft: 10}} />;
                  }
                  if (data) {
                    const userDatas = data.getProfesional;
                    return (
                      <View>
                        {Loading ? (
                          <ActivityIndicator
                            style={{marginLeft: 15}}
                            size="large"
                          />
                        ) : (
                          <TouchableOpacity
                            onPress={() => Isonline(userDatas.id)}
                            style={{marginLeft: 10}}>
                            <Avatar
                              rounded
                              size={35}
                              source={{
                                uri:
                                  NETWORK_INTERFACE_LINK_AVATAR +
                                  userDatas.avatar,
                              }}
                              containerStyle={{borderWidth: 2,
                                borderColor: colors.white,
                                ...ifIphoneX(
                                  {
                                    marginTop: 0
                                  },
                                  {
                                    marginTop: -5
                                  },
                                )}}
                            />
                          </TouchableOpacity>
                        )}
                      </View>
                    );
                  }
                }}
              </Query>
            ) : (
              <TouchableOpacity
                onPress={() => handleClick()}
                style={{marginLeft: 10}}>
                <Icon name="user" type="AntDesign" size={30} color="white" />
              </TouchableOpacity>
            )}
          </View>
        )}
        <CustomText
          light={colors.white}
          dark={colors.white}
          style={styles.title}>
          {props.title}
        </CustomText>

        {props.nologin ? (
          <TouchableOpacity
            onPress={() => handleClick()}
            style={{
              marginLeft: 'auto',
              marginRight: 10,
            }}>
            <Icon
              name="customerservice"
              type="AntDesign"
              size={30}
              color="white"
            />
          </TouchableOpacity>
        ) : (
          <Query query={GET_NOTIFICATIONS} variables={{Id: id}}>
            {(data, error, loading, refetch) => {
              refetch = refetch;
              if (loading) {
                return <ActivityIndicator />;
              }
              if (error) {
                return console.log('Response Error-------', error);
              }
              if (data) {
                const total =
                  data && data.data && data.data.getNotifications
                    ? data.data.getNotifications.notifications
                    : '';
                return (
                  <TouchableOpacity
                    onPress={() =>
                      Navigation.navigate('Notifications', {
                        data: id,
                      })
                    }
                    style={{
                      marginLeft: 'auto',
                      marginRight: total.length ? 25 : 10,
                    }}>
                    <Badge text={total.length} overflowCount={9}>
                      <Icon
                        name="bells"
                        type="AntDesign"
                        size={30}
                        color="white"
                      />
                    </Badge>
                  </TouchableOpacity>
                );
              }
            }}
          </Query>
        )}
      </View>
      <View style={styles.contenedor} />
    </LinearGradient>
  );
});

const height = Dimensions.get('window').height

const dynamicStyles = new DynamicStyleSheet({
  headers: {
    width: dimensions.ScreenWidth,
    ...ifIphoneX(
      {
        height: dimensions.Height(15),
      },
      {
        height: height < 600 ? dimensions.Height(25) : dimensions.Height(18),
      },
    ),
  },
  
  hei: {
    flexDirection: 'row',
    ...ifIphoneX(
      {
        marginTop: 50,
      },
      {
        marginTop: 50,
      },
    ),
  },

  contenedor: {
    position: 'absolute',
    width: dimensions.ScreenWidth,
    height: dimensions.Height(5),
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    ...ifIphoneX(
      {
        marginTop: dimensions.Height(12),
      },
      {
        marginTop: height < 600 ? dimensions.Height(20) : dimensions.Height(14),
      },
    ),
    borderTopEndRadius: 15,
    borderTopStartRadius: 15,
  },

  title: {
    alignSelf: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    fontSize: dimensions.FontSize(18),
    fontWeight: '500',
  },
});
