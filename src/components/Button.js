import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {CustomText} from './CustomText';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import LinearGradient from 'react-native-linear-gradient';

const pinkyGradient = ['#0BAB64', '#3BB78F'];

export const Button = props => {
  const dynamicStyles = new DynamicStyleSheet({
    colord: {
      color: new DynamicValue(props.dark, props.light),
    },
  });
  const styles = useDynamicStyleSheet(dynamicStyles);
  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      colors={pinkyGradient}
      style={props.containerStyle}>
      <TouchableOpacity onPress={() => props.onPress()}>
        <CustomText style={[props.titleStyle, styles.colord]}>
          {props.title}
        </CustomText>
      </TouchableOpacity>
    </LinearGradient>
  );
};
