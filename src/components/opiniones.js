import React from 'react';
import {View, FlatList, TouchableOpacity, Image} from 'react-native';
import Professional from '../constants/data';
import {DynamicStyleSheet, useDynamicStyleSheet} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import {CustomText} from './CustomText';

export default function Opiniones() {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const _renderItem = ({item}) => {
    return (
      <TouchableOpacity style={styles.card}>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: dimensions.Width(2),
          }}>
          <View style={styles.avatar}>
            <Image source={{uri: item.avatar}} style={styles.avatar} />
          </View>
          <View style={{marginLeft: dimensions.Width(4)}}>
            <CustomText
              numberOfLines={1}
              light={colors.rgb_102}
              dark={colors.light_white}
              style={styles.title}>
              {item.nombre} {item.apillidos}
            </CustomText>
            <View style={{flexDirection: 'row'}}>
              <CustomText style={[styles.text, {color: colors.main1}]}>
                Hace un mes
              </CustomText>
              <CustomText style={[styles.text, {marginLeft: 'auto'}]}>
                <Icon
                  name="staro"
                  type="AntDesign"
                  size={14}
                  color={colors.orange}
                />{' '}
                ({item.rating})
              </CustomText>
            </View>
            <CustomText style={styles.text}>
              <Icon
                name="enviromento"
                type="AntDesign"
                size={14}
                color={colors.rgb_102}
              />{' '}
              {item.city}
            </CustomText>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: dimensions.Width(2),
          }}>
          <CustomText
            numberOfLines={5}
            light={colors.rgb_153}
            dark={colors.rgb_153}
            style={styles.des}>
            {item.descripcion}
          </CustomText>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <FlatList
      data={Professional}
      renderItem={item => _renderItem(item)}
      keyExtractor={item => item.id}
      showsHorizontalScrollIndicator={false}
      horizontal={true}
      ListEmptyComponent={
        <View style={{alignSelf: 'center', padding: 20}}>
          {/*<Image source={image.Vacia} style={{width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13)}} />*/}
          <CustomText
            light={colors.blue_main}
            dark={colors.light_white}
            style={{
              textAlign: 'center',
              fontSize: dimensions.FontSize(20),
              fontWeight: '200',
            }}>
            Aún no tienes ordenes para procesar
          </CustomText>
        </View>
      }
    />
  );
}

const dynamicStyles = new DynamicStyleSheet({
  card: {
    width: dimensions.Width(92),
    height: dimensions.Height(25),
    marginHorizontal: dimensions.Width(4),
    marginTop: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.Height(2),
  },

  avatar: {
    width: 70,
    height: 70,
    borderRadius: 13,
  },

  text: {
    color: colors.rgb_102,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
    marginTop: dimensions.Height(1),
  },

  textt: {
    color: colors.rgb_102,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
    marginTop: dimensions.Height(1.5),
  },
  title: {
    fontSize: dimensions.FontSize(16),
    fontWeight: '400',
    width: dimensions.Height(27),
  },
  des: {
    fontSize: dimensions.FontSize(16),
    fontWeight: '300',
    marginTop: dimensions.Height(1),
  },
});
