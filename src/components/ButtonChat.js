import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {CustomText} from './CustomText';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import LinearGradient from 'react-native-linear-gradient';
import {colors} from '../themes';

const pinkyGradient = [colors.twitter_color, colors.twitter_color];

export const ButtonChat = props => {
  const dynamicStyles = new DynamicStyleSheet({
    colord: {
      color: new DynamicValue(props.dark, props.light),
    },
  });
  const styles = useDynamicStyleSheet(dynamicStyles);
  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      colors={pinkyGradient}
      style={props.containerStyle}>
      <TouchableOpacity onPress={() => props.onPress()}>
        <CustomText style={[props.titleStyle, styles.colord]}>
          {props.title}
        </CustomText>
      </TouchableOpacity>
    </LinearGradient>
  );
};
