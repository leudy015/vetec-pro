import React from 'react';
import {View, FlatList, TouchableOpacity} from 'react-native';
import Professional from '../constants/data';
import {DynamicStyleSheet, useDynamicStyleSheet} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import LinearGradient from 'react-native-linear-gradient';
import {CustomText} from './CustomText';
import {Avatar, Badge} from 'react-native-elements';

const pinkyGradient = ['#0BAB64', '#3BB78F'];

export default function OnlineDoctor() {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const _renderItem = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => {}}
        style={{marginLeft: dimensions.Height(2)}}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={pinkyGradient}
          style={styles.avatar}>
          <Avatar
            rounded
            size="medium"
            source={{uri: item.avatar}}
            containerStyle={{alignSelf: 'center', marginTop: 3}}
          />
          <Badge
            status="success"
            containerStyle={{marginLeft: 28, marginTop: -10}}
          />
        </LinearGradient>
      </TouchableOpacity>
    );
  };
  return (
    <FlatList
      data={Professional}
      renderItem={item => _renderItem(item)}
      keyExtractor={item => item.id}
      horizontal={true}
      showsHorizontalScrollIndicator={false}
      ListEmptyComponent={
        <View style={{alignSelf: 'center', padding: 20}}>
          {/*<Image source={image.Vacia} style={{width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13)}} />*/}
          <CustomText
            light={colors.blue_main}
            dark={colors.light_white}
            style={{
              textAlign: 'center',
              fontSize: dimensions.FontSize(20),
              fontWeight: '200',
            }}>
            Aún no tienes ordenes para procesar
          </CustomText>
        </View>
      }
    />
  );
}

const dynamicStyles = new DynamicStyleSheet({
  avatar: {
    width: 56,
    height: 56,
    borderRadius: 43,
  },
});
