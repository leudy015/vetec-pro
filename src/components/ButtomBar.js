import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity, SafeAreaView} from 'react-native';
import Navigation from './../services/NavigationService';
import BottomBar from 'react-native-bottom-bar';
import Icon from 'react-native-dynamic-vector-icons';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
  useDarkModeContext,
} from 'react-native-dark-mode';
import {colors} from '../themes/colors';
import AsyncStorage from '@react-native-community/async-storage';
const backgroundColors = {
  light: colors.white,
  dark: colors.back_dark,
};

const logout = async () => {
  await AsyncStorage.removeItem('token');
  await AsyncStorage.removeItem('id');
  Navigation.navigate('Landing');
};

export const ButtomBar = props => {
  const [id, setId] = useState(null);

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
      console.log(id);
    };
    dataid();
  }, []);

  const styles = useDynamicStyleSheet(dynamicStyles);
  const mainColor = '#373f4c';
  const pinkyGradient = ['#0BAB64', '#3BB78F'];
  const mode = useDarkModeContext();
  const backgroundColor = backgroundColors[mode];

  const NaviG = () => {
    if (id) {
      Navigation.navigate('Profile', {data: id});
    } else {
      Navigation.navigate('Landing');
    }
  };

  const NaviM = () => {
    if (id) {
      Navigation.navigate('Chats', {data: id});
    } else {
      Navigation.navigate('Landing');
    }
  };

  const renderMainIcon = () => {
    return (
      <TouchableOpacity onPress={() => NaviM()}>
        <Icon name="message1" type="AntDesign" size={40} color="white" />
      </TouchableOpacity>
    );
  };

  const renderFirstIconComponent = () => {
    return (
      <View
        style={{
          ...Platform.select({
            ios: {
              right: 16,
              top: 8,
            },
            android: {
              right: 8,
              top: 10,
            },
          }),
        }}>
        <TouchableOpacity onPress={() => Navigation.navigate('Home')}>
          <Icon
            name="stethoscope"
            type="MaterialCommunityIcons"
            size={30}
            style={styles.colorDark}
          />
        </TouchableOpacity>
      </View>
    );
  };

  const renderThirdIconComponent = () => {
    return (
      <View
        style={{
          ...Platform.select({
            ios: {
              left: 24,
              bottom: 3,
              top: 5,
            },
            android: {
              top: 5,
              left: 3,
            },
          }),
        }}>
        <TouchableOpacity onPress={() => NaviG()}>
          <Icon
            name="user"
            type="AntDesign"
            size={30}
            style={styles.colorDark}
          />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <BottomBar
          shapeColor={backgroundColor}
          mainIcon={renderMainIcon()}
          mainIconColor={mainColor}
          mainIconGradient={pinkyGradient}
          mainIconComponent={''}
          miniButtonsColor={mainColor}
          firstIconComponent={renderFirstIconComponent()}
          disableSecondIcon={true}
          thirdIconComponent={renderThirdIconComponent()}
          disableFourthIcon={true}
        />
      </View>
    </SafeAreaView>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
  },

  colorDark: {
    color: new DynamicValue(colors.back_dark, colors.white),
  },
});
