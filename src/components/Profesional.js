import React from 'react';
import {View, FlatList, TouchableOpacity, Image, Linking} from 'react-native';
import Professional from '../constants/data';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {CustomText} from './CustomText';
import {Button} from './Button';

export default function Profesional() {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const _renderItem = ({item}) => {
    const handleClick = () => {
      Linking.canOpenURL(`tel:${item.telefono}`).then(supported => {
        if (supported) {
          Linking.openURL(`tel:${item.telefono}`);
          console.log('calling: ' + item.telefono);
        } else {
          console.log("Don't know how to open URI: " + item.telefono);
        }
      });
    };
    return (
      <TouchableOpacity
        onPress={() => Navigation.navigate('DetailsPro', {data: item})}
        style={styles.card}>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: dimensions.Width(2),
          }}>
          <View style={styles.avatar}>
            <Image source={{uri: item.avatar}} style={styles.avatar} />
          </View>
          <View style={{marginLeft: dimensions.Width(4)}}>
            <CustomText
              numberOfLines={1}
              light={colors.rgb_102}
              dark={colors.light_white}
              style={styles.title}>
              {item.nombre} {item.apillidos}
            </CustomText>
            <View style={{flexDirection: 'row'}}>
              <CustomText style={[styles.text, {color: colors.main1}]}>
                {item.profesion}
              </CustomText>
              <CustomText style={[styles.text, {marginLeft: 'auto'}]}>
                <Icon
                  name="staro"
                  type="AntDesign"
                  size={14}
                  color={colors.orange}
                />{' '}
                ({item.rating})
              </CustomText>
            </View>
            <CustomText style={styles.text}>
              <Icon
                name="enviromento"
                type="AntDesign"
                size={14}
                color={colors.rgb_102}
              />{' '}
              {item.city}
            </CustomText>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: dimensions.Width(2),
          }}>
          <TouchableOpacity onPress={() => handleClick()}>
            <CustomText style={styles.textt}>
              <Icon
                name="phone"
                type="AntDesign"
                size={14}
                color={colors.main1}
              />{' '}
              {item.telefono}
            </CustomText>
          </TouchableOpacity>
          <View style={{marginLeft: 'auto'}}>
            <View style={styles.signupButtonContainer}>
              <Button
                light={colors.white}
                dark={colors.white}
                containerStyle={styles.buttonView}
                onPress={() => {}}
                title="Chat"
                titleStyle={styles.buttonTitle}
              />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <FlatList
      data={Professional}
      renderItem={item => _renderItem(item)}
      keyExtractor={item => item.id}
      showsVerticalScrollIndicator={false}
      ListEmptyComponent={
        <View style={{alignSelf: 'center', padding: 20}}>
          {/*<Image source={image.Vacia} style={{width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13)}} />*/}
          <CustomText
            light={colors.blue_main}
            dark={colors.light_white}
            style={{
              textAlign: 'center',
              fontSize: dimensions.FontSize(20),
              fontWeight: '200',
            }}>
            Aún no tienes ordenes para procesar
          </CustomText>
        </View>
      }
    />
  );
}

const dynamicStyles = new DynamicStyleSheet({
  card: {
    width: dimensions.Width(92),
    height: dimensions.Height(17),
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginHorizontal: dimensions.Width(4),
    marginTop: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.Height(2),
    borderRadius: 15,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  avatar: {
    width: 70,
    height: 70,
    borderRadius: 13,
  },

  text: {
    color: colors.rgb_102,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
    marginTop: dimensions.Height(1),
  },

  textt: {
    color: colors.rgb_102,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
    marginTop: dimensions.Height(1.5),
  },
  title: {
    fontSize: dimensions.FontSize(16),
    fontWeight: '400',
    width: dimensions.Height(27),
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(30),
    borderRadius: dimensions.Width(8),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
  },
});
