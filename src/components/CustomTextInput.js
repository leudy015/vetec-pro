import React from 'react';
import {TextInput, View, Image, TouchableOpacity} from 'react-native';
import {dimensions, colors} from './../themes';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';

export const CustomTextInput = props => {
  const styles = useDynamicStyleSheet(dynamicStyles);
  return (
    <View style={[styles.content, props.containerStyle]}>
      {props.right ? (
        <TouchableOpacity
          style={{justifyContent: 'center', alignItems: 'center'}}
          onPress={() => props.rightIconPress()}>
          <Image
            style={{alignSelf: 'center'}}
            resizeMode="contain"
            source={props.right}
          />
        </TouchableOpacity>
      ) : null}
      <TextInput
        {...props}
        style={[
          props.textInputStyle,
          styles.textInputStyle,
          props.left ? {width: '85%'} : {},
        ]}
      />
      {props.left ? (
        <TouchableOpacity
          style={{justifyContent: 'center', alignItems: 'center'}}
          onPress={() => props.onPress()}>
          <Image
            style={{alignSelf: 'center'}}
            resizeMode="contain"
            source={props.left}
          />
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  textInputStyle: {
    paddingVertical: 0,
    height: dimensions.Height(7),
    borderColor: colors.grey,
    width: '100%',
    paddingHorizontal: dimensions.Width(4),
  },
  hashTrix: {
    color: colors.red,
    marginVertical: dimensions.Height(1),
  },
  text: {
    color: new DynamicValue(colors.white, colors.black),
    fontSize: dimensions.FontSize(14),
    justifyContent: 'center',
    textAlignVertical: 'center',
    alignContent: 'center',
    marginVertical: dimensions.Height(2),
  },
  content: {
    flexDirection: 'row',
    flex: 1,
    borderRadius: dimensions.Width(2),
    backgroundColor: new DynamicValue(
      colors.slight_grey,
      colors.back_suave_dark,
    ),
  },
});
