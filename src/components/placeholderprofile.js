import React from 'react';
import {SafeAreaView, View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {useDynamicValue} from 'react-native-dark-mode';
import {colors, dimensions} from './../themes';

export default function Loading() {
  const placeholderColor = useDynamicValue('#E1E9EE', colors.back_dark);
  const placeholderColor1 = useDynamicValue('#F2F8FC', '#303030');
  return (
    <SafeAreaView style={{alignSelf: 'center'}}>
      <SkeletonPlaceholder
        backgroundColor={placeholderColor}
        highlightColor={placeholderColor1}>
        <View
          style={{
            width: dimensions.Width(100),
            marginBottom: 40,
            marginTop: 50,
            alignItems: 'center',
          }}>
          <View style={{width: 150, height: 150, borderRadius: 100}} />
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View>
              <View
                style={{
                  width: dimensions.Width(65),
                  height: 15,
                  borderRadius: 3,
                  marginTop: 15,
                }}
              />
              <View
                style={{
                  width: dimensions.Width(45),
                  height: 15,
                  borderRadius: 3,
                  marginTop: 15,
                }}
              />
            </View>
          </View>
          <View style={{marginTop: 70}}>
            <View
              style={{
                width: dimensions.Width(95),
                height: 90,
                borderRadius: 15,
                marginTop: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(95),
                height: 90,
                borderRadius: 15,
                marginTop: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(95),
                height: 90,
                borderRadius: 15,
                marginTop: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(95),
                height: 90,
                borderRadius: 15,
                marginTop: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(95),
                height: 90,
                borderRadius: 15,
                marginTop: 15,
              }}
            />
            <View
              style={{
                width: dimensions.Width(95),
                height: 90,
                borderRadius: 15,
                marginTop: 15,
              }}
            />
          </View>
        </View>
      </SkeletonPlaceholder>
    </SafeAreaView>
  );
}
