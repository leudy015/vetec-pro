import React from 'react';
import {Text} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';

export const CustomText = props => {
  const dynamicStyles = new DynamicStyleSheet({
    TextView: {
      color: new DynamicValue(props.light, props.dark),
      height: null,
      width:  null
    },
  });
  const styles = useDynamicStyleSheet(dynamicStyles);
  return (
    <Text {...props} style={[styles.TextView, props.style]}>
      {props.children}
    </Text>
  );
};
