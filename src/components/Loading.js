import React, {useState, useEffect} from 'react';
import {ActivityIndicator, View, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import {
  NETWORK_INTERFACE_LINK_AVATAR
} from '../constants/config';
import {Avatar} from 'react-native-elements';
import {CustomText} from './CustomText';
import {stylesText} from './StyleText';
import Navigationservice from './../services/NavigationService';
import AsyncStorage from '@react-native-community/async-storage';

const Loading = (props, {message}) => {

  const [id, setId] = useState(null)

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  const styles = useDynamicStyleSheet(dynamicStyles);
  const datos = props.data;

  console.log(datos)
  return(
    <View style={styles.container}>
      <View style={{marginTop: dimensions.Height(25), alignItems: 'center',}}>
            <Avatar
              rounded
              size={120}
              source={{
                uri: NETWORK_INTERFACE_LINK_AVATAR + datos.avatar
              }}
              containerStyle={styles.avatar}
            />

              <View style={{flexDirection: 'row'}}>
                <CustomText
                  numberOfLines={1}
                  light={colors.back_dark}
                  dark={colors.white}
                  style={[stylesText.mainText, {textAlign: 'center', marginTop: 15}]}>
                  {datos.nombre} {datos.apellidos}
                </CustomText>
              </View>
        </View>
        <View>
          <View style={{flexDirection: 'row', alignSelf: 'center', marginTop: 15}}>
          <CustomText
            numberOfLines={1}
            light={colors.back_dark}
            dark={colors.white}
            style={[stylesText.mainText, {textAlign: 'center', paddingHorizontal: 15}]}>
                Conectando... 
          </CustomText>
          <ActivityIndicator color={colors.main} size="small" />
          </View>
          <View
            style={{
              marginTop: 50,
              marginBottom: 15,
              alignItems: 'center',
              paddingHorizontal: 30
            }}>
          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={[stylesText.mainText, {textAlign: 'center', paddingHorizontal: 30}]}>
                Si tarda mucho en conectar no te preocupes solo vuelve a intentarlo.
          </CustomText>
          <View style={{marginTop: dimensions.Height(2)}}>
            <TouchableOpacity style={styles.btncont} onPress={()=> Navigationservice.navigate('Chats', {data: id})}>
              <CustomText
                light={colors.main}
                dark={colors.white}
                style={styles.te}>
                Volver a intentarlo
              </CustomText>
            </TouchableOpacity>
          </View>
          </View>
        </View>
    {message ? (
      <CustomText style={{color: colors.rgb_153, textAlign: 'center'}}>{message}</CustomText>
    ) : null}
  </View>
  )
}

Loading.navigationOptions = {title: 'Loading'};

const dynamicStyles = new DynamicStyleSheet({
    container: {
      alignItems: 'center',
      backgroundColor: new DynamicValue(colors.white, colors.back_dark),
      flex: 1,
      width: '100%',
    },
    btncont: {
      width: dimensions.Width(85),
      padding: dimensions.Height(2),
      backgroundColor: new DynamicValue(colors.white, colors.main),
      borderWidth: 1,
      borderColor: colors.main,
      margin: dimensions.Width(4),
      borderRadius: dimensions.Width(10),
    },
  
    te: {
      textAlign: 'center',
      fontSize: dimensions.FontSize(18),
    },

})

export default Loading;
