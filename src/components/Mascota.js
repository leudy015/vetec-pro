import React from 'react';
import {View, FlatList, TouchableOpacity} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {CustomText} from './CustomText';
import {Avatar} from 'react-native-elements';
import {GET_MASCOTAS} from '../query';
import {ActivityIndicator} from '@ant-design/react-native';
import {Query} from 'react-apollo';

export default function Mascota() {
  const styles = useDynamicStyleSheet(dynamicStyles);
  const _renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={styles.cardinfo}
        onPress={() => Navigation.navigate('DetailsMascota', {data: item})}>
        <View style={{alignSelf: 'center', marginLeft: dimensions.Width(4)}}>
          <Avatar
            rounded
            size={50}
            source={{uri: item.avatar}}
            containerStyle={styles.avatar}
          />
        </View>
        <View style={{alignSelf: 'center', marginLeft: dimensions.Width(4)}}>
          <CustomText
            light={colors.back_dark}
            dark={colors.white}
            style={styles.name1}>
            {item.name}
          </CustomText>
          <CustomText light={colors.back_dark} dark={colors.rgb_153}>
            {item.age}
          </CustomText>
        </View>
        <View
          style={{
            alignSelf: 'center',
            marginLeft: 'auto',
            marginRight: dimensions.Width(4),
          }}>
          <Icon
            name="right"
            type="AntDesign"
            size={20}
            color={colors.rgb_153}
          />
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <Query query={GET_MASCOTAS}>
      {({loading, error, data}) => {
        if (loading) {
          return <ActivityIndicator />;
        }
        if (error) {
          return console.log('error in mascota', error);
        }
        if (data) {
          return (
            <FlatList
              data={data && data.getMascota ? data.getMascota.list : ''}
              renderItem={item => _renderItem(item)}
              keyExtractor={item => item.id}
              showsVerticalScrollIndicator={false}
              ListEmptyComponent={
                <View style={{alignSelf: 'center', padding: 20}}>
                  {/*<Image source={image.Vacia} style={{width: dimensions.Width(34), alignSelf: 'center', height: dimensions.Height(13)}} />*/}
                  <CustomText
                    light={colors.blue_main}
                    dark={colors.light_white}
                    style={{
                      textAlign: 'center',
                      fontSize: dimensions.FontSize(20),
                      fontWeight: '200',
                    }}>
                    Aún no tienes ordenes para procesar
                  </CustomText>
                </View>
              }
            />
          );
        } else {
          return <ActivityIndicator />;
        }
      }}
    </Query>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  cardinfo: {
    width: dimensions.Width(95),
    height: dimensions.Height(8),
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginLeft: dimensions.Width(2.5),
    marginTop: dimensions.Height(2),
    borderRadius: 10,
    flexDirection: 'row',
  },
  name1: {
    fontSize: dimensions.FontSize(24),
  },

  avatar: {
    borderColor: new DynamicValue(colors.light_white, colors.back_suave_dark),
    borderWidth: 2,
  },
});
