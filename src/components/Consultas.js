import React, {useEffect, useState} from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Alert,
} from 'react-native';
import {
  DynamicStyleSheet,
  DynamicValue,
  useDynamicStyleSheet,
} from 'react-native-dark-mode';
import {dimensions, colors} from '../themes';
import Icon from 'react-native-dynamic-vector-icons';
import Navigation from './../services/NavigationService';
import {CustomText} from './CustomText';
import {Button} from './Button';
import {ButtonEspecial} from './Buttonespecial';
import {ButtonChat} from './ButtonChat';
import {Query, Mutation} from 'react-apollo';
import {CONSULTA_BY_PROFESIONAL} from '../query/index';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
import {
  NETWORK_INTERFACE_LINK,
  NETWORK_INTERFACE_LINK_AVATAR,
} from '../constants/config';
import {
  PROFESSIONAL_CONSULTA_PROCEED,
  CREATE_NOTIFICATION,
} from './../mutations';
import {useMutation} from '@apollo/react-hooks';
import NoData from './NoData';
import moment from 'moment';
import 'moment/locale/es';
import io from 'socket.io-client';
import {usersGet} from '../actionCreators';
import {connect} from 'react-redux';
import * as Progress from 'react-native-progress';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import PlaceHolder from './../components/placeholder';

function Consulta() {
  const [id, setId] = useState(null);
  const [messageTexte] = useState(
    'Vetec App, el profesional de Vetec ha aceptado la consulta contratada ve a tu perfil para iniciarla',
  );
  const [messageRec] = useState(
    'Vetec App, el profesional ha rechazado la consulta lamentamos los incovenientes.',
  );
  const [messgeFinish] = useState(
    'Vetec App, La consulta de Vetec ha sido completada es hora de valorarla',
  );

  const [createNotification] = useMutation(CREATE_NOTIFICATION);

  const socket = io(NETWORK_INTERFACE_LINK, {
    forceNew: true,
  });

  useEffect(() => {
    const dataid = async () => {
      const id = await AsyncStorage.getItem('id');
      setId(id);
    };
    dataid();
  }, []);

  const styles = useDynamicStyleSheet(dynamicStyles);
  const _renderItem = (item, refetch) => {

    refetch();

    const Navigationachat = () => {
      refetch()
      socket.emit('available', {
        user: item.usuario.id,
        prof: item.profesional.id,
      });
      Navigation.navigate('ChatsScreens', {data: item});
    };
    socket.on('connect', () => {
      socket.emit('online_user', {
        usuario: item.usuario.id,
        profesional: item.profesional.id,
        id: item.profesional.id,
      });
    });

    const Aceptarconsulta = (consultaProceed, data) => {
      const formData = {
        consultaID: item.id,
        estado: 'Aceptado',
        progreso: '75',
        status: 'active',
      };
      console.log('esto es lo qie se esya enviadop', formData);
      consultaProceed({variables: formData})
        .then(async res => {
          if (
            res &&
            res.data &&
            res.data.consultaProceed &&
            res.data.consultaProceed.success
          ) {
            Toast.showWithGravity(
              'Consulta aceptada éxitosamente',
              Toast.LONG,
              Toast.TOP,
            );
            refetch();
            console.log('Has acep el profesional éxitosamente');
            const NotificationInput = {
              consulta: item.id,
              user: item.usuario.id,
              usuario: item.usuario.id,
              profesional: item.profesional.id,
              type: 'accept_order',
            };
            createNotification({variables: {input: NotificationInput}})
              .then(async results => {
                console.log('results', results);
              })
              .catch(err => {
                console.log('err', err);
              });

            const SendPushNotificationvalorar = () => {
              fetch(
                `${NETWORK_INTERFACE_LINK}/send-push-notification?IdOnesignal=${
                  item.usuario.UserID
                }&textmessage=${messageTexte}`,
              ).catch(err => console.log(err));
              console.log(item.usuario.UserID, messageTexte);
            };

            SendPushNotificationvalorar();
          }
        })
        .catch(err => {
          console.log(
            'Algo salió mal. Por favor intente nuevamente en un momento.',
            err,
          );
        });
    };

    const handlefinalizar = (consultaProceed, data) => {
      const formData = {
        consultaID: item.id,
        estado: 'Completada',
        progreso: '100',
        status: 'success',
      };
      console.log('esto es lo qie se esya enviadop', formData);
      consultaProceed({variables: formData})
        .then(async res => {
          if (
            res &&
            res.data &&
            res.data.consultaProceed &&
            res.data.consultaProceed.success
          ) {
            Toast.showWithGravity(
              'Consulta finalizada éxitosamente',
              Toast.LONG,
              Toast.TOP,
            );
            refetch();
            console.log('Has acep el profesional éxitosamente');
            const NotificationInput = {
              consulta: item.id,
              user: item.usuario.id,
              usuario: item.usuario.id,
              profesional: item.profesional.id,
              type: 'finish_order',
            };
            createNotification({variables: {input: NotificationInput}})
              .then(async results => {
                console.log('results', results);
              })
              .catch(err => {
                console.log('err', err);
              });
            const SendPushNotificationvalorar = () => {
              fetch(
                `${NETWORK_INTERFACE_LINK}/send-push-notification?IdOnesignal=${
                  item.usuario.UserID
                }&textmessage=${messgeFinish}`,
              ).catch(err => console.log(err));
            };

            SendPushNotificationvalorar();

            socket.emit('not_available', {
              user: item.usuario.id,
              prof: item.profesional.id,
            });
          }
        })
        .catch(err => {
          console.log(
            'Algo salió mal. Por favor intente nuevamente en un momento.',
            err,
          );
        });
    };

    const Rechazarconsulta = (consultaProceed, data) => {
      const formData = {
        consultaID: item.id,
        estado: 'Rechazado',
        progreso: '0',
        status: 'exception',
      };
      console.log('esto es lo qie se esya enviadop', formData);
      consultaProceed({variables: formData})
        .then(async res => {
          if (
            res &&
            res.data &&
            res.data.consultaProceed &&
            res.data.consultaProceed.success
          ) {
            Toast.showWithGravity(
              'Consulta rechazada éxitosamente',
              Toast.LONG,
              Toast.TOP,
            );
            refetch();
            console.log('Has aceptado el profesional éxitosamente');
            const NotificationInput = {
              consulta: item.id,
              user: item.usuario.id,
              usuario: item.usuario.id,
              profesional: item.profesional.id,
              type: 'reject_order',
            };
            createNotification({variables: {input: NotificationInput}})
              .then(async results => {
                console.log('results', results);
              })
              .catch(err => {
                console.log('err', err);
              });

            const SendPushNotificationvalorar = () => {
              fetch(
                `${NETWORK_INTERFACE_LINK}/send-push-notification?IdOnesignal=${
                  item.usuario.UserID
                }&textmessage=${messageRec}`,
              ).catch(err => console.log(err));
              console.log(item.usuario.UserID, messageRec);
            };

            SendPushNotificationvalorar();
          }
        })
        .catch(err => {
          console.log(
            'Algo salió mal. Por favor intente nuevamente en un momento.',
            err,
          );
        });
    };

    return (
      <TouchableOpacity
        onPress={() => Navigation.navigate('DetailsConsulta', {data: item})}
        style={styles.card}>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: dimensions.Width(2),
          }}>
          <View style={styles.avatar}>
            <Image
              source={{
                uri: NETWORK_INTERFACE_LINK_AVATAR + item.usuario.avatar,
              }}
              style={styles.avatar}
            />
          </View>
          <View style={{marginLeft: dimensions.Width(2)}}>
            <View style={styles.ol}>
              <CustomText
                numberOfLines={1}
                light={colors.rgb_102}
                dark={colors.light_white}
                style={styles.title}>
                {item.usuario.nombre} {item.usuario.apellidos}
              </CustomText>
              <CustomText
                numberOfLines={1}
                style={{
                  color: colors.main1,
                  marginLeft: 'auto',
                  marginRight: 5,
                  width: dimensions.Width(20),
                  textAlign: 'right',
                }}>
                {item.estado}
              </CustomText>
            </View>
            <View style={{flexDirection: 'row'}}>
              <CustomText style={styles.textt}>
                <Icon
                  name="dog"
                  type="MaterialCommunityIcons"
                  size={14}
                  color={colors.main1}
                />{' '}
                {item.mascota.name}
              </CustomText>
              <CustomText
                style={[styles.textt, {marginLeft: 'auto', marginRight: 10}]}>
                {moment(Number(item.created_at)).format('LL')}
              </CustomText>
            </View>
            <View style={{flexDirection: 'row'}}>
              <CustomText style={styles.text}>
                <Icon
                  name="enviromento"
                  type="AntDesign"
                  size={14}
                  color={colors.rgb_102}
                />{' '}
                {item.usuario.ciudad}
              </CustomText>
              {item.estado !== 'Valorada' ? (
                <View
                  style={{
                    marginLeft: 'auto',
                    marginTop: 10,
                    marginRight: 10,
                    marginBottom: 15,
                  }}>
                  <Progress.Circle
                    size={20}
                    indeterminate={true}
                    color={colors.main}
                  />
                </View>
              ) : (
                <View
                  style={{
                    marginLeft: 'auto',
                    marginTop: 10,
                    marginRight: 10,
                    marginBottom: 15,
                  }}>
                  <Progress.Circle
                    size={20}
                    indeterminate={false}
                    color={colors.main}
                  />
                </View>
              )}
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: dimensions.Width(2),
          }}>
          {item.estado === 'Nueva' ? (
            <View style={{}}>
              <Mutation mutation={PROFESSIONAL_CONSULTA_PROCEED}>
                {consultaProceed => (
                  <View style={styles.signupButtonContainer}>
                    <ButtonEspecial
                      light={colors.white}
                      dark={colors.white}
                      containerStyle={styles.buttonView}
                      onPress={() =>
                        Alert.alert(
                          '¿Estás seguro que deseas rechazar la consulta?',
                          'Lamentamos que no puedas cumplir con el servicio',
                          [
                            {
                              text: 'Rechazarla',
                              onPress: () => Rechazarconsulta(consultaProceed),
                            },
                            {
                              text: 'Aceptarla',
                              onPress: () => Aceptarconsulta(consultaProceed),
                            },
                          ],

                          {cancelable: false},
                        )
                      }
                      title="Rechazar"
                      titleStyle={styles.buttonTitle}
                    />
                  </View>
                )}
              </Mutation>
            </View>
          ) : null}

          {item.estado === 'Nueva' ? (
            <View style={{marginLeft: 'auto'}}>
              <Mutation mutation={PROFESSIONAL_CONSULTA_PROCEED}>
                {consultaProceed => (
                  <View style={styles.signupButtonContainer}>
                    <Button
                      light={colors.white}
                      dark={colors.white}
                      containerStyle={styles.buttonView}
                      onPress={() => Aceptarconsulta(consultaProceed)}
                      title="Aceptar"
                      titleStyle={styles.buttonTitle}
                    />
                  </View>
                )}
              </Mutation>
            </View>
          ) : null}

          {item.estado === 'Aceptado' ? (
            <View style={{flexDirection: 'row'}}>
              <Mutation mutation={PROFESSIONAL_CONSULTA_PROCEED}>
                {consultaProceed => (
                  <View style={styles.signupButtonContainer}>
                    <Button
                      light={colors.white}
                      dark={colors.white}
                      containerStyle={styles.buttonView}
                      onPress={() => handlefinalizar(consultaProceed)}
                      title="Finalizar"
                      titleStyle={styles.buttonTitle}
                    />
                  </View>
                )}
              </Mutation>
              <View style={{marginLeft: 15}}>
                <ButtonChat
                  light={colors.white}
                  dark={colors.white}
                  containerStyle={styles.buttonView}
                  onPress={() => Navigationachat()}
                  title="Iniciar chat"
                  titleStyle={styles.buttonTitle}
                />
              </View>
            </View>
          ) : null}

          {item.estado === 'Completada' ? (
            <View style={{marginLeft: 'auto'}}>
              <View style={styles.signupButtonContainer}>
                <Button
                  light={colors.white}
                  dark={colors.white}
                  containerStyle={styles.buttonView}
                  onPress={() =>
                    Navigation.navigate('DetailsConsulta', {data: item})
                  }
                  title="Diagnósticar"
                  titleStyle={styles.buttonTitle}
                />
              </View>
            </View>
          ) : null}
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <Query query={CONSULTA_BY_PROFESIONAL} variables={{profesional: id}}>
      {response => {
        if (response.loading) {
          return <PlaceHolder />;
        }
        if (response.error) {
          return <PlaceHolder />;
        }
        if (response) {
          return (
            <FlatList
              data={
                response &&
                response.data &&
                response.data.getConsultaByProfessional
                  ? response.data.getConsultaByProfessional.list
                  : ''
              }
              renderItem={item => _renderItem(item.item, response.refetch)}
              keyExtractor={item => item.id}
              showsVerticalScrollIndicator={false}
              ListEmptyComponent={<NoData menssge="Aún no tienes consulta" />}
            />
          );
        }
      }}
    </Query>
  );
}

const dynamicStyles = new DynamicStyleSheet({
  container: {
    flex: 1,
    height: 'auto',
    width: dimensions.ScreenWidth,
    backgroundColor: new DynamicValue(colors.white, colors.back_dark),
    alignSelf: 'center',
  },

  card: {
    width: dimensions.Width(92),
    height: 'auto',
    backgroundColor: new DynamicValue(
      colors.light_white,
      colors.back_suave_dark,
    ),
    marginHorizontal: dimensions.Width(4),
    marginTop: dimensions.Height(2),
    marginBottom: dimensions.Height(2),
    paddingHorizontal: dimensions.Width(2),
    paddingVertical: dimensions.Height(2),
    borderRadius: 15,
    shadowColor: colors.black,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.2,
    elevation: 3,
  },

  ol: {
    flexDirection: 'row',
    ...ifIphoneX(
      {
        width: dimensions.Height(30),
      },
      {
        width: dimensions.Height(37),
      },
    ),
  },

  avatar: {
    width: 70,
    height: 70,
    borderRadius: 13,
  },

  text: {
    color: colors.rgb_102,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
    marginTop: dimensions.Height(1),
  },

  textt: {
    color: colors.rgb_102,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
    marginTop: dimensions.Height(1.5),
  },
  title: {
    fontSize: dimensions.FontSize(16),
    fontWeight: '400',
    width: dimensions.Width(40),
  },

  buttonView: {
    backgroundColor: colors.light_blue,
    width: dimensions.Width(40),
    borderRadius: dimensions.Width(8),
    marginTop: dimensions.Height(1),
  },
  buttonTitle: {
    alignSelf: 'center',
    paddingVertical: dimensions.Height(1),
    paddingHorizontal: dimensions.Width(2),
    color: colors.white,
    fontWeight: '300',
    fontSize: dimensions.FontSize(14),
  },
  signupButtonContainer1: {
    marginTop: dimensions.Height(3),
  },
});

const mapDispatchToProps = {
  getUsers: usersGet,
};

export default connect(
  null,
  mapDispatchToProps,
)(Consulta);
