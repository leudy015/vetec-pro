import {Dimensions, Platform, NativeModules, PixelRatio} from 'react-native';
import {isIphoneX} from 'react-native-iphone-x-helper';
import {
  responsiveHeight,
  responsiveWidth,
} from 'react-native-responsive-dimensions';

const {fontScale} = Dimensions.get('window');

const {StatusBarManager} = NativeModules;

const Width = widthPercent => {
  return responsiveWidth(widthPercent);
};
const Height = heightPercent => {
  return responsiveHeight(heightPercent);
};
const isIphone = () => {
  return Platform.OS === 'ios';
};

const FontSize = size => {
  return size / fontScale;
};

const IsIphoneX = () => {
  return isIphoneX();
};

const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;

const StatusBarHeight = () => {
  return Platform.OS === 'android' ? StatusBarManager.HEIGHT : 20;
};

const convertToPixel = size => {
  return PixelRatio.roundToNearestPixel(size);
};

export const dimensions = {
  Width,
  Height,
  FontSize,
  IsIphoneX,
  isIphone,
  ScreenHeight,
  ScreenWidth,
  StatusBarHeight,
  convertToPixel,
};
