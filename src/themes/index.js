import {colors} from './colors';
import {fonts} from './fonts';
import {dimensions} from './dimensions';

export {colors, fonts, dimensions};
