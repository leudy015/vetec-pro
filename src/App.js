import React, {Component} from 'react';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {ApolloProvider} from 'react-apollo';
import {ApolloClient} from 'apollo-client';
import {createHttpLink} from 'apollo-link-http';
import {setContext} from 'apollo-link-context';
import {InMemoryCache} from 'apollo-cache-inmemory';
import NavigationService from './services/NavigationService';
import {AuthContainer, UnAuthContainer} from './navigation';
import {NETWORK_INTERFACE, NETWORK_INTERFACE_LINK} from './constants/config';
import {DarkModeProvider} from 'react-native-dark-mode';
import NetInfo from '@react-native-community/netinfo';
import * as StoreReview from 'react-native-store-review';
import OneSignal from 'react-native-onesignal';
import {useScreens} from 'react-native-screens';
import Nointernet from './../Nointernet';
import configureStore from './store';
import rootSaga from './sagas';
import {StatusBar} from 'react-native';
import Bugsnag from '@bugsnag/react-native'

Bugsnag.start()

useScreens();

const {runSaga, store, persistor} = configureStore();
runSaga(rootSaga);
// Subscribe
const unsubscribe = NetInfo.addEventListener(state => {
  console.log('Connection type', state.type);
  console.log('Is connected?', state.isConnected);
});
// Unsubscribe
unsubscribe();

const httpLink = createHttpLink({
  uri: NETWORK_INTERFACE,
});

const authLink = setContext(async (_, {headers}) => {
  // get the authentication token from local storage if it exists
  const token = await AsyncStorage.getItem('token');
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? token : '',
    },
  };
});

const apolloClient = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
});

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: 'fetching',
      device: '',
      Isconected: false,
    };
    OneSignal.init('3b850d4c-457b-4e1b-bc7d-e5e4dab86b27', {
      kOSSettingsKeyAutoPrompt: true,
    });
    OneSignal.inFocusDisplaying(0);
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    if (StoreReview.isAvailable) {
      StoreReview.requestReview();
    }
  }

  onReceived(notification) {
    console.log('Notification received: ', notification);
  }

  onOpened = async () => {
    const id = await AsyncStorage.getItem('id');
    NavigationService.navigate('Chats', {
      data: id,
    });
  };

  onIds = async device => {
    const id = await AsyncStorage.getItem('id');
    const Userid = device && device.userId;
    if (Userid && id) {
      fetch(
        `${NETWORK_INTERFACE_LINK}/save-profesionalid-notification?UserId=${Userid}&id=${id}`,
      ).catch(err => console.log(err));
    } else {
      return null;
    }
  };

  Isonline = async () => {
    const id = await AsyncStorage.getItem('id');
    const lastine = new Date();
    fetch(
      `${NETWORK_INTERFACE_LINK}/user-active?userid=${id}&lastTime=${lastine}`,
    ).catch(err => console.log(err));
  };

  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem('token');
      console.log(value);
      if (value !== null) {
        this.setState({token: value});
        global.token = value;
      } else {
        this.setState({token: false});
      }
    } catch (e) {
      // error reading value
      this.setState({token: false});
    }
    this.onIds();
    this.Isonline();

    NetInfo.fetch().then(state => {
      this.setState({
        Isconected: state.isConnected,
      });
    });
  }

  refeshButton = () => {
    NetInfo.fetch().then(state => {
      this.setState({
        Isconected: state.isConnected,
      });
    });
  };

  render() {
    if (this.state.token == 'fetching') {
      return null;
    }
    if (!this.state.Isconected) {
      return <Nointernet />;
    } else
      return (
        <DarkModeProvider>
          <Provider store={store}>

            <PersistGate loading={true} persistor={persistor}>
            <StatusBar translucent backgroundColor="transparent" />
              <ApolloProvider client={apolloClient}>
                {this.state.token == false ? (
                  <UnAuthContainer
                    ref={navigatorRef => {
                      NavigationService.setContainer(navigatorRef);
                    }}
                  />
                ) : (
                  <AuthContainer
                    ref={navigatorRef => {
                      NavigationService.setContainer(navigatorRef);
                    }}
                  />
                )}
              </ApolloProvider>
            </PersistGate>
          </Provider>
        </DarkModeProvider>
      );
  }
}
