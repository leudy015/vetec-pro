import  React  from 'react';
import { View } from "react-native";
import { colors, dimensions } from './src/themes';
import { Button } from './src/components/Button';
import { DynamicStyleSheet, DynamicValue, useDynamicStyleSheet } from 'react-native-dark-mode'
import Nodata from './src/components/NoData'
import Navigation from './src/services/NavigationService';



export default function Nointernet(){

    const styles = useDynamicStyleSheet(dynamicStyles)

    return(
        <View style={styles.container} >
          <Nodata menssge='Hay un problema con tu conexión a internet.' />
          <View style={styles.signupButtonContainer}>
          <Button light={colors.white} dark={colors.white} containerStyle={styles.buttonView}
              onPress={() => {}} title="Volver a intentarlo"
              titleStyle={styles.buttonTitle} />
          </View>
      </View>
    )
}

const dynamicStyles = new DynamicStyleSheet({

    container: {
        flex: 1,
        justifyContent: 'center', 
        height: dimensions.Height(100),
        backgroundColor: new DynamicValue(colors.white, colors.back_dark)
      },
    
    buttonView: {
      backgroundColor: colors.light_blue,
      width: dimensions.Width(80),
      borderRadius: dimensions.Width(8),
      alignSelf: 'center',
      marginTop: dimensions.Height(4)
    },
    buttonTitle: {
      alignSelf: 'center',
      paddingVertical: dimensions.Height(2),
      paddingHorizontal: dimensions.Width(2),
      color: colors.white,
      fontWeight: '300',
      fontSize: dimensions.FontSize(14),
    },
    
    })
    
